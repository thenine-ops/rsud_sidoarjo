<?=$this->session->flashdata('name');?>
<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>');;?>


          <?=form_open();?>
            <div class="input-group mb-3 mt-4">
              <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <p class="mb-3">
              <a class="text-info" href="<?=base_url('csunitit/auth/lupa');?>">Forget Password</a>
            </p>
              <div class="col-12">
                <button type="submit" class="btn btn-success  btn-lg form-control" style="border-radius:20px;padding-bottom:40px">Sign In</button>
              <!-- /.col -->
            </div>
          </form>

