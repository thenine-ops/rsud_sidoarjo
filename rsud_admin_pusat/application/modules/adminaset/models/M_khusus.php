<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_khusus extends CI_Model {

	public function master()
	{
		return $this->db->select('sg.*, a.name subname')
						->from('sparepart_generals sg')
						->join('asset_sub_categories a','a.id=sg.assetsubcategory_id')
						->order_by('sg.name','asc')
						->get()
						->result()
		;
	}

	public function master_by_id($id)
	{
		return $this->db->select('sg.*, a.name subname')
						->from('sparepart_generals sg')
						->join('asset_sub_categories a','a.id=sg.assetsubcategory_id')
						->where('sg.id',$id)
						->order_by('sg.name','asc')
						->get()
						->row()
		;
	}

	public function master_tambah()
	{
		$post = $this->input->post();
		$data = [
			'assetsubcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		return $this->db->insert('sparepart_generals', $data);
	}

	public function master_edit()
	{
		$post = $this->input->post();
		$data = [
			'assetsubcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'user_ent' => $this->session->id
		];
		$where = ['id' => htmlspecialchars(trim($post['id']))];
		$this->db->set('date_ent','now()');
		return $this->db->update('sparepart_generals', $data, $where);
	}



	public function data()
	{
		return $this->db->select('spg.*, sg.name sgname')
						->from('sparepart_product_generals spg')
						->join('sparepart_generals sg','sg.id=spg.sparepartgeneral_id')
						->order_by('spg.name','asc')
						->get()
						->result()
		;
	}

	public function data_by_id($id)
	{
		return $this->db->select('spg.*, sg.name sgname')
						->from('sparepart_product_generals spg')
						->join('sparepart_generals sg','sg.id=spg.sparepartgeneral_id')
						->where('spg.id', $id)
						->order_by('spg.name','asc')
						->get()
						->row()
		;
	}

	public function lastId($tabel)
	{
		$data = $this->db->select("nextval(pg_get_serial_sequence('$tabel', 'id')) as id")->get($tabel)->row();
		if ($data->id<=9) {
			$d = '0'.$data->id;
		} else {
			$d = $data->id;
		}
		
		return $d;
	}

	public function data_tambah()
	{
		$post = $this->input->post();
		$data = [
			'sparepartgeneral_id' => htmlspecialchars(trim($post['kategori'])),
			'id_sparepart_register' => 'SPK-'.date('Ymd').'0'.$this->lastId('sparepart_product_generals'),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'type' => htmlspecialchars(trim($post['type'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		return $this->db->insert('sparepart_product_generals', $data);
	}

	public function data_edit()
	{
		$post = $this->input->post();
		$data = [
			'sparepartgeneral_id' => htmlspecialchars(trim($post['kategori'])),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'type' => htmlspecialchars(trim($post['type'])),
			'user_ent' => $this->session->id
		];
		$where = ['id' => htmlspecialchars(trim($post['id']))];
		$this->db->set('date_ent','now()');
		return $this->db->update('sparepart_product_generals', $data, $where);
	}

}

/* End of file M_khusus.php */
/* Location: ./application/modules/adminaset/models/M_khusus.php */