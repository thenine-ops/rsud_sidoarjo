<?=$this->session->flashdata('name');?>
<?php
$attr = [
	'class' => 'form-inline mb-4'
];
?>
<?=validation_errors('<small class="text-danger">','</small>');?>
<?=form_open('adminaset/sparepart/kategori_edit/'.$data->id, $attr);?>
	<input type="hidden" name="id" id="inputId" class="form-control" value="<?=$data->id;?>">
	<label class="sr-only" for="nama">Nama</label>
	<input type="text" class="form-control form-control-lg mb-2 mr-sm-2 col-md-7 <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" placeholder="Nama Kategori Spare Part" value="<?=$data->name;?>">

	<button type="submit" class="btn btn-success btn-lg mb-2 rounded">Simpan</button>
</form>
