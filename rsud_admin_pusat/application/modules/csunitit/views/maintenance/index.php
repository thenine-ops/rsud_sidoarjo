<h4><a class="text-success">MAINTENANCE</a> | <a href="<?=base_url('csunitit/maintenance/data');?>" class="text-dark">DATA MAINTENANCE</a></h4>
<?=form_open('csunitit/maintenance');?>
<p><strong>Data Tiket</strong></p>
<?=$this->session->flashdata('name');?>
<div class="flex">
	<div class="f-col-5 f-float-round pad-lg mb-3">
		<div class="row">
			<div class="col-md-2">
				<div class="row">
					ID Tiket
				</div>
				<div class="row">
					<?php $idtiket = "TI-".time(); echo $idtiket; ?>
					<input type="hidden" name="idtiket" value="<?=$idtiket;?>">
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					Tanggal
				</div>
				<div class="row">
					<?=hari_ini();?>, <?=date_indo(date('Y-m-d'));?>
				</div>
			</div>
			<div class="col-md px-4">
				<div class="row">
					Ruangan
				</div>
				<div class="row">
					<select name="ruangan" id="inputRuangan" class="form-control" required="required">
						<option value="">Pilih Ruangan</option>
						<?php foreach($ruangan as $r): ?> 
						<option value="<?=$r->id;?>"><?=$r->name;?></option>
						<?php endforeach; ?>
					</select>
					<input type="hidden" name="id_asset_register" id="inputId_asset_register" class="form-control" value="">
					<input type="hidden" name="asset_name" id="inputAsset_name" class="form-control" value="">
					<input type="hidden" name="kategori" id="inputKategori" class="form-control" value="">
				</div>
			</div>
			<div class="col-md">
				<div class="row">
					Aset
				</div>
				<div class="row">
					<select name="aset" id="inputAset" class="form-control" required="required">
						<option value="">Pilih Aset</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="flex">
	<h6>Delegasi Petugas</h6>
</div>
<input type="hidden" name="idtiket" id="inputIdtiket" class="form-control" value="<?=$idtiket;?>">
<?php $no=1; foreach($petugas as $data): ?>
<div class="flex">
	<div class="f-col-9 f-float-round pad-sm">
		<div class="row">
			<div class="col-md-3 my-auto">
				<img src="<?=base_url('aset/dist/img/avatar04.png');?>" width="100">
			</div>
			<div class="col-md-6 my-auto">
				<div class="row">
					<small>Nama Petugas</small>
				</div>
				<div class="row">
					<h4 class="text-warning"><?=$data->name;?></h4>
				</div>
				<div class="row">
					<table>
						<tr>
							<td width="10">Total Tugas</td>
							<td width="10">Selesai</td>
							<td width="10">Belum Selesai</td>
						</tr>
						<tr>
							<td><h4 class="text-primary"><?=rekor_petugas($data->id);?></h4></td>
							<td><h4 class="text-primary"><?=rekor_petugas($data->id, 4);?></h4></td>
							<td><h4 class="text-primary"><?=rekor_petugas($data->id, 3);?></h4></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3 my-auto">
				<div class="custom-control custom-radio">
				  <input type="radio" id="<?=$no;?>" name="petugas" value="<?=$data->id;?>" class="custom-control-input" <?=($no=='1')?'checked':'';?>>
				  <label class="custom-control-label lead" for="<?=$no;?>"><strong>Pilih</strong></label>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $no++; endforeach; ?>

<div class="flex justify-content-center">
	<button type="submit" class="btn btn-success mt-3 btn-lg">Tugaskan</button><?=form_close();?>
</div>