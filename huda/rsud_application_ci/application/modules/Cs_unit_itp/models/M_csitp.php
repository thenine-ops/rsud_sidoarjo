<?php

class M_csitp extends CI_Model
{
    function get_rooms()
    {
        return $this->db->get('room_details');
    }
    function rooms_area($id)
    {
        return $this->db->get_where('room_areas', array('id' => $id));
    }
    function save_patients_tickets()
    {
        //ambil zona waktu
        date_default_timezone_set("Asia/Jakarta");
        //mengambil room categori berdasarkan id room detail yang di input user
        $get_all_room = $this->db->get_where("room_details", array("id" => $this->input->post("id_roomdetail")))->result_array();
        $room_categories_id = $get_all_room[0]['roomcategory_id'];

        //set room inventory id(di sesuaikan permintaan untuk pemilihan inventory nya seperti apa)
        $room_inventory = "8";

        //set transporter ticket id(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id = strtotime(date('h:i:s'));

        //set id ticket register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_ticket_register = strtotime(date('h:i:s'))
        ;

        //set id asset register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_asset_register = "123456";

        //data yang mau di insert di table transporter ticket
        $insert = array(
            "id" => $id,
            "roomdetail_id" => htmlspecialchars(trim($this->input->post('id_roomdetail'))),
            "roomcategory_id" => $room_categories_id,
            "roominventory_id" => $room_inventory,
            "assetproduct_id" => "8",
            "useradmin_id" => "1",
            "id_ticket_register" => $id_ticket_register,
            "id_asset_register" => $id_asset_register,
            "room_inventory_name" => "bunga",
            "patient_name" => htmlspecialchars(trim($this->input->post('nama'))),
            "patient_contact" => htmlspecialchars(trim($this->input->post('kontak'))),
            "ticket_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_pelapor'))),
            "date_ent" => date('Y-m-d h:i:s')
        );
        //data yang mau di save di table transporter
        $insert2 = array(
            "transporterticket_id" => $id,
            "id_ticket_register" => $id_ticket_register,
            "roominventory_id" => $room_inventory,
            "room_origin_id" => htmlspecialchars(trim($this->input->post('id_room_penjemputan'))),
            "room_destination_id " => htmlspecialchars(trim($this->input->post('id_roomdetail'))),
            "transfer_date" => date('Y-m-d h:i:s'),
            "arrival_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "useroperator_id" => "1",
            "useradmin_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_pelapor'))),
            "date_ent" => date('Y-m-d h:i:s'),
            "transporteractivity_id" => "1"
        );
        //save di table transporter ticket
        $save1 = $this->db->insert("transporter_tickets", $insert);

        //save di table transporter
        $save2 = $this->db->insert("transporters", $insert2);
        return true;
    }
    // mengambil jumlah status permintaan
    function get_count_permintaan_activity()
    {
        return $this->db->get_where('transporters', array('transporteractivity_id' => 1))->num_rows();
    }
    function get_count_terjadwal_activity()
    {
        return $this->db->get_where('transporters', array('transporteractivity_id' => 2))->num_rows();
    }
    function get_count_selesai_activity()
    {
        return $this->db->get_where('transporters', array('transporteractivity_id' => 3))->num_rows();
    }
    //-----------------------------------------------------------------------

    //mengambil data aktifitas pemindahan pasien
    function get_aktifitas_harian()
    {
        $this->db->select('tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,rt.name as name_area_destination,rd.id as id_roomdetails_ori ,rd.name as name_room_origin,rdt.id as id_roomdetails_dest,rdt.name as name_room_destination,tr_stat.name as name_status,tr_activ.name as name_activ');
        $this->db->from('transporters tr'); 
        $this->db->join('transporter_tickets trt', 'trt.id=tr.transporterticket_id','left');
        $this->db->join('room_areas ra', 'ra.id=tr.room_origin_id','left');
        $this->db->join('room_areas rt', 'rt.id=tr.room_destination_id','left');
        $this->db->join('room_details rd', 'rd.id=tr.room_origin_id','left');
        $this->db->join('room_details rdt', 'rdt.id=tr.room_destination_id','left');
        $this->db->join('transporter_status tr_stat', 'tr_stat.id=tr.transporterstatus_id','left');
        $this->db->join('transporter_activities tr_activ', 'tr_activ.id=tr.transporteractivity_id','left');
        $query = $this->db->get();
        return $query;
    }
    //mengambil data aktifitas pemindahan pasien
    function get_aktifitas_harian_filter($filter)
    {
        $sql = "SELECT tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,
        tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,
        rt.name as name_area_destination,rd.name as name_room_origin,rdt.name as name_room_destination,
        tr_stat.name as name_status,tr_activ.name as name_activ FROM transporters tr
        LEFT JOIN transporter_tickets trt ON trt.id=tr.transporterticket_id
        LEFT JOIN room_areas ra ON ra.id=tr.room_origin_id
        LEFT JOIN room_areas rt ON rt.id=tr.room_destination_id
        LEFT JOIN room_details rd ON rd.id=tr.room_origin_id
        LEFT JOIN room_details rdt ON rdt.id=tr.room_destination_id
        LEFT JOIN transporter_status tr_stat ON tr_stat.id=tr.transporterstatus_id
        LEFT JOIN transporter_activities tr_activ ON tr_activ.id=tr.transporteractivity_id WHERE transfer_date::varchar LIKE '%".$filter."%'
        ";
        return $this->db->query($sql);

    }

    function get_aktifitas_harian_detail($id)
    {
        $this->db->select('tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,rt.name as name_area_destination,rd.id as id_roomdetails_ori ,rd.name as name_room_origin,rdt.id as id_roomdetails_dest,rdt.name as name_room_destination,tr_stat.name as name_status,tr_activ.name as name_activ');
        $this->db->from('transporters tr'); 
        $this->db->join('transporter_tickets trt', 'trt.id=tr.transporterticket_id','left');
        $this->db->join('room_areas ra', 'ra.id=tr.room_origin_id','left');
        $this->db->join('room_areas rt', 'rt.id=tr.room_destination_id','left');
        $this->db->join('room_details rd', 'rd.id=tr.room_origin_id','left');
        $this->db->join('room_details rdt', 'rdt.id=tr.room_destination_id','left');
        $this->db->join('transporter_status tr_stat', 'tr_stat.id=tr.transporterstatus_id','left');
        $this->db->join('transporter_activities tr_activ', 'tr_activ.id=tr.transporteractivity_id','left');
        $this->db->where("transporterticket_id",$id);
        $query = $this->db->get();
        return $query;
    }
    function save_ganti_petugas_model()
    {
        //ambil zona waktu
        date_default_timezone_set("Asia/Jakarta");
        //mengambil room categori berdasarkan id room detail yang di input user
        $get_all_room = $this->db->get_where("room_details", array("id" => $this->input->post("room_dest")))->result_array();
        $room_categories_id = $get_all_room[0]['roomcategory_id'];
        //id ticket yang mau di close
        $idtiket = $this->input->post('idtiket');

        //set room inventory id(di sesuaikan permintaan untuk pemilihan inventory nya seperti apa)
        $room_inventory = "8";

        //set transporter ticket id(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id = strtotime(date('h:i:s'));

        //set id ticket register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_ticket_register = strtotime(date('h:i:s'))
        ;

        //set id asset register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_asset_register = "123456";

        //data yang mau di insert di table transporter ticket
        $insert = array(
            "id" => $id,
            "roomdetail_id" => htmlspecialchars(trim($this->input->post('room_dest'))),
            "roomcategory_id" => $room_categories_id,
            "roominventory_id" => $room_inventory,
            "assetproduct_id" => "8",
            "useradmin_id" => "1",
            "id_ticket_register" => $id_ticket_register,
            "id_asset_register" => $id_asset_register,
            "room_inventory_name" => "bunga",
            "patient_name" => htmlspecialchars(trim($this->input->post('patient_name'))),
            "patient_contact" => htmlspecialchars(trim($this->input->post('patient_contact'))),
            "ticket_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_petugas'))),
            "date_ent" => date('Y-m-d h:i:s')
        );
        //data yang mau di save di table transporter
        $insert2 = array(
            "transporterticket_id" => $id,
            "id_ticket_register" => $id_ticket_register,
            "roominventory_id" => $room_inventory,
            "room_origin_id" => htmlspecialchars(trim($this->input->post('room_ori'))),
            "room_destination_id " => htmlspecialchars(trim($this->input->post('room_dest'))),
            "transfer_date" => date('Y-m-d h:i:s'),
            "arrival_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "useroperator_id" => "1",
            "useradmin_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_petugas'))),
            "date_ent" => date('Y-m-d h:i:s'),
            "transporteractivity_id" => "1"
        );
        //save di table transporter ticket
        $save1 = $this->db->insert("transporter_tickets", $insert);

        //save di table transporter
        $save2 = $this->db->insert("transporters", $insert2);
        //update status ticket petugas sebelumnya
        $upd_status = "UPDATE transporter_tickets SET transporterstatus_id=2 WHERE id='". $idtiket ."' ";
        $this->db->query($upd_status);
        //update status activity petugas sebelumnya
        $upd_status2 = "UPDATE transporters SET transporterstatus_id=2,transporteractivity_id=4 WHERE transporterticket_id='". $idtiket ."' ";
        $this->db->query($upd_status2);
        return true;
    }
}
