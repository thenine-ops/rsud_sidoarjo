<?php $this->load->view('sparepart/menu');?>

<?=$this->session->flashdata('name');?>
<a href="<?=base_url('adminaset/sparepart/data_tambah');?>" class="btn btn-success btn-lg mb-3 rounded">Tambah Data</a>

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Spare Part</th>
      <th scope="col">Nama Aset</th>
      <th scope="col">Quantity</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php $no=1; foreach($data as $d): ?>
    <tr>
      <th scope="row"><?=$no++;?></th>
      <td><?=$d->sparepart;?></td>
      <td><?=$d->aset;?></td>
      <td><?=$d->quantity;?></td>
      <td><?=$d->description;?></td>
      <td>
      	<a href="<?=base_url('adminaset/sparepart/data_edit/'.$d->id);?>" class="btn btn-primary btn-sm">Edit</a>
      	<a href="<?=base_url('adminaset/sparepart/data_hapus/'.$d->id);?>" class="btn btn-danger btn-sm">Hapus</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
