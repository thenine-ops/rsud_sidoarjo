<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('csunitaset');
		$this->load->model('M_sparepart','ms');
		
	}

	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('sparepart', 'Spare Part', 'trim|required|numeric');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Dashboard Admin Aset',
				'hal' => 'dashboard/index',
				'sparepart' => $this->ms->data(),
				'js' => 'dashboard/js',
				'css' => 'dashboard/css'
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->tambah_stok();
			if ($cek) {
				$this->session->set_flashdata('berhasil', '<div class="alert alert-success" role="alert">Data berhasil disimpan!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal disimpan!</div>');
			}
			redirect('csunitaset/dashboard','refresh');
		}
		
	}

	public function board()
	{
		$this->load->view('dashboard/onboard');
	}

	public function install()
	{
		$this->load->view('install/index');
	}
}

/* End of file Dashboard.php */
/* Location: ./application/modules/csunitaset/controllers/Dashboard.php */