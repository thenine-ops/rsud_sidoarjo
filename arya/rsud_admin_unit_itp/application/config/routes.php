<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Cs_unit_itp/aktifitas_pemindahan_pasien';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['adminpusat'] = 'adminpusat/dashboard';
$route['adminpusat/backup/(:any)'] = 'adminpusat/backup';

$route['adminunit'] = 'adminunit/dashboard';
$route['adminunit/backup/(:any)'] = 'adminunit/backup';

$route['csunitit'] = 'csunitit/dashboard';

//CS_ITP pemindahan pasien
$route['csitp/save'] = 'Cs_unit_itp/save_patient_movement';
$route['csitp/cancel'] = 'Cs_unit_itp/cancel_ticket';
$route['r_penjemputan'] = 'Cs_unit_itp/get_rooms_area';
$route['r_tujuan'] = 'Cs_unit_itp/get_rooms_area';
$route['csitp/pindah_pasien'] = 'Cs_unit_itp';
$route['csitp/data_pindah_pasien'] = 'Cs_unit_itp/data_pindah_pasien';
$route['csitp/ganti_petugas'] = 'Cs_unit_itp/ganti_petugas';
$route['csitp/ganti_petugas_form'] = 'Cs_unit_itp/ganti_petugas_form';
$route['csitp/save_ganti_petugas'] = 'Cs_unit_itp/save_ganti_petugas';
$route['csitp/laporan_aktivitas_pindah_pasien'] = 'Cs_unit_itp/laporan_aktivitas_pindah_pasien';
$route['csitp/laporan_aktivitas_petugas'] = 'Cs_unit_itp/laporan_aktivitas_petugas';
$route['csitp/search_laporan_aktivitas_petugas'] = 'Cs_unit_itp/search_laporan_aktivitas_petugas';

//CS_ITP aktifitash pemeindahan pasien
$route['csitp/aktifitas_pemindahan_pasien'] = 'Cs_unit_itp/aktifitas_pemindahan_pasien';
$route['adminaset'] = 'adminaset/dashboard';
