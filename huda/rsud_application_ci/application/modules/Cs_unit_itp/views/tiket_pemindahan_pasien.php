<script src="<?= base_url(); ?>aset/external/js/jquery-3.4.1.min.js"></script>

<body class="dashboard-body">
    <div class="loading">
        <img src="<?= base_url(); ?>aset/image/asset/loading.gif" alt="">
    </div>
    <div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?= base_url(); ?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
    </div>
    <div class="content-container">
        <div class="col col-content padding-content">
            <h4 class="bold">TIKET PEMINDAHAN PASIEN</h4>
            <div id="informasi"></div>
            <div class="date-info f-green padding-tanggal">
                <a href="<?= site_url('Cs_unit_itp/aktifitas_pemindahan_pasien');?>" class="btn btn-sm rounded pull-right c-danger">x</a>
            </div>
            <div class="flex">
                <div class="f-col">
                    <div class="gray">Tanggal</div>
                    <button class="btn btn-outline-success w95"><?= sekarang(date('l'));
                                                                echo date('d-m-Y'); ?></button>
                </div>
                <div class="f-col">
                    <div class="gray">ID Tiket</div>
                    <button class="btn btn-outline-success w95">ITP_<?= strtotime(date('d-m-Y')); ?></button>
                </div>
                <div class="f-col-right">
                    <div class="gray">Status</div>
                    <button class="btn btn-outline-danger w95 m-right0">MENUNGGU </button>
                </div>
            </div>
            <?= validation_errors(); ?>
            <?php $atrributs = array('id' => 'form1'); ?>
            <?= form_open('csitp/save', $atrributs); ?>
            <input style="display:none;" id="cs" type="text" name="csrf_test_rsud" value="<?= $token; ?>">
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Pasien</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <input type="text" class="form-control" name="nama" value="" placeholder="Nama Pasien">
                    </div>
                    <div class="label m-input">
                        <input type="text" class="form-control" name="kontak" value="" placeholder="Kontak">
                    </div>
                </div>
            </div><br>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Ruangan Penjemputan</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <select id="r_penjemputan" name="id_room_penjemputan" class="form-control">
                            <option value="" selected disabled>Nama Ruangan</option>
                            <?php foreach ($rooms as $row) {; ?>
                                <option id="r_p" value="<?php echo $row->id; ?>"> <?php echo $row->name; ?></option>
                            <?php }; ?>
                        </select>
                    </div>
                    <div class="label m-input">
                        <input id="area_penjemputan" type="text" class="form-control" name="area_ruangan_penjemputan" value="" placeholder="Area Ruangan" disabled="disabled">
                    </div>
                    <div class="label m-input">
                        <input id="detail_ruangan_penjemputan" type="text" class="form-control" name="detail_raungan_penjemputan" value="" placeholder="Detail Ruangan">
                    </div>
                </div>
            </div><br>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Ruangan Tujuan</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <select id="r_tujuan" name="id_roomdetail" class="form-control">
                            <option value="" selected disabled>Nama Ruangan</option>
                            <?php foreach ($rooms as $row) {; ?>
                                <option id="r_t" value="<?php echo $row->id; ?>"> <?php echo $row->name; ?></option>
                            <?php }; ?>
                        </select>
                    </div>
                    <div class="label m-input">
                        <input id="area_tujuan" type="text" class="form-control" name="area_ruangan_tujuan" value="" placeholder="Area Ruangan" disabled="disabled">
                    </div>
                    <div class="label m-input">
                        <input id="detail_ruangan_tujuan" type="text" class="form-control" name="detail_ruangan_tujuan" value="" placeholder="Detail Ruangan">
                    </div>
                </div>
            </div><br>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Pelapor</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <input type="text" class="form-control" name="nama_pelapor" value="" placeholder="Nama Pelapor">
                    </div>
                    <div class="label m-input">
                    <select id="r_t" name="nama_ruangan" class="form-control">
                            <option value="" selected disabled>Nama Ruangan</option>
                            <?php foreach ($rooms as $row) {; ?>
                                <option id="r_t" value="<?php echo $row->id; ?>"> <?php echo $row->name; ?></option>
                            <?php }; ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-lg c-success pull-right">Terbitkan Tiket</button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?= base_url(); ?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASEIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="footer-container">
        <label>Nine Cloud 2019</label>
    </div>
</body>
<script>
    $('#r_penjemputan').on('change', function() {
        var id = $('#r_penjemputan').val();
        var token = "<?php echo $this->security->get_csrf_hash(); ?>";
        var name = "<?php echo $this->security->get_csrf_token_name();; ?>";
        $.ajax({
            url: "<?php echo site_url('r_penjemputan'); ?>",
            method: "POST",
            data: {
                id: id,
                csrf_test_rsud: token
            },
            dataType: "JSON",
            success: function(response) {
                $("#area_penjemputan").val(response.area);
            }
        })
    })
    $('#r_tujuan').on('change', function() {
        var id = $('#r_tujuan').val();
        var token = "<?php echo $this->security->get_csrf_hash(); ?>";
        var name = "<?php echo $this->security->get_csrf_token_name(); ?>";
        $.ajax({
            url: "<?php echo site_url('r_tujuan'); ?>",
            method: "POST",
            data: {
                id: id,
                csrf_test_rsud: token
            },
            dataType: "JSON",
            success: function(response) {
                $("#area_tujuan").val(response.area);
            }
        })
    })
</script>