<body class="dashboard-body">
    <div class="loading">
        <img src="<?=base_url();?>aset/image/asset/loading.gif" alt="">
    </div>
    <div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?=base_url();?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
        <a href="" class="btn btn-danger rounded pull-right">close</a>
    </div>
    <div class="content-container">
        <div class="col col-content padding-content">
            <h4 class="bold">PINDAH PASIEN</h4>
            <div class="date-info f-green padding-tanggal">
                Jumat, 13 Maret 2020
            </div>
            <div class="flex">
                <ul class="sub-menu-container">
                    <li class="item active"><a href="">GANTI PETUGAS</a></li>
                    <li class="sep">|</li>
                    <li class="item"><a href="<?= site_url('csitp/data_pindah_pasien'); ?>">DATA PINDAH PASIEN</a></li>
                </ul>
            </div>
            <?php validation_errors(); $attributes = array("id"=>"form1");
                echo form_open("csitp/ganti_petugas",$attributes); ?>
            <div class="flex">
                <div class="f-col f-float-round padding-tanggal">
                    <div class="label m-input">
                        <select name="id_petugas" id="" class="form-control" required>
                            <option value="" selected disabled>Pilih Petugas</option>
                            <?php foreach ($petugas as $row) {; ?>
                                <option id="petugas_<?php echo $row->id; ?>" value="<?php echo $row->id; ?>"> <?php echo $row->user_ent; ?></option>
                            <?php }; ?>
                        </select>
                    </div>
                    <div class="label m-input">
                        <input style="display:none;" id="cs" type="text" name="csrf_test_rsud" value="<?= $token; ?>">
                        <input placeholder="Cari Tanggal" class="form-control" type="text" onfocus="(this.type='date')"
                            id="date" name="tgl" required>
                    </div>
                    <div class="flex">
                        <div class="f-col-4">
                            <button type="submit" class="btn c-success rounded pull-right" style="width: 100px;">CARI</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Aktifitas Harian Pemindahan Pasien</div>
                </div>
            </div>
            <div class="flex">
                <div class="">
                    <table class="table table-green table-bordered">
                        <col>
                        <colgroup span="2"></colgroup>
                        </col>
                        <tr class="c-success">
                            <th colspan="1" rowspan="2" scope="colgroup">No</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
                            <th colspan="1" rowspan="2" scope="colgroup">ID Tiket Permintaan</th>
                            <th colspan="2" scope="colgroup">Ruangan Penjemputan</th>
                            <th colspan="2" scope="colgroup">Ruangan Tujuan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Petugas</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Pasien</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status Tiket</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status Pelayanan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Action</th>
                        </tr>
                        <tr class="c-success">
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                        </tr>
                        <?php
                        $no = 1;
                        foreach($result as $row) { ;?>
                        <tr>
                            <td><?= $no++;?></td>
                            <td><?php $tgl = $row->transfer_date; $hasil =  explode(" ",$tgl); echo $hasil[0] ;?></td>
                            <td><?= $row->transporterticket_id;?></td>
                            <td><?= $row->name_room_origin;?></td>
                            <td><?= $row->name_area_origin;?></td>
                            <td><?= $row->name_room_destination;?></td>
                            <td><?= $row->name_area_destination;?></td>
                            <td><?= $row->user_ent;?></td>
                            <td><?= $row->patient_name;?></td>
                            <td><?= $row->name_status;?></td>
                            <td><?= $row->name_activ;?></td>
                            <td>
                                <div class="dropdown">
                                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Pilih Aksi
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                    
                                    <a class="dropdown-item" href="<?=site_url('Cs_unit_itp/detail_pemindahan_pasien/'.$row->transporterticket_id);?>">Detail</a>
                                    <a class="dropdown-item" href="<?=site_url('Cs_unit_itp/ganti_petugas_form/'.$row->transporterticket_id);?>">Ganti Petugas</a>
                                    <a class="dropdown-item" href="<?=site_url('Cs_unit_itp/batalkan_tiket/'.$row->transporterticket_id);?>">Batal</a>
                                  </div>
                                </div>                                  
                            </td>
                        </tr>
                        <?php } ;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?= base_url(); ?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/ganti_petugas'); ?>" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/laporan_aktivitas_petugas'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <script>
        $("#date").on('keypress', function(e){
            if(e.which == 13){
                $("#form1").submit();
            }
        })
    </script>