<?php
function get_acronym($words){
	$word_array = explode(' ', ucwords($words));
	$acronym = "";

	foreach ($word_array as $value) {
		$acronym .= substr($value, 0, 1);
	}
	return substr($acronym, 0, 2);
}
function uri_img($type, $img){
	return base_url()."prabotan/image/$type/$img";
}
?>