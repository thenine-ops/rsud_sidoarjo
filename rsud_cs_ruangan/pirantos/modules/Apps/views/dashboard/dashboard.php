<h3>DASHBOARD</h3>
<div class="flex">
	<div class="f-col-4 f-float-round pad-sm">
		<h5>Aktivitas Hari Ini</h5>
		<div class="flex-in">
			<div class="f-col-8">
				<div class="pad-xs c-info rounded mar-v-xs">
					Permintaan
				</div>
				<div class="pad-xs c-danger rounded mar-v-xs">
					Open
				</div>
				<div class="pad-xs c-warning rounded mar-v-xs">
					Pending
				</div>
				<div class="pad-xs c-success rounded mar-v-xs">
					Close
				</div>
			</div>
			<div class="f-col-2 mar-0">
				<div class="pad-xs c-info rounded mar-v-xs">
					<center class="comp-data" data-source="<?= base_url('apps/get_today_activity/all') ?>">0</center>
				</div>
				<div class="pad-xs c-danger rounded mar-v-xs">
					<center class="comp-data" data-source="<?= base_url('apps/get_today_activity/Open') ?>">0</center>
				</div>
				<div class="pad-xs c-warning rounded mar-v-xs">
					<center class="comp-data" data-source="<?= base_url('apps/get_today_activity/Pending') ?>">0</center>
				</div>
				<div class="pad-xs c-success rounded mar-v-xs">
					<center class="comp-data" data-source="<?= base_url('apps/get_today_activity/Close') ?>">0</center>
				</div>
			</div>
		</div>
	</div>
	<div class="f-col-6 f-float-round pad-sm">
		<h5>Lapor Cepat</h5>
		<form class="form-ajax" data-uri="<?= base_url() ?>apps/act_lapor_cepat" data-redirect="<?= base_url() ?>apps/lapor_cepat">
			<div class="label">
				<div class="title">Kode Laporan</div>
				<input type="text" class="form-control rounded" name="kode_laporan" value="<?= 'TLP_'.date('ymdHis') ?>" readonly="">
			</div>
			<div class="label">
				<div class="title">Nama Pelapor</div>
				<input type="text" class="form-control rounded" name="nama_pelapor" required>
			</div>
			<button type="submit" class="btn c-base rounded pull-right">Terbitkan Tiket</button>
		</form>
	</div>
</div>
<div class="flex">
	<div class="f-col f-float-round pad-sm">
		<ul class="sub-menu-container pull-right">
			<li class="stepper item active" data-id='step-1'><a>All</a></li>
			<li class="sep">|</li>
			<li class="stepper item" data-id='step-2'><a>Open</a></li>
			<li class="sep">|</li>
			<li class="stepper item" data-id='step-3'><a>Pending</a></li>
			<li class="sep">|</li>
			<li class="stepper item" data-id='step-4'><a>Close</a></li>
		</ul>
		<h5>Lapor Cepat</h5>

		<div class="stepper-container">
			<div class="step" id="step-1">
				<div class="flex-in">
					<div class="f-col">
						<h6>Aktivitas Permintaan</h6>
					</div>
					<div class="f-col mar-0">
						<input type="text" class="form-control form-control-sm rounded inp-find" data-target="#table-all" placeholder="Cari">
					</div>
				</div>
				<hr class="mar-xs">
				<table class="table table-clr table-bordered data-table" id="table-all" data-source="<?= base_url('apps/dashboard_data/all') ?>">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th colspan="5">Permintaan</th>
							<th colspan="2">Lokasi Asal</th>
							<th rowspan="2">Status Tiket</th>
							<th rowspan="2">Status Pelayanan</th>
						</tr>
						<tr>
							<th>Tiket</th>
							<th>Waktu</th>
							<th>Nama Pelapor</th>
							<th>Unit</th>
							<th>Nama Aset</th>
							<th>Area Ruangan</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="step" id="step-2" style="display: none">
				<div class="flex-in">
					<div class="f-col">
						<h6>Aktivitas Permintaan Status Open</h6>
					</div>
					<div class="f-col mar-0">
						<input type="text" class="form-control form-control-sm rounded inp-find" data-target="#table-open" placeholder="Cari">
					</div>
				</div>
				<hr class="mar-xs">
				<table class="table table-clr table-bordered data-table" id="table-open" data-source="<?= base_url('apps/dashboard_data/Open') ?>">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th colspan="5">Permintaan</th>
							<th colspan="2">Lokasi Asal</th>
							<th rowspan="2">Status Tiket</th>
							<th rowspan="2">Status Pelayanan</th>
						</tr>
						<tr>
							<th>Tiket</th>
							<th>Waktu</th>
							<th>Nama Pelapor</th>
							<th>Unit</th>
							<th>Nama Aset</th>
							<th>Area Ruangan</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="step" id="step-3" style="display: none">
				<div class="flex-in">
					<div class="f-col">
						<h6>Aktivitas Permintaan Status Pending</h6>
					</div>
					<div class="f-col mar-0">
						<input type="text" class="form-control form-control-sm rounded inp-find" data-target="#table-pending" placeholder="Cari">
					</div>
				</div>
				<hr class="mar-xs">
				<table class="table table-clr table-bordered data-table" id="table-pending" data-source="<?= base_url('apps/dashboard_data/Pending') ?>">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th colspan="5">Permintaan</th>
							<th colspan="2">Lokasi Asal</th>
							<th rowspan="2">Status Tiket</th>
							<th rowspan="2">Status Pelayanan</th>
						</tr>
						<tr>
							<th>Tiket</th>
							<th>Waktu</th>
							<th>Nama Pelapor</th>
							<th>Unit</th>
							<th>Nama Aset</th>
							<th>Area Ruangan</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="step" id="step-4" style="display: none">
				<div class="flex-in">
					<div class="f-col">
						<h6>Aktivitas Permintaan Status Close</h6>
					</div>
					<div class="f-col mar-0">
						<input type="text" class="form-control form-control-sm rounded inp-find" data-target="#table-close" placeholder="Cari">
					</div>
				</div>
				<hr class="mar-xs">
				<table class="table table-clr table-bordered data-table" id="table-close" data-source="<?= base_url('apps/dashboard_data/Close') ?>">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th colspan="5">Permintaan</th>
							<th colspan="2">Lokasi Asal</th>
							<th rowspan="2">Status Tiket</th>
							<th rowspan="2">Status Pelayanan</th>
						</tr>
						<tr>
							<th>Tiket</th>
							<th>Waktu</th>
							<th>Nama Pelapor</th>
							<th>Unit</th>
							<th>Nama Aset</th>
							<th>Area Ruangan</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>