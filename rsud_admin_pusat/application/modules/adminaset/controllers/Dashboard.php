<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminaset');
		$this->load->model('M_sparepart','ms');
		
	}

	public function index()
	{
		$data = [
			'title' => 'Dashboard Admin Aset',
			'hal' => 'dashboard/index',
			'stokhabis' => $this->ms->jumlah(0)
		];
		$this->load->view('app', $data);
	}

	public function board()
	{
		$this->load->view('dashboard/onboard');
	}

	public function tabel()
	{
		$data = [
			'title' => 'Dashboard Admin Pusat',
			'hal' => 'dashboard/tabel',
			'stokhabis' => $this->ms->jumlah(0),
			'js' => 'dashboard/grafik'
		];
		$this->load->view('app', $data);
	}
}

/* End of file Dashboard.php */
/* Location: ./application/modules/adminaset/controllers/Dashboard.php */