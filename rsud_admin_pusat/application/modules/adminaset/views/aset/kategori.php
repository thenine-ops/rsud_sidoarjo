<div class="flex mb-4">
	<div class="f-col-5 f-float-round center pad-lg text-center">
		<div class="row">
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/kategori');?>" class="btn btn-success rounded btn-block">Kategori</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/sub_kategori');?>" class="btn btn-primary rounded btn-block">Sub Kategori</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/detail_kategori');?>" class="btn btn-primary rounded btn-block">Detail Kategori</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/aset');?>" class="btn btn-primary rounded btn-block">Data Aset</a>
			</div>
		</div>
  	</div>
</div>
<?=$this->session->flashdata('name');?>
<?php
$attr = [
	'class' => 'form-inline mb-4'
];
?>
<?=validation_errors('<small class="text-danger">','</small>');?>
<?=form_open('adminaset/aset/kategori', $attr);?>
	<label class="sr-only" for="nama">Nama</label>
	<input type="text" class="form-control form-control-lg mb-2 mr-sm-2 col-md-7 <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" placeholder="Nama Kategori">

	<button type="submit" class="btn btn-success btn-lg mb-2 rounded">Simpan</button>
</form>

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Kategori</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php $no=1; foreach($data as $d): ?>
    <tr>
      <th scope="row"><?=$no++;?></th>
      <td><?=$d->name;?></td>
      <td>
      	<a href="<?=base_url('adminaset/aset/kategori_edit/'.$d->id);?>" class="btn btn-primary btn-sm">Edit</a>
      	<a href="<?=base_url('adminaset/aset/kategori_hapus/'.$d->id);?>" class="btn btn-danger btn-sm">Hapus</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
