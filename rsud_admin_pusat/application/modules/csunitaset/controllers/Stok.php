<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('csunitaset');
		$this->load->model('M_sparepart','ms');
	}

	public function index()
	{
		$data = [
			'title' => 'STOK SPAREPART',
			'hal' => 'stok/index',
			'data' => $this->ms->stok(),
			'data2' => $this->ms->stok_khusus(),
				'js' => 'stok/js'
		];
		$this->load->view('layout', $data);
	}

	public function kosong()
	{
		$data = [
			'title' => 'STOK SPAREPART',
			'hal' => 'stok/index',
			'data' => $this->ms->stok(0),
			'data2' => $this->ms->stok_khusus(0),
				'js' => 'stok/js'
		];
		$this->load->view('layout', $data);
	}

	public function detail($id='')
	{
		if(!$id or $id<=0) redirect('csunitaset/stok','refresh');
		$data = [
			'title' => 'DETAIL SPAREPART',
			'hal' => 'stok/detail',
			'data' => $this->ms->data_by_id($id)
		];
		$this->load->view('layout', $data);
	}

	public function detail_khusus($id='')
	{
		if(!$id or $id<=0) redirect('csunitaset/stok','refresh');
		$data = [
			'title' => 'DETAIL SPAREPART',
			'hal' => 'stok/detail_khusus',
			'data' => $this->ms->data_khusus_by_id($id)
		];
		$this->load->view('layout', $data);
	}

	public function tambah($id='')
	{
		if(!$id or $id<=0) redirect('csunitaset/stok','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'TAMBAH STOK SPAREPART',
				'hal' => 'stok/tambah',
				'data' => $this->ms->data_by_id($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->tambah_stok();
			if ($cek) {
				$this->session->set_flashdata('berhasil', '<div class="alert alert-success" role="alert">Data berhasil disimpan!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal disimpan!</div>');
			}
			redirect('csunitaset/stok','refresh');
		}
	}

	public function tambah_khusus($id='')
	{
		if(!$id or $id<=0) redirect('csunitaset/stok','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'TAMBAH STOK SPAREPART',
				'hal' => 'stok/tambah_khusus',
				'data' => $this->ms->data_khusus_by_id($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->tambah_stok_khusus();
			if ($cek) {
				$this->session->set_flashdata('berhasil', '<div class="alert alert-success" role="alert">Data berhasil disimpan!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal disimpan!</div>');
			}
			redirect('csunitaset/stok','refresh');
		}
	}

}

/* End of file Stok.php */
/* Location: ./application/modules/csunitaset/controllers/Stok.php */