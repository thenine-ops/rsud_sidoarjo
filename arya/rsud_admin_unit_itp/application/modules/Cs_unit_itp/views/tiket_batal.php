<script src="<?= base_url(); ?>aset/external/js/jquery-3.4.1.min.js"></script>

<body class="dashboard-body">
    <div class="loading">
        <img src="<?= base_url(); ?>aset/image/asset/loading.gif" alt="">
    </div>
    <div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?= base_url(); ?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
    </div>
    <div class="content-container">
        <div class="col col-content padding-content">
            <h4 class="bold">TIKET PEMINDAHAN PASIEN</h4>
            <div id="informasi"></div>
            <div class="date-info f-green padding-tanggal">
                <a href="<?= site_url('Cs_unit_itp/aktifitas_pemindahan_pasien');?>" class="btn btn-sm rounded pull-right c-danger">x</a>
            </div>
            <div class="flex">
                <div class="f-col">
                    <div class="gray">Tanggal</div>
                    <button class="btn btn-outline-success w95"><?php echo date('l, d F Y', strtotime($result->transfer_date)); ?></button>
                </div>
                <div class="f-col">
                    <div class="gray">ID Tiket</div>
                    <button class="btn btn-outline-success w95">ITP_<?php echo $result->transporterticket_id; ?></button>
                </div>
                <div class="f-col-right">
                    <div class="gray">Status</div>
                    <button class="btn btn-outline-info w95 m-right0"><?php echo $result->name_activ; ?></button>
                </div>
            </div>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Pasien</div>
                </div>
            </div>
            <div class="flex f-float-round p-3">
                <div class="f-col">
                    <div class="label m-input">
                        <label class="small">Ruangan Penjemputan</label>
                        <input type="text" class="form-control" name="ruangan_penjemputan" value="<?php echo $result->name_room_origin; ?>" placeholder="Ruangan Penjemputan" disabled>
                    </div>
                    <div class="label m-input">
                        <label class="small">Ruangan Tujuan</label>
                        <input type="text" class="form-control" name="ruangan_tujuan" value="<?php echo $result->name_room_destination; ?>" placeholder="Ruangan Tujuan" disabled>
                    </div>
                    <div class="label m-input">
                        <label class="small">Nama Petugas</label>
                        <input type="text" class="form-control" name="nama_petugas" value="<?php echo $result->user_ent; ?>" placeholder="Nama Petugas" disabled>
                    </div>
                    <div class="label m-input">
                        <label class="small">Nama Pasien</label>
                        <input type="text" class="form-control" name="nama_pasien" value="<?php echo $result->patient_name; ?>" placeholder="Nama Pasien" disabled>
                    </div>
                </div>
            </div>
            <?= validation_errors(); ?>
            <?php $atrributs = array('id' => 'form_pembatalan'); ?>
            <?= form_open('csitp/cancel', $atrributs); ?>
            <input style="display:none;" id="cs" type="text" name="csrf_test_rsud" value="<?= $token; ?>">
            <input style="display:none;" id="id_ticket" type="text" name="id_ticket" value="<?= $transporterticket_id; ?>">
            <h6 class="mt-5 mb-3">Alasan Pembatalan</h6>
            <div class="flex f-float-round p-3">
                <div class="f-col">
                    <div class="label m-input">
                        <textarea class="form-control" id="alasan_pembatalan" name="alasan_pembatalan" value="" style="height: 5em;"></textarea>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-lg c-success pull-right mt-3">Simpan</button>
            <?= form_close(); ?>
        </div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?= base_url(); ?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/ganti_petugas'); ?>" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/laporan_aktivitas_petugas'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="footer-container">
        <label>Nine Cloud 2019</label>
    </div>
</body>
<script>
    $('#r_penjemputan').on('change', function() {
        var id = $('#r_penjemputan').val();
        var token = "<?php echo $this->security->get_csrf_hash(); ?>";
        var name = "<?php echo $this->security->get_csrf_token_name();; ?>";
        $.ajax({
            url: "<?php echo site_url('r_penjemputan'); ?>",
            method: "POST",
            data: {
                id: id,
                csrf_test_rsud: token
            },
            dataType: "JSON",
            success: function(response) {
                $("#area_penjemputan").val(response.area);
            }
        })
    })
    $('#r_tujuan').on('change', function() {
        var id = $('#r_tujuan').val();
        var token = "<?php echo $this->security->get_csrf_hash(); ?>";
        var name = "<?php echo $this->security->get_csrf_token_name(); ?>";
        $.ajax({
            url: "<?php echo site_url('r_tujuan'); ?>",
            method: "POST",
            data: {
                id: id,
                csrf_test_rsud: token
            },
            dataType: "JSON",
            success: function(response) {
                $("#area_tujuan").val(response.area);
            }
        })
    })
</script>