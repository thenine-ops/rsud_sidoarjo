<div class="flex mb-4">
	<div class="f-col-5 f-float-round center pad-lg text-center">
		<div class="row">
			<div class="col-md">
				<a href="<?=base_url('adminaset/khusus');?>" class="btn btn-<?=($this->uri->segment(3)=='')?'success':'primary';?> rounded btn-block">Master Spare Part</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/khusus/data');?>" class="btn btn-<?=($this->uri->segment(3)=='data')?'success':'primary';?> rounded btn-block">Spare Part Khusus</a>
			</div>
		</div>
  	</div>
</div>