<script type="text/javascript">
	$('select#inputRuangan').change(function(){
		var kamar = $('#inputRuangan').val();
		$.ajax({
			url: "<?=base_url('csunitit/tiket/getaset');?>",
		    data: {kamar:kamar},
		    method: "GET",
		    cache: false,
		    success: function(msg){
		        $("#inputAset").html(msg);
		    }
		});
	});

	$('select#inputAset').change(function(){
		var id = $('#inputAset').val();
		$.ajax({
			url: "<?=base_url('csunitit/tiket/idaset');?>",
		    data: {id:id},
		    method: "GET",
		    cache: false,
		    success: function(msg){
		        $("#inputId_asset_register").val(msg);
		    }
		});
	});

	$('select#inputAset').change(function(){
		var id = $('#inputAset').val();
		$.ajax({
			url: "<?=base_url('csunitit/tiket/namaaset');?>",
		    data: {id:id},
		    method: "GET",
		    cache: false,
		    success: function(msg){
		        $("#inputAsset_name").val(msg);
		    }
		});
	});

	$('select#inputRuangan').change(function(){
		var id = $('#inputRuangan').val();
		$.ajax({
			url: "<?=base_url('csunitit/tiket/getkategori');?>",
		    data: {id:id},
		    method: "GET",
		    cache: false,
		    success: function(msg){
		        $("#inputKategori").val(msg);
		    }
		});
	});
</script>