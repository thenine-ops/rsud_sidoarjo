<!DOCTYPE html>
<html>
<head>
	<title><?= $header_title ?></title>

	<!-- Javascript External -->
	<script src="<?= base_url() ?>prabotan/external/js/jquery.js"></script>
	<script src="<?= base_url() ?>prabotan/external/js/bootstrap.min.js"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url() ?>prabotan/internal/js/button_action.js"></script>
	<script src="<?= base_url() ?>prabotan/internal/js/modal.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/external/css/bootstrap.min.css">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/content.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/button_action.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/modal.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/loading.css">
</head>
<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>prabotan/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-base">
			<?= $header_title ?>
		</div>
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init"><?= get_acronym($user_log['name']) ?></label>
				<img src="<?= base_url() ?>prabotan/image/profile_photo/-.jpg" alt="">
			</div>
			<label class="account-name"><?= ucwords($user_log['name']) ?> / <?= ucwords($user_log['role']) ?></label>
		</div>
		<a href="<?= base_url().'dashboard' ?>" class="pull-right rounded c-danger">Close</a>
	</div>