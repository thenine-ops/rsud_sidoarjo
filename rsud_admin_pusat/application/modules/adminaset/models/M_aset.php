<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_aset extends CI_Model {

	public function data_aset()
	{
		return $this->db->order_by('name','asc')->get('assets')->result();
	}

	public function hapus($tabel, $id)
	{
		return $this->db->delete($tabel, ['id'=>$id]);
	}

	public function kategori()
	{
		return $this->db->order_by('name','asc')->get('asset_categories')->result();
	}

	public function kategori_by_id($id)
	{
		return $this->db->where('id',$id)->get('asset_categories')->row();
	}
	
	public function kategori_satuan($id, $field='name')
	{
		$data = $this->db->where('id',$id)->get('asset_categories')->row_array();
		return $data[$field];
	}

	public function kategori_tambah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['nama']))
		];
		return $this->db->insert('asset_categories', $object);
	}

	public function kategori_ubah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['nama']))
		];
		$where = [
			'id' => htmlspecialchars(trim($post['id']))
		];
		return $this->db->update('asset_categories', $object, $where);
	}

















	public function sub_kategori()
	{
		return $this->db->select('kat.name as namakategori, sub.name as namasubkategori, sub.id')
				 ->from('asset_sub_categories sub')
				 ->join('asset_categories kat','kat.id=sub.assetcategory_id')
				 ->order_by('kat.name','asc')
				 ->order_by('sub.name','asc')
				 ->get()
				 ->result();
	}

	public function sub_kategori_by_id($id)
	{
		return $this->db->where('id',$id)->get('asset_sub_categories')->row();
	}

	public function sub_kategori_by_idaset($idaset)
	{
		return $this->db->select('s.id, s.name')
				 ->from('asset_sub_categories s')
				 ->join('assets a','s.id=a.assetsubcategory_id')
				 ->get()
				 ->row();
	}

	public function sub_kategori_by_idproduk($idproduk)
	{
		return $this->db->select('s.id, s.name')
				 ->from('asset_sub_categories s')
				 ->join('asset_products a','s.id=a.assetsubcategory_id')
				 ->get()
				 ->row();
	}

	public function sub_kategori_tambah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['nama'])),
			'assetcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'unit_id' => $this->session->unit_id
		];
		return $this->db->insert('asset_sub_categories', $object);
	}

	public function sub_kategori_ubah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['nama'])),
			'assetcategory_id' => htmlspecialchars(trim($post['kategori'])),
		];
		$where = [
			'id' => htmlspecialchars(trim($post['id']))
		];
		return $this->db->update('asset_sub_categories', $object, $where);
	}



















	public function detail_kategori()
	{
		return $this->db->select('a.name, a.quantity, a.description, kat.name as namakategori, sub.name as namasubkategori, a.id')
				 ->from('assets a')
				 ->join('asset_sub_categories sub','sub.id=a.assetsubcategory_id')
				 ->join('asset_categories kat','kat.id=a.assetcategory_id')
				 ->order_by('a.name','asc')
				 ->order_by('kat.name','asc')
				 ->order_by('sub.name','asc')
				 ->get()
				 ->result();
	}

	public function detail_kategori_by_id($id)
	{
		return $this->db->where('id',$id)->get('assets')->row();
	}

	public function detail_kategori_by_idproduk($idproduk)
	{
		return $this->db->select('s.id, s.name')
				 ->from('assets s')
				 ->where('a.id',$idproduk)
				 ->join('asset_products a','s.id=a.asset_id')
				 ->get()
				 ->row();
	}

	public function detail_kategori_tambah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['detail'])),
			'description' => htmlspecialchars(trim($post['deskripsi'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'assetcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'assetsubcategory_id' => htmlspecialchars(trim($post['subkategori'])),
			'unit_id' => $this->session->unit_id,
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		return $this->db->insert('assets', $object);
	}

	public function detail_kategori_ubah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['detail'])),
			'description' => htmlspecialchars(trim($post['deskripsi'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'assetcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'assetsubcategory_id' => htmlspecialchars(trim($post['subkategori'])),
			'unit_id' => $this->session->unit_id,
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		$where = [
			'id' => htmlspecialchars(trim($post['id']))
		];
		return $this->db->update('assets', $object, $where);
	}














	public function aset()
	{
		$this->db->select('a.id, a.id_asset_register, a.name, a.type, a.category_name, ac.name as kondisi')
				 ->from('asset_products a')
				 ->join('asset_conditions ac','ac.id=a.assetcondition_id');
		return $this->db->order_by('a.name','asc')->get()->result();
	}

	public function aset_by_id($id)
	{
		return $this->db->where('id',$id)->get('asset_products')->row();
	}

	public function aset_tambah()
	{
		$post = $this->input->post();
		$data = [
			'asset_id' => htmlspecialchars(trim($post['detail'])),
			'assetsubcategory_id' => htmlspecialchars(trim($post['subkategori'])),
			'assetcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'assetcondition_id' => htmlspecialchars(trim($post['kondisi'])),
			'unit_id' => htmlspecialchars(trim($post['unit'])),
			'id_asset_register' => $this->idaset(),
			'category_name' => $this->kategori_satuan(htmlspecialchars(trim($post['kategori']))),
			'name' => htmlspecialchars(trim($post['nama'])),
			'type' => htmlspecialchars(trim($post['tipe'])),
			'factory_serial_number' => htmlspecialchars(trim($post['serial'])),
			'description' => htmlspecialchars(trim($post['deskripsi'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		$this->db->set('procurement_date','now()');
		$this->db->set('active_date','now()');
		return $this->db->insert('asset_products', $data);
	}

	public function aset_ubah()
	{
		$post = $this->input->post();
		$data = [
			'asset_id' => htmlspecialchars(trim($post['detail'])),
			'assetsubcategory_id' => htmlspecialchars(trim($post['subkategori'])),
			'assetcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'assetcondition_id' => htmlspecialchars(trim($post['kondisi'])),
			'unit_id' => htmlspecialchars(trim($post['unit'])),
			'id_asset_register' => $this->idaset(),
			'category_name' => $this->kategori_satuan(htmlspecialchars(trim($post['kategori']))),
			'name' => htmlspecialchars(trim($post['nama'])),
			'type' => htmlspecialchars(trim($post['tipe'])),
			'factory_serial_number' => htmlspecialchars(trim($post['serial'])),
			'description' => htmlspecialchars(trim($post['deskripsi'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		$this->db->set('procurement_date','now()');
		$this->db->set('active_date','now()');
		$where = ['id'=>htmlspecialchars(trim($post['id']))];
		return $this->db->update('asset_products', $data, $where);
	}

	public function jum_aset()
	{
		$data = $this->db->select('id+1 as id')->order_by('id','desc')->get('asset_products')->row();
		return $data->id;
	}

	public function idaset()
	{
		return "IT-".date('Ymd').'00'.$this->jum_aset();
	}















	public function kondisi()
	{
		return $this->db->order_by('name','asc')->get('asset_conditions')->result();
	}

	public function getsubkategori($id)
	{
		return $this->db->where('assetcategory_id', $id)->get('asset_sub_categories')->result();
	}

	public function getdetailkategori($kat,$sub)
	{
		return $this->db->where('assetcategory_id', $kat)->where('assetsubcategory_id', $sub)->get('assets')->result();
	}

}

/* End of file M_aset.php */
/* Location: ./application/modules/adminaset/models/M_aset.php */