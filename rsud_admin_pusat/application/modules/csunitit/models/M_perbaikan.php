<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_perbaikan extends CI_Model {

	public function data()
	{
		$bulan = date('m');
		$tahun = date('Y');
		return $this->db->select('
									tiket.id_ticket_register AS tiket,
									tiket.ticket_date AS waktu,
									detail.name AS keterangan,
									akun.name AS pelapor,
									katkamar.name AS ruangan,
									produk.name AS namaaset,
									area.name AS area,
									status.name AS statustiket,
									aktifitas.name AS statuspelayanan
								')
						->from('troubleshooting_tickets tiket')
						->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						->join('user_accounts akun','akun.id=tiket.useradmin_id')
						->join('room_categories katkamar','katkamar.id=tiket.roomcategory_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->join('asset_products produk','produk.id_asset_register=trobel.id_asset_register')
						->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
						->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
						->where('EXTRACT(YEAR FROM tiket.ticket_date)=', $tahun)
						->where('EXTRACT(MONTH FROM tiket.ticket_date)=', $bulan)
						->where('tiket.troubleshootingtype_id', 1)
						->get()
						->result();
	}

	public function semua()
	{
		return $this->db->select('
									tiket.id_ticket_register AS tiket,
									tiket.ticket_date AS waktu,
									detail.name AS keterangan,
									akun.name AS pelapor,
									katkamar.name AS ruangan,
									produk.name AS namaaset,
									area.name AS area,
									status.name AS statustiket,
									aktifitas.name AS statuspelayanan
								')
						->from('troubleshooting_tickets tiket')
						->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						->join('user_accounts akun','akun.id=tiket.useradmin_id')
						->join('room_categories katkamar','katkamar.id=tiket.roomcategory_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->join('asset_products produk','produk.id_asset_register=trobel.id_asset_register')
						->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
						->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
						->where('tiket.troubleshootingtype_id', 1)
						->get()
						->result();
	}

	public function filter()
	{
		$post = $this->input->post();
		$bulan = $post['bulan'];
		$tahun = $post['tahun'];
		$ruangan = $post['ruangan'];
		$status = $post['status'];
		return $this->db->select('
									tiket.id_ticket_register AS tiket,
									tiket.ticket_date AS waktu,
									detail.name AS keterangan,
									akun.name AS pelapor,
									katkamar.name AS ruangan,
									produk.name AS namaaset,
									area.name AS area,
									status.name AS statustiket,
									aktifitas.name AS statuspelayanan
								')
						->from('troubleshooting_tickets tiket')
						->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						->join('user_accounts akun','akun.id=tiket.useradmin_id')
						->join('room_categories katkamar','katkamar.id=tiket.roomcategory_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->join('asset_products produk','produk.id_asset_register=trobel.id_asset_register')
						->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
						->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
						->where('EXTRACT(YEAR FROM tiket.ticket_date)=', $tahun)
						->where('EXTRACT(MONTH FROM tiket.ticket_date)=', $bulan)
						->where('tiket.troubleshootingstatus_id', $status)
						->where('tiket.roomcategory_id', $ruangan)
						->where('tiket.troubleshootingtype_id', 1)
						->get()
						->result();
	}

}

/* End of file M_perbaikan.php */
/* Location: ./application/modules/csunitit/models/M_perbaikan.php */