<a href="<?=base_url('csunitaset/stok');?>" <?php if($this->uri->segment(3)==''){ ?>class="text-success" style="font-weight: bold;"<?php } ?>>Stok Sparepart</a> | <a href="<?=base_url('csunitaset/stok/kosong');?>" <?php if($this->uri->segment(3)!=''){ ?>class="text-success" style="font-weight: bold;"<?php } ?>>Stok Sparepart Kosong</a>

<div class="table-responsive">
	<table class="table table-striped table-bordered table-md mt-4">
	  <thead class="bg-success text-center">
	    <tr>
	      <th scope="col" rowspan="2">No</th>
	      <th scope="col" rowspan="2">ID Spare Part</th>
	      <th scope="col" rowspan="2">Nama Spare Part</th>
	      <th scope="col" colspan="4">Detail</th>
	      <th scope="col" rowspan="2">Stok</th>
	      <th scope="col" rowspan="2">Aksi</th>
	    </tr>
	    <tr>
	      <th scope="col">Jenis</th>
	      <th scope="col">Kategori</th>
	      <th scope="col">Sub Kategori</th>
	      <th scope="col">Detail Kategori</th>
	    </tr>
	  </thead>
	  <tbody>
	  	<?php $no=1; foreach($data as $d): ?>
	    <tr>
	      <td><?=$no++;?></td>
	      <td><?=$d->id_sparepart_register;?></td>
	      <td><?=$d->spname;?></td>
	      <td><?=$d->type;?></td>
	      <td><?=$d->scname;?></td>
	      <td><?=$d->sname;?></td>
	      <td><?=$d->aname;?></td>
	      <td><?=$d->quantity;?></td>
	      <td>
	      	<a href="<?=base_url('csunitaset/stok/detail/'.$d->id);?>" class="btn btn-primary btn-sm" alt="Detail" title="Detail"><i class="fa fa-search"></i></a> 
	      	<a href="<?=base_url('csunitaset/stok/tambah/'.$d->id);?>" class="btn btn-success btn-sm" alt="Tambah" title="Tambah"><i class="fa fa-plus"></i></a>
	      </td>
	    </tr>
	    <?php endforeach; ?>
	  	<?php foreach($data2 as $d): ?>
	    <tr>
	      <td><?=$no++;?></td>
	      <td><?=$d->id_sparepart_register;?></td>
	      <td><?=$d->spgname;?></td>
	      <td><?=$d->type;?></td>
	      <td><?=$d->ascname;?></td>
	      <td><?=$d->sgname;?></td>
	      <td>-</td>
	      <td><?=$d->quantity;?></td>
	      <td>
	      	<a href="<?=base_url('csunitaset/stok/detail_khusus/'.$d->id);?>" class="btn btn-primary btn-sm" alt="Detail" title="Detail"><i class="fa fa-search"></i></a> 
	      	<a href="<?=base_url('csunitaset/stok/tambah_khusus/'.$d->id);?>" class="btn btn-success btn-sm" alt="Tambah" title="Tambah"><i class="fa fa-plus"></i></a>
	      </td>
	    </tr>
	    <?php endforeach; ?>
	  </tbody>
	</table>
</div>