<div class="flex">
	<div class="f-col-7 f-float-round pad-sm my-4">
		<?=form_open('csunitit/perbaikan');?>

		<div class="row">
			<div class="col-md">
				<select name="bulan" id="inputBulan" class="form-control" required="required">
					<option value="">Pilih Bulan</option>
					<?php
					$bulan = [
						'01' => 'Januari',
						'02' => 'Februari',
						'03' => 'Maret',
						'04' => 'April',
						'05' => 'Mei',
						'06' => 'Juni',
						'07' => 'Juli',
						'08' => 'Agustus',
						'09' => 'September',
						'10' => 'Oktober',
						'11' => 'November',
						'12' => 'Desember'
					];
					foreach($bulan as $key => $value): ?>
					<option value="<?=$key;?>"><?=$value;?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-md">
				<select name="tahun" id="inputTahun" class="form-control" required="required">
					<option value="">Pilih Tahun</option>
					<?php
					$tahun = [2020, 2021, 2022, 2023, 2024, 2025];
					foreach($tahun as $tahun): ?>
					<option value="<?=$tahun;?>"><?=$tahun;?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-md">
				<select name="status" id="inputStatus" class="form-control" required="required">
					<option value="">Pilih Status</option>
					<?php foreach($status as $status): ?>
					<option value="<?=$status->id;?>"><?=$status->name;?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div class="row">
			<div class="col-8">
				<select name="ruangan" id="inputRuangan" class="form-control" required="required">
					<option value="">Pilih Ruangan</option>
					<?php foreach($ruangan as $ruangan): ?>
					<option value="<?=$ruangan->id;?>">Ruangan <?=$ruangan->name;?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-4">
				<button type="submit" class="btn btn-danger rounded">Lock</button>
			</div>
		</div>
		
		<?=form_close();?>
	</div>
</div>

<h6 style="text-transform: uppercase;">Perbaikan Peridode Bulan <?=bulan($this->input->post('bulan'));?> <?=$this->input->post('tahun');?><br>Ruang <?=$hasil->name;?></h6>
<div class="flex">
	<div class="f-col-7 f-float-round pad-sm mt-3">
		<table class="table table-bordered">
			<tr class="bg-success">
				<td rowspan="2">No</td>
				<td colspan="5" class="text-center">Permintaan</td>
				<td colspan="2" class="text-center">Lokasi Aset</td>
				<td rowspan="2" align="center" valign="middle">Status Tiket</td>
				<td rowspan="2">Status Pelayanan</td>
			</tr>
			<tr class="bg-success">
				<td>Tiket</td>
				<td>Waktu</td>
				<td>Nama Pelapor</td>
				<td>Ruangan</td>
				<td>Nama Aset</td>
				<td>Area Ruangan</td>
				<td>Keterangan</td>
			</tr>
			<?php if(count($data)==0){ ?>
			<tr>
				<td colspan="10" class="text-center"><h5>Tidak ada data</h5></td>
			</tr>
			<?php } ?>
			<?php $no=1; foreach($data as $d): ?>
			<tr>
				<td><?=$no++;?></td>
				<td><?=$d->tiket;?></td>
				<td><?=date_indo($d->waktu);?></td>
				<td><?=$d->pelapor;?></td>
				<td><?=$d->ruangan;?></td>
				<td><?=$d->namaaset;?></td>
				<td><?=$d->area;?></td>
				<td><?=$d->keterangan;?></td>
				<td><?=$d->statustiket;?></td>
				<td><?=$d->statuspelayanan;?></td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>