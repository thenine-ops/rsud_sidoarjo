<?=$this->session->flashdata('name');?>
<div class="mx-3">
    <?=form_open();?>
      <input type="hidden" name="id" id="inputId" class="form-control" value="<?=$data->id;?>">
        <div class="form-group">
          <label for="kategori">Spare Part Master</label>
          <select class="form-control <?=(form_error('kategori'))?'is-invalid':'';?>" name="kategori" id="kategori">
            <option>Pilih Spare Part</option>
            <?php foreach($sp as $k): ?>
            <option value="<?=$k->id;?>" <?=($data->sparepartgeneral_id==$k->id)?'selected':'';?>><?=$k->name;?></option>
            <?php endforeach; ?>
          </select>
          <?=form_error('kategori','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Produk Spare Part Khusus</label>
            <input type="text" class="form-control <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" aria-describedby="nama" placeholder="Nama Produk Spare Part" value="<?=$data->name;?>">
            <?=form_error('nama','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="type">Type</label>
          <input type="text" class="form-control <?=(form_error('type'))?'is-invalid':'';?>" name="type" id="type" aria-describedby="type" placeholder="Type Spare Part" value="<?=$data->type;?>">
          <?=form_error('type','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="description">Deskripsi</label>
          <textarea class="form-control <?=(form_error('description'))?'is-invalid':'';?>" name="description" id="description" rows="3" placeholder="Deskripsi Spare Part Khusus"><?=$data->description;?></textarea>
          <?=form_error('description','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="quantity">Quantity</label>
          <input type="text" class="form-control <?=(form_error('quantity'))?'is-invalid':'';?>" name="quantity" id="quantity" aria-describedby="quantity" placeholder="Quantity" value="<?=$data->quantity;?>">
          <?=form_error('quantity','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="image">Gambar</label>
          <input type="file" class="form-control <?=(form_error('image'))?'is-invalid':'';?>" name="image" id="image" aria-describedby="image">
          <?=form_error('image','<small class="text-danger">','</small>');?>
        </div>
    <button type="submit" class="btn btn-success btn-lg">Submit</button>
    <a href="<?=base_url('adminaset/khusus/data');?>" class="btn btn-dark btn-lg">Kembali</a>
    </form>
</div>