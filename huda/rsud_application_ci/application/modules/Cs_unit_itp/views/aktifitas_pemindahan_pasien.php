<body class="dashboard-body">
    <div class="loading">
        <img src="<?= base_url(); ?>aset/image/asset/loading.gif" alt="">
    </div>
    <div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?= base_url(); ?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
        <a href="" class="btn btn-danger rounded pull-right">close</a>
    </div>
    <div class="content-container">
        <div class="col col-content padding-content">
            <h4 class="bold">DASHBOARD</h4>
            <div class="date-info f-green padding-tanggal">
                Jumat, 13 Maret 2020
            </div><br />
            <div class="flex">
                <div class="f-col-4 f-float-round pad-sm">
                    <div class="head-form-control">Pelayanan Hari Ini</div><br>
                    <div class="flex">
                        <div class="f-col-9">
                            <button class="btn btn-sm btn-outline-primary w95 rounded">Permintaan</button>
                        </div>
                        <div class="f-col-2">
                            <button class="btn btn-sm btn-outline-primary w95 rounded"><?= $permintaan; ?></button>
                        </div>
                    </div>
                    <div class="flex top15">
                        <div class="f-col-9">
                            <button class="btn btn-sm btn-outline-warning w95 rounded">Terjadwal</button>
                        </div>
                        <div class="f-col-2">
                            <button class="btn btn-sm btn-outline-warning w95 rounded"><?= $terjadwal; ?></button>
                        </div>
                    </div>
                    <div class="flex top15">
                        <div class="f-col-9">
                            <button class="btn btn-sm btn-outline-danger w95 rounded">Selesai</button>
                        </div>
                        <div class="f-col-2">
                            <button class="btn btn-sm btn-outline-danger w95 rounded"><?= $selesai; ?></button>
                        </div>
                    </div>
                </div>
                <div class="f-col-5 f-float-round pad-sm" style="margin-left: 50px;">
                    <div class="head-form-control">Progres Pelayanan Hari Ini</div><br>
                    <div class="flex">
                        <div class="f-col">
                            <input type="text" class="form-control" name="" id="" placeholder="CSIT_APDSFAKSD">
                        </div>
                    </div>
                    <div class="flex">
                        <div class="f-col">
                            <input type="text" class="form-control" name="" id="" placeholder="Nama Pelapor">
                        </div>
                    </div>
                    <div class="flex">
                        <div class="f-col">
                            <select name="" class="form-control" id="">
                                <option value="" selected disabled>Nama Ruangan</option>
                            </select>
                        </div>
                    </div>
                    <a href="<?= site_url('csitp/pindah_pasien');?>" class="btn btn-sm c-success pull-right">Terbitkan Tiket</a>
                </div>
            </div>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Aktifitas Harian Pemindahan Pasien</div>
                </div>
            </div>
            <div class="flex">
                <div class="f-col f-float-round padding-tanggal">
                <?php $attributes = array("id"=>"form1");
                echo form_open("Cs_unit_itp/aktifitas_pemindahan_pasien",$attributes); ?>
                    <input placeholder="Cari Tanggal" class="form-control pull-right" type="text" onfocus="(this.type='date')"
                            id="date" name="tgl" style="width:400px"><br><br>
                <?php echo form_close();?>
                    <table class="table table-green table-bordered">
                        <col>
                        <colgroup span="2"></colgroup>
                        </col>
                        <tr class="c-success">
                            <th colspan="1" rowspan="2" scope="colgroup">No</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
                            <th colspan="1" rowspan="2" scope="colgroup">ID Tiket Permintaan</th>
                            <th colspan="2" scope="colgroup">Ruangan Penjemputan</th>
                            <th colspan="2" scope="colgroup">Ruangan Tujuan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Petugas</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Pasien</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status Tiket</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status Pelayanan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Action</th>
                        </tr>
                        <tr class="c-success">
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                        </tr>
                        <?php
                        $no = 1;
                        foreach($result as $row) { ;?>
                        <tr>
                            <td><?= $no++;?></td>
                            <td><?php $tgl = $row->transfer_date ; $hasil =  explode(" ",$tgl); echo $hasil[0] ;?></td>
                            <td><?= $row->transporterticket_id;?></td>
                            <td><?= $row->name_room_origin;?></td>
                            <td><?= $row->name_area_origin;?></td>
                            <td><?= $row->name_room_destination;?></td>
                            <td><?= $row->name_area_destination;?></td>
                            <td><?= $row->user_ent;?></td>
                            <td><?= $row->patient_name;?></td>
                            <td><?= $row->name_status;?></td>
                            <td><?= $row->name_activ;?></td>
                            <td> <a href="<?=site_url('Cs_unit_itp/detail_pemindahan_pasien/'.$row->transporterticket_id);?>">detail</a></td>
                        </tr>
                        <?php } ;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?= base_url(); ?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASEIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <script>
        $("#date").on('keypress', function(e){
            if(e.which == 13){
                $("#form1").submit();
            }
        })
    </script>