<?php $this->load->view('sparepart/menu');?>

<?=$this->session->flashdata('name');?>
<a href="<?=base_url('adminaset/sparepart/umum_tambah');?>" class="btn btn-success btn-lg mb-3 rounded">Tambah Data</a>

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Serial</th>
      <th scope="col">Nama Spare Part</th>
      <th scope="col">Tipe</th>
      <th scope="col">Quantity</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php $no=1; foreach($data as $d): ?>
    <tr>
      <th scope="row"><?=$no++;?></th>
      <td><?=$d->serial;?></td>
      <td><?=$d->nama;?></td>
      <td><?=$d->type;?></td>
      <td><?=$d->quantity;?></td>
      <td><?=$d->description;?></td>
      <td>
      	<a href="<?=base_url('adminaset/sparepart/umum_detail/'.$d->id);?>" class="btn btn-success btn-sm rounded">Detail</a>
      	<a href="<?=base_url('adminaset/sparepart/umum_edit/'.$d->id);?>" class="btn btn-primary btn-sm rounded">Edit</a>
      	<a href="<?=base_url('adminaset/sparepart/umum_hapus/'.$d->id);?>" class="btn btn-danger btn-sm rounded">Hapus</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
