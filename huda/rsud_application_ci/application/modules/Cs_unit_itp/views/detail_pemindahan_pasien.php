<body class="dashboard-body">
    <div class="loading">
        <img src="<?=base_url();?>aset/image/asset/loading.gif" alt="">
    </div>
    <div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?=base_url();?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
    </div>
    <div class="content-container">
        <div class="col col-content padding-content">
            <h4 class="bold">DETAIL AKTIFITAS</h4>
            <div class="date-info f-green padding-tanggal">
                <a href="<?= site_url('Cs_unit_itp/aktifitas_pemindahan_pasien');?>" class="btn btn-sm rounded pull-right c-danger">x</a>
            </div>
            <div class="flex">
                <div class="f-col">
                    <div class="gray">Tanggal</div>
                    <button class="btn btn-outline-success w95"><?php $tgl = $result->transfer_date; $hasil =  explode(" ",$tgl); echo $hasil[0] ;?></button>
                </div>
                <div class="f-col">
                    <div class="gray">ID Tiket</div>
                    <button class="btn btn-outline-success w95"><?= "ITP_".$result->transporterticket_id;?></button>
                </div>
                <div class="f-col-right">
                    <div class="gray">Status</div>
                    <button class="btn btn-outline-success w95 m-right0"><?= $result->name_status;?></button> 
                </div>
            </div>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Pasien</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->patient_name;?>" disabled>
                    </div>
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->patient_contact;?>"disabled>
                    </div>
                </div>
            </div><br>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Ruangan Penjemputan</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->name_room_origin;?>" disabled>
                    </div>
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->name_area_origin;?>" disabled>
                    </div>
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->name_room_origin;?>" disabled>
                    </div>
                </div>
            </div><br>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Ruangan Tujuan</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->name_room_destination;?>" disabled>
                    </div>
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->name_area_destination;?>" disabled>
                    </div>
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->name_room_destination;?>" disabled>
                    </div>
                </div>
            </div><br>
            <div class="flex">
                <div class="f-col-9">
                    <div class="head-form-control">Petugas</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="label m-input">
                        <input type="text" class="form-control" name="nama" value="<?= $result->user_ent;?>" disabled>
                    </div>
                </div>
            </div><br>
        </div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?=base_url();?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?=base_url();?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?=base_url();?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASEIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="profile_edit.html" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?=base_url();?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
    </div>