<div class="flex mb-4">
	<div class="f-col-5 f-float-round center pad-lg text-center">
		<div class="row">
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/kategori');?>" class="btn btn-primary rounded btn-block">Kategori</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/sub_kategori');?>" class="btn btn-primary rounded btn-block">Sub Kategori</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/detail_kategori');?>" class="btn btn-success rounded btn-block">Detail Kategori</a>
			</div>
			<div class="col-md">
				<a href="<?=base_url('adminaset/aset/aset');?>" class="btn btn-primary rounded btn-block">Data Aset</a>
			</div>
		</div>
  	</div>
</div>
<?=$this->session->flashdata('name');?>
<a href="<?=base_url('adminaset/aset/detail_kategori_tambah');?>" class="btn btn-success btn-lg rounded mb-3">Tambah Data</a>
<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Detail Kategori</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Qty</th>
      <th scope="col">Nama Sub Kategori</th>
      <th scope="col">Nama Kategori</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php $no=1; foreach($data as $d): ?>
    <tr>
      <th scope="row"><?=$no++;?></th>
      <td><?=$d->name;?></td>
      <td><?=$d->description;?></td>
      <td><?=$d->quantity;?></td>
      <td><?=$d->namasubkategori;?></td>
      <td><?=$d->namakategori;?></td>
      <td>
      	<a href="<?=base_url('adminaset/aset/detail_kategori_edit/'.$d->id);?>" class="btn btn-primary btn-sm">Edit</a>
      	<a href="<?=base_url('adminaset/aset/detail_kategori_hapus/'.$d->id);?>" class="btn btn-danger btn-sm">Hapus</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
