<h3>UBAH PROFIL</h3>
<div class="flex">
	<div class="f-col">
		<form class="form-ajax" data-uri="<?= base_url() ?>account/act_edit_profile" data-redirect="<?= base_url() ?>account">
			<div class="label">
				<div class="label-header">Nama</div>
				<input type="hidden" name="id_account_register" value="<?= @$user_log['id_account_register'] ?>">
				<input type="text" class="form-control" name="name" value="<?= $user_log['name'] ?>">
			</div>
			<div class="label">
				<div class="label-header">Email</div>
				<input type="email" class="form-control" name="email" value="<?= $user_log['email'] ?>">
			</div>
			<div class="label">
				<div class="label-header">Nomor Telepon</div>
				<input type="text" class="form-control input number" name="phone" value="<?= $user_log['phone'] ?>">
			</div>
			<button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
		</form>
	</div>
</div>