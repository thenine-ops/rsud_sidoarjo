<?=$this->session->flashdata('name');?>
<?=form_open_multipart('adminaset/aset/aset_tambah');?>
  <div class="form-group row">
    <label for="inputKategori" class="col-sm-2 col-form-label">Kategori</label>
    <div class="col-sm-10">
      <select name="kategori" id="inputKategori" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('kategori'))?'is-invalid':'';?>">
        <option value="">Pilih Kategori</option>
        <?php foreach($kategori as $k): ?>
        <option value="<?=$k->id;?>"><?=$k->name;?></option>
        <?php endforeach; ?>
      </select>
      <?=form_error('kategori','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputSubkategori" class="col-sm-2 col-form-label">Sub Kategori</label>
    <div class="col-sm-10">
      <select name="subkategori" id="inputSubkategori" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('subkategori'))?'is-invalid':'';?>">
        <option value="">Pilih Sub Kategori</option>
      </select>
      <?=form_error('subkategori','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Detail Kategori</label>
    <div class="col-sm-10">
      <select name="detail" id="inputDetail" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('detail'))?'is-invalid':'';?>">
      	<option value="">Pilih Detail Kategori</option>
      </select>
      <?=form_error('detail','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputDetail" class="col-sm-2 col-form-label">Kondisi Aset</label>
    <div class="col-sm-10">
      <select name="kondisi" id="inputDetail" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('kondisi'))?'is-invalid':'';?>">
        <option value="">Pilih Kondisi Aset</option>
        <?php foreach($kondisi as $k): ?>
        <option value="<?=$k->id;?>"><?=$k->name;?></option>
        <?php endforeach; ?>
      </select>
      <?=form_error('kondisi','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="unit" class="col-sm-2 col-form-label">Wewenang Unit</label>
    <div class="col-sm-10">
      <select name="unit" id="unit" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('unit'))?'is-invalid':'';?>">
        <option value="">Pilih Unit</option>
        <?php foreach($unit as $k): ?>
        <option value="<?=$k->id;?>"><?=$k->name;?></option>
        <?php endforeach; ?>
      </select>
      <?=form_error('unit','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="nama" class="col-sm-2 col-form-label">Nama Aset</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('nama'))?'is-invalid':'';?>" id="nama" placeholder="Nama Aset" name="nama">
      <?=form_error('nama','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="tipe" class="col-sm-2 col-form-label">Tipe Aset</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('tipe'))?'is-invalid':'';?>" id="tipe" placeholder="Tipe Aset" name="tipe">
      <?=form_error('tipe','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="serial" class="col-sm-2 col-form-label">Serial Number</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('serial'))?'is-invalid':'';?>" id="serial" placeholder="Serial Number" name="serial">
      <?=form_error('serial','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('deskripsi'))?'is-invalid':'';?>" id="deskripsi" placeholder="Deskripsi Aset" name="deskripsi">
      <?=form_error('deskripsi','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="tahun" class="col-sm-2 col-form-label">Tahun Pengadaan</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('tahun'))?'is-invalid':'';?>" id="tahun" placeholder="Tahun Pengadaan Aset" name="tahun">
      <?=form_error('tahun','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="file" class="col-sm-2 col-form-label">Gambar</label>
    <div class="col-sm-10">
      <input type="file" class="form-control form-control-lg <?=(form_error('file'))?'is-invalid':'';?>" id="file" placeholder="file Pengadaan Aset" name="file" required>
      <?=form_error('file','<small class="text-danger">','</small>');?>
    </div>
  </div>


  <div class="form-group row">
    <div class="col-sm-10 offset-2">
      <button type="submit" class="btn btn-success rounded btn-lg">Simpan</button> 
      <a href="<?=base_url('adminaset/aset/aset');?>" class="btn btn-dark rounded btn-lg">Kembali</a>
    </div>
  </div>
<?=form_close();?>