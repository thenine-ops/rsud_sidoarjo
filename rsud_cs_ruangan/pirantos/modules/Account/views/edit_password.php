<h3>UBAH KATA SANDI</h3>
<div class="flex">
	<div class="f-col">
		<form class="form-ajax" data-uri="<?= base_url() ?>account/act_edit_password" data-redirect="<?= base_url() ?>account">
			<div class="label">
				<div class="label-header">Kata Sandi Lama</div>
				<input type="hidden" name="id_account_register" value="<?= @$user_log['id_account_register'] ?>">
				<input type="password" class="form-control" name="old_password">
			</div>
			<div class="label">
				<div class="label-header">Kata Sandi Baru</div>
				<input type="password" class="form-control" name="new_password">
			</div>
			<div class="label">
				<div class="label-header">Konfirmasi Kata Sandi Baru</div>
				<input type="password" class="form-control" name="new_password2">
			</div>
			<button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
		</form>
	</div>
</div>