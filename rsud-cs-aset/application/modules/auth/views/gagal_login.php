<script src="<?=base_url('aset');?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript">
  $(function() {
      Swal.fire({
        type: 'error',
        title: '<h3>Login Gagal!</h3>',
        text: 'Mohon maaf, username atau password Anda tidak cocok dalam database sistem.\n\nSilahkan melakukan recovery akun jika Anda lupa detail akun.',
        confirmButtonText: '<a class="text-white" href="<?=base_url('auth');?>">OK</a>'
      });
  });
</script>