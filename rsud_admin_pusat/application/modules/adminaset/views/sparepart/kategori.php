<?php $this->load->view('sparepart/menu');?>
<?=$this->session->flashdata('name');?>
<?php
$attr = [
	'class' => 'form-inline mb-4'
];
?>
<?=validation_errors('<small class="text-danger">','</small>');?>
<?=form_open('adminaset/sparepart/kategori', $attr);?>
	<label class="sr-only" for="nama">Nama</label>
	<input type="text" class="form-control form-control-lg mb-2 mr-sm-2 col-md-7 <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" placeholder="Nama Kategori Spare Part">

	<button type="submit" class="btn btn-success btn-lg mb-2 rounded">Simpan</button>
</form>

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Kategori</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php $no=1; foreach($data as $d): ?>
    <tr>
      <th scope="row"><?=$no++;?></th>
      <td><?=$d->name;?></td>
      <td>
      	<a href="<?=base_url('adminaset/sparepart/kategori_edit/'.$d->id);?>" class="btn btn-primary btn-sm rounded">Edit</a>
      	<a href="<?=base_url('adminaset/sparepart/kategori_hapus/'.$d->id);?>" class="btn btn-danger btn-sm rounded">Hapus</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
