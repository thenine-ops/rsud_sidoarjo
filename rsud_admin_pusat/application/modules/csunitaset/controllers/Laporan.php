<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('csunitaset');
	}

	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('sparepart', 'Spare Part', 'trim|required|numeric');
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Laporan',
				'hal' => 'laporan/index',
				'js' => 'dashboard/js',
				'css' => 'dashboard/css'
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->tambah_stok();
			if ($cek) {
				$this->session->set_flashdata('berhasil', '<div class="alert alert-success" role="alert">Data berhasil disimpan!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal disimpan!</div>');
			}
			redirect('csunitaset/laporan','refresh');
		}
	}

}

/* End of file Laporan.php */
/* Location: ./application/modules/csunitaset/controllers/Laporan.php */