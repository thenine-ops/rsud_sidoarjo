<body class="dashboard-body">
	<div class="loading">
        <img src="<?=base_url();?>aset/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?=base_url();?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
        <a href="" class="btn btn-danger rounded pull-right">close</a>
    </div>
	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">LAPORAN AKTIVITAS</h4>
            <div class="date-info f-green padding-tanggal">
                Jumat, 13 Maret 2020
            </div>
            <div class="flex">
                <ul class="sub-menu-container">
                    <li class="item active"><a href="">AKTIFITAS PETUGAS</a></li>
                    <li class="sep">|</li>
                </ul>
            </div>
			<div class="f-col f-float-round pad-sm">
            <?php validation_errors(); $attributes = array("id"=>"form1");
                echo form_open("csitp/search_laporan_aktivitas_petugas",$attributes); ?>
                <div class="flex">
					<div class="f-col-4">
                        <label class="small font-weight-bold mx-3">Start Date</label>
						<input type="date" id="start_date" name="start_date" class="form-control" required>
					</div>
					<div class="f-col-4">
                        <label class="small font-weight-bold mx-3">End Date</label>
						<input type="date" id="end_date" name="end_date" class="form-control" required>
					</div>
                </div>
                <div class="flex">
					<div class="f-col-8">
                        <select name="id_petugas" id="id_petugas" class="form-control" required>
                            <option value="" selected disabled>Pilih Petugas</option>
                            <?php foreach ($petugas as $row) {; ?>
                                <option id="petugas_<?php echo $row->id; ?>" value="<?php echo $row->id; ?>"> <?php echo $row->user_ent; ?></option>
                            <?php }; ?>
                        </select>
					</div>
				</div>
                <div class="flex">
					<div class="f-col-4">
						<button type="submit" class="btn c-success rounded pull-right" style="width: 100px;">CARI</button>
					</div>
                </div>
            <?php echo form_close();?>
			</div>
            <a href="#"><h6 class="m-3 pull-right">Download</h6></a>
			<div class="flex">
                <table class="table table-green table-bordered bold">
                    <thead> 
                        <th style="text-align: left;" colspan="12">LAPORAN AKTIFITAS PETUGAS ITP</th>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach ($result as $row) {  ; ?>
                        <tr>
                            <td style="text-align: left;" colspan="12">NAMA PETUGAS -- <?php echo $row->user_ent ;?></td>
                        </tr>
                        <tr>
                            <th colspan="1" rowspan="2" scope="colgroup">No.</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
                            <th colspan="1" rowspan="2" scope="colgroup">ID Tiket Permintaan</th>
                            <th colspan="3" scope="colgroup">Ruangan Penjemputan</th>
                            <th colspan="3" scope="colgroup">Ruangan Tujuan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Petugas</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Pasien</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status</th>
                        </tr>
                        <tr>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Detail Ruangan</th>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Detail Ruangan</th>
                        </tr>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $row->transfer_date;?></td>
                            <td><?php echo $row->transporterticket_id;?></td>
                            <td><?php echo $row->name_room_origin;?></td>
                            <td><?php echo $row->name_area_origin;?></td>
                            <td><?php echo $row->name_room_origin_category;?></td>
                            <td><?php echo $row->name_room_destination;?></td>
                            <td><?php echo $row->name_area_destination;?></td>
                            <td><?php echo $row->name_room_destination_category;?></td>
                            <td><?php echo $row->user_ent;?></td>
                            <td><?php echo $row->patient_name;?></td>
                            <td><?php echo $row->name_activ;?></td>
                        </tr>
                        <?php $i++; }; ?>
                    </tbody>
                </table>
            </div>
		</div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?= base_url(); ?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/ganti_petugas'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/laporan_aktivitas_petugas'); ?>" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
	</div>
	<div class="footer-container">
		<label>Nine Cloud 2019</label>
	</div>
</body>
<script src="../../template/huda/internal/js/general.js"></script>

</html>