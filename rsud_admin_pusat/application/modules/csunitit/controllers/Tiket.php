<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiket extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('csunitit');
		$this->load->model('M_tiket','mtiket');
		$this->load->model('M_ruangan','mruang');
		$this->load->model('csunitit/M_aset','maset');
	}

	public function index()
	{
		$data = [
			'idtroubleshooting' => $this->mtiket->id(),
			'title' => 'Tiket Permintaan',
			'hal' => 'tiket/index',
			'tiket' => htmlspecialchars(trim($this->input->post('tiket'))),
			'nama' => htmlspecialchars(trim($this->input->post('nama'))),
			'ruangan' => $this->mruang->categories(),
			'js' => 'tiket/js_buat_tiket',
			'areas' => $this->mruang->areas(),
		];
		$this->load->view('layout', $data);
	}

	public function simpan()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('area', 'Area', 'trim|required|numeric');
		$this->form_validation->set_rules('detail', 'Detail Area', 'trim|required|numeric');
		$this->form_validation->set_rules('aset', 'Aset', 'trim|required|numeric');
		$this->form_validation->set_rules('keluhan', 'Keluhan', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$cek = $this->mtiket->simpan();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil disimpan!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data gagal disimpan!</div>');
			}
		}
		redirect('csunitit/dashboard','refresh');
	}

	public function getkategori()
	{
		$kamar = $this->input->get('id');
		$data = [
			'data' => $this->mruang->category_by_idroomdetail($kamar)
		];
		$this->load->view('tiket/katruangan', $data);
	}

	public function getkamar()
	{
		$ruangan = $this->input->get('ruangan');
		$area = $this->input->get('area');
		$data = [
			'data' => $this->mruang->getkamar($ruangan, $area)
		];
		$this->load->view('tiket/detailarea', $data);
	}

	public function getaset()
	{
		$kamar = $this->input->get('kamar');
		$data = [
			'data' => $this->maset->getaset($kamar)
		];
		$this->load->view('tiket/detailaset', $data);
	}

	public function idaset()
	{
		$kamar = $this->input->get('id');
		$data = [
			'data' => $this->maset->detailaset($kamar)
		];
		$this->load->view('tiket/detailaset2', $data);
	}

	public function namaaset()
	{
		$kamar = $this->input->get('id');
		$data = [
			'data' => $this->maset->detailaset($kamar)
		];
		$this->load->view('tiket/detailaset3', $data);
	}

}

/* End of file Tiket.php */
/* Location: ./application/modules/csunitit/controllers/Tiket.php */