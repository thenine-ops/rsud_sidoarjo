<!DOCTYPE html>
<html>
<head>
	<title>Perubahan Kata Sandi</title>

	<!-- Javascript External -->
	<script src="<?= base_url() ?>prabotan/external/js/bootstrap.min.js"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url() ?>prabotan/internal/js/authenticate.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/external/css/bootstrap.min.css">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/authenticate.css">
</head>

<body class="login-body">
	<div class="background-glass">
	</div>
	<div class="cp-container">
		<div class="title-container">
			UBAH KATA SANDI 		
		</div>
		<div class="form-container">
			<form id="form_cpass">
				<div class="form-group">
					<div class="fg-img">
						<img src="<?= base_url().'prabotan/image/asset/lock.png' ?>" alt="">
					</div>
					<input type="password" class="form-control" name="old_password" placeholder="Kata Sandi Lama">			
				</div>
				<div class="form-group">
					<div class="fg-img">
						<img src="<?= base_url().'prabotan/image/asset/lock.png' ?>" alt="">
					</div>
					<input type="password" class="form-control" id="new_pass1" name="new_password" placeholder="Kata Sandi Baru">			
				</div>
				<div class="form-group">
					<div class="fg-img">
						<img src="<?= base_url().'prabotan/image/asset/lock.png' ?>" alt="">
					</div>
					<input type="password" class="form-control" id="new_pass2" placeholder="Konfirmasi Kata Sandi">			
				</div>
				<button class="btn-login btn-lg btn-block" type="submit">KIRIM</button>
			</form>
		</div>
	</div>
</body>
</html>