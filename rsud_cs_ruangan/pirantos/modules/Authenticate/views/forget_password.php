<!DOCTYPE html>
<html>
<head>
	<title>Pemulihan Akun</title>

	<!-- Javascript External -->
	<script src="<?= base_url() ?>prabotan/external/js/bootstrap.min.js"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url() ?>prabotan/internal/js/authenticate.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/external/css/bootstrap.min.css">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/authenticate.css">
</head>

<body class="login-body">
	<div class="background-glass">
	</div>
	<div class="fp-container">
		<div class="title-container">
			PEMULIHAN AKUN 		
		</div>
		<div class="msg-container">
			Link form penggantian kata sandi akan di kirim melalui email. 		
		</div>
		<div class="form-container">
			<form id="form_fpass">
				<div class="form-group">
					<div class="fg-img">
						<img src="<?= base_url().'prabotan/image/asset/avatar.png' ?>" alt="">
					</div>
					<input type="text" class="form-control" name="email" placeholder="Email / No. Telepon">			
				</div>
				<button class="btn-login btn-lg btn-block" type="submit">KIRIM</button>
			</form>
		</div>
	</div>
</body>
</html>