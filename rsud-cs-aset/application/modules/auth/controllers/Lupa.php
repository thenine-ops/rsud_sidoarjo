<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lupa extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth','ma');
	}

	public function index()
	{
		parent::login();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('auth', 'Email/No.Handphone', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Lupa Password Admin Pusat',
				'js' => 'auth/lupa_js',
				'css' => 'auth/lupa_css',
				'hal' => 'auth/lupa'
			];
			$this->load->view('auth/auth', $data);
		} else {
			$cek = $this->ma->cek();
			if ($cek) {
				$this->kirim($cek->email,$cek->name,$cek->username,$cek->password);
				redirect('recovery-berhasil','refresh');
			} else {
				$this->gagal();
			}
		}
	}

	public function kirim($email, $name, $username, $password)
	{
		$email_akun = "ummul.rodhiyah.noreply@gmail.com"; // Email gmail ummul.rodhiyah.noreply@gmail.com
		$email_pass = "ummul2019"; // Password gmail : ummul2019

		$config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => $email_akun,
            'smtp_pass'   => $email_pass,
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];
		$this->load->library('email', $config);
		$this->email->from($email_akun, 'Admin Pusat RSUD');
		$this->email->to($email);
		$this->email->bcc('rezanurfachmi@gmail.com');
		$this->email->subject('Password Recovery - RSUD Sidoarjo');

		$teks = "<p>Helo $name.</p>";
		$teks.= "<p>Seseorang telah meminta tautan untuk memberikan informasi akun Anda.<br>";
		$teks.= "Username : $username<br>";
		$teks.= "Password : $password</p>";
		$teks.= "<p>Jika Anda tidak merasa melakukan ini, mohon tetap rahasiakan informasi akun Anda agar tidak terjadi kesalahan di kemudian waktu.</p>";
		$teks.= "<p>Terima kasih<br>Admin Pusat RSUD Sidoarjo</p>";
		$this->email->message($teks);
		if( !$this->email->send() ){
			die( $this->email->print_debugger() );
		}
	}

	public function alertService($js){
		$data = [
			'title' => 'Recovery Password',
			'hal' => 'auth/index',
			'css' => 'auth/lupa_css',
			'js' => 'auth/'.$js
		];
		$this->load->view('auth/auth', $data);
	}
	
	public function gagal()
	{
		$this->alertService('gagal');
	}
	
	public function gagal_login()
	{
		$this->alertService('gagal_login');
	}

	public function berhasil()
	{
		$this->alertService('berhasil');
	}

	public function recover()
	{
		$this->alertService('recover_js');
	}

}

/* End of file Lupa.php */
/* Location: ./application/modules/auth/controllers/Lupa.php */