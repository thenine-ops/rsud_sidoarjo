<?php

class M_csitp extends CI_Model
{
    function get_rooms()
    {
        return $this->db->get('room_details');
    }
    function get_petugas(){
        return $this->db->get_where('user_admin', array('role_id' => '3'))->result();
    }
    function get_nama_petugas_by_id($id_petugas){
        $this->db->select('user_ent');
        $this->db->where('id', $id_petugas);
        return $this->db->get('user_admin', 1)->row('user_ent');
    }
    function get_status()
    {
        return $this->db->get('transporter_status')->result();
    }
    function rooms_area($id)
    {
        return $this->db->get_where('room_areas', array('id' => $id));
    }
    function get_room_details_name($id)
    {
        $this->db->select('name');
        $this->db->where('id', $id);
        return $this->db->get('room_details', 1)->row('name');
    }
    function save_patients_tickets()
    {
        //ambil zona waktu
        date_default_timezone_set("Asia/Jakarta");
        //mengambil room categori berdasarkan id room detail yang di input user
        $get_all_room = $this->db->get_where("room_details", array("id" => $this->input->post("id_roomdetail")))->result_array();
        $room_categories_id = $get_all_room[0]['roomcategory_id'];

        //set room inventory id(di sesuaikan permintaan untuk pemilihan inventory nya seperti apa)
        $room_inventory = "8";

        //set transporter ticket id(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id = strtotime(date('h:i:s'));

        //set id ticket register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_ticket_register = strtotime(date('h:i:s'))
        ;

        //set id asset register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_asset_register = "123456";

        //data yang mau di insert di table transporter ticket
        $insert = array(
            "id" => $id,
            "roomdetail_id" => htmlspecialchars(trim($this->input->post('id_roomdetail'))),
            "roomcategory_id" => $room_categories_id,
            "roominventory_id" => $room_inventory,
            "assetproduct_id" => "8",
            "useradmin_id" => "1",
            "id_ticket_register" => $id_ticket_register,
            "id_asset_register" => $id_asset_register,
            "room_inventory_name" => "bunga",
            "patient_name" => htmlspecialchars(trim($this->input->post('nama'))),
            "patient_contact" => htmlspecialchars(trim($this->input->post('kontak'))),
            "ticket_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_pelapor'))),
            "date_ent" => date('Y-m-d h:i:s')
        );
        //data yang mau di save di table transporter
        $insert2 = array(
            "transporterticket_id" => $id,
            "id_ticket_register" => $id_ticket_register,
            "roominventory_id" => $room_inventory,
            "room_origin_id" => htmlspecialchars(trim($this->input->post('id_room_penjemputan'))),
            "room_destination_id " => htmlspecialchars(trim($this->input->post('id_roomdetail'))),
            "transfer_date" => date('Y-m-d h:i:s'),
            "arrival_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "useroperator_id" => "1",
            "useradmin_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_pelapor'))),
            "date_ent" => date('Y-m-d h:i:s'),
            "transporteractivity_id" => "1"
        );
        //save di table transporter ticket
        $save1 = $this->db->insert("transporter_tickets", $insert);

        //save di table transporter
        $save2 = $this->db->insert("transporters", $insert2);

        redirect('csitp/aktifitas_pemindahan_pasien','refresh');
        return true;
    }
    // mengambil jumlah status permintaan
    function get_count_permintaan_activity()
    {
        return $this->db->get_where('transporters', array('transporteractivity_id' => 1))->num_rows();
    }
    function get_count_terjadwal_activity()
    {
        return $this->db->get_where('transporters', array('transporteractivity_id' => 2))->num_rows();
    }
    function get_count_selesai_activity()
    {
        return $this->db->get_where('transporters', array('transporteractivity_id' => 3))->num_rows();
    }
    //-----------------------------------------------------------------------

    //mengambil data aktifitas pemindahan pasien
    function get_aktifitas_harian()
    {
        $this->db->select('tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,rt.name as name_area_destination,rd.id as id_roomdetails_ori ,rd.name as name_room_origin,rdt.id as id_roomdetails_dest,rdt.name as name_room_destination, rco.name as name_room_origin_category, rcd.name as name_room_destination_category, tr_stat.name as name_status,tr_activ.name as name_activ');
        $this->db->from('transporters tr'); 
        $this->db->join('transporter_tickets trt', 'trt.id=tr.transporterticket_id','left');
        $this->db->join('room_areas ra', 'ra.id=tr.room_origin_id','left');
        $this->db->join('room_areas rt', 'rt.id=tr.room_destination_id','left');
        $this->db->join('room_details rd', 'rd.id=tr.room_origin_id','left');
        $this->db->join('room_details rdt', 'rdt.id=tr.room_destination_id','left');
        $this->db->join('room_categories rco', 'rco.id=rd.roomcategory_id','left');
        $this->db->join('room_categories rcd', 'rcd.id=rdt.roomcategory_id','left');
        $this->db->join('transporter_status tr_stat', 'tr_stat.id=tr.transporterstatus_id','left');
        $this->db->join('transporter_activities tr_activ', 'tr_activ.id=tr.transporteractivity_id','left');
        $query = $this->db->get();
        return $query;
    }
    //mengambil data aktifitas pemindahan pasien
    function get_aktifitas_harian_filter($filter)
    {
        // debug($filter);
        $sql = "SELECT tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,
        tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,
        rt.name as name_area_destination,rd.name as name_room_origin,rdt.name as name_room_destination,
        tr_stat.name as name_status,tr_activ.name as name_activ FROM transporters tr
        LEFT JOIN transporter_tickets trt ON trt.id=tr.transporterticket_id
        LEFT JOIN room_areas ra ON ra.id=tr.room_origin_id
        LEFT JOIN room_areas rt ON rt.id=tr.room_destination_id
        LEFT JOIN room_details rd ON rd.id=tr.room_origin_id
        LEFT JOIN room_details rdt ON rdt.id=tr.room_destination_id
        LEFT JOIN transporter_status tr_stat ON tr_stat.id=tr.transporterstatus_id
        LEFT JOIN transporter_activities tr_activ ON tr_activ.id=tr.transporteractivity_id WHERE transfer_date::varchar LIKE '%".$filter."%'
        ";
        return $this->db->query($sql);

    }

    //mengambil data aktifitas pemindahan pasien berdasarkan id_petugas dan tanggal
    function get_aktifitas_harian_filter_by_id_petugas($filter)
    {
        $exploded_filter = explode("_", $filter);
        $id_petugas = $exploded_filter[0];
        $tgl = $exploded_filter[1];

        $sql = "SELECT tr.transporterticket_id,tr.useradmin_id,tr.user_ent,tr.transporteractivity_id,
        tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,
        rt.name as name_area_destination,rd.name as name_room_origin,rdt.name as name_room_destination,
        tr_stat.name as name_status,tr_activ.name as name_activ FROM transporters tr
        LEFT JOIN transporter_tickets trt ON trt.id=tr.transporterticket_id
        LEFT JOIN room_areas ra ON ra.id=tr.room_origin_id
        LEFT JOIN room_areas rt ON rt.id=tr.room_destination_id
        LEFT JOIN room_details rd ON rd.id=tr.room_origin_id
        LEFT JOIN room_details rdt ON rdt.id=tr.room_destination_id
        LEFT JOIN transporter_status tr_stat ON tr_stat.id=tr.transporterstatus_id
        LEFT JOIN transporter_activities tr_activ ON tr_activ.id=tr.transporteractivity_id 
        WHERE tr.useradmin_id = '".$id_petugas."'
        AND transfer_date::varchar LIKE '%".$tgl."%'
        ";
        return $this->db->query($sql)->result();
    }

    //mengambil data aktifitas pemindahan pasien berdasarkan date range dan id_petugas
    function get_aktifitas_harian_filter_by_date_range($filter)
    {
        $exploded_filter = explode("_", $filter);
        $id_petugas = $exploded_filter[0];
        $start_date = $exploded_filter[1];
        $end_date = $exploded_filter[2];

        $sql = "
        SELECT  tr.transporterticket_id,
                tr.useradmin_id,
                tr.user_ent,
                tr.transporteractivity_id,
                tr.transfer_date,
                tr_stat.name as name_status,
                tr_activ.name as name_activ,
                trt.patient_name,
                trt.patient_contact,
                ra.name as name_area_origin,
                rt.name as name_area_destination,
                rd.name as name_room_origin,
                rdt.name as name_room_destination,
                rco.name as name_room_origin_category,
                rcd.name as name_room_destination_category
        FROM transporters tr
        LEFT JOIN transporter_tickets trt ON trt.id=tr.transporterticket_id
        LEFT JOIN room_areas ra ON ra.id=tr.room_origin_id
        LEFT JOIN room_areas rt ON rt.id=tr.room_destination_id
        LEFT JOIN room_details rd ON rd.id=tr.room_origin_id
        LEFT JOIN room_details rdt ON rdt.id=tr.room_destination_id
        LEFT JOIN room_categories rco ON rco.id=rd.roomcategory_id
        LEFT JOIN room_categories rcd ON rcd.id=rdt.roomcategory_id
        LEFT JOIN transporter_status tr_stat ON tr_stat.id=tr.transporterstatus_id
        LEFT JOIN transporter_activities tr_activ ON tr_activ.id=tr.transporteractivity_id 
        WHERE transfer_date BETWEEN '".$start_date."' AND '".$end_date."'
        AND tr.useradmin_id = '".$id_petugas."'
        ";

        return $this->db->query($sql)->result();
    }

    function get_aktifitas_harian_detail($id)
    {
        $this->db->select('tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,rt.name as name_area_destination,rd.id as id_roomdetails_ori ,rd.name as name_room_origin,rdt.id as id_roomdetails_dest,rdt.name as name_room_destination,tr_stat.name as name_status,tr_activ.name as name_activ');
        $this->db->from('transporters tr'); 
        $this->db->join('transporter_tickets trt', 'trt.id=tr.transporterticket_id','left');
        $this->db->join('room_areas ra', 'ra.id=tr.room_origin_id','left');
        $this->db->join('room_areas rt', 'rt.id=tr.room_destination_id','left');
        $this->db->join('room_details rd', 'rd.id=tr.room_origin_id','left');
        $this->db->join('room_details rdt', 'rdt.id=tr.room_destination_id','left');
        $this->db->join('transporter_status tr_stat', 'tr_stat.id=tr.transporterstatus_id','left');
        $this->db->join('transporter_activities tr_activ', 'tr_activ.id=tr.transporteractivity_id','left');
        $this->db->where("transporterticket_id",$id);
        $query = $this->db->get();
        return $query;
    }

    function get_data_pindah_pasien($filter)
    {
        $exploded_filter = explode("_", $filter);
        $tgl = $exploded_filter[0];
        $id_status = $exploded_filter[1];

        $sql = "SELECT tr.transporterticket_id,tr.user_ent,tr.transporteractivity_id,
        tr.transfer_date, trt.patient_name,trt.patient_contact,ra.name as name_area_origin,
        rt.name as name_area_destination,rd.name as name_room_origin,rdt.name as name_room_destination,
        tr_stat.name as name_status,tr_activ.name as name_activ FROM transporters tr
        LEFT JOIN transporter_tickets trt ON trt.id=tr.transporterticket_id
        LEFT JOIN room_areas ra ON ra.id=tr.room_origin_id
        LEFT JOIN room_areas rt ON rt.id=tr.room_destination_id
        LEFT JOIN room_details rd ON rd.id=tr.room_origin_id
        LEFT JOIN room_details rdt ON rdt.id=tr.room_destination_id
        LEFT JOIN transporter_status tr_stat ON tr_stat.id=tr.transporterstatus_id
        LEFT JOIN transporter_activities tr_activ ON tr_activ.id=tr.transporteractivity_id WHERE tr.transporterstatus_id = '".$id_status."' AND transfer_date::varchar LIKE '%".$tgl."%'
        ";
        return $this->db->query($sql);

    }

    function save_ganti_petugas_model()
    {
        // debug($_POST);
        //ambil zona waktu
        date_default_timezone_set("Asia/Jakarta");
        //mengambil room categori berdasarkan id room detail yang di input user
        $get_all_room = $this->db->get_where("room_details", array("id" => $this->input->post("room_dest")))->result_array();
        $room_categories_id = $get_all_room[0]['roomcategory_id'];
        //id ticket yang mau di close
        $idtiket = $this->input->post('idtiket');

        //set room inventory id(di sesuaikan permintaan untuk pemilihan inventory nya seperti apa)
        $room_inventory = "8";

        //set transporter ticket id(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id = strtotime(date('h:i:s'));

        //set id ticket register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_ticket_register = strtotime(date('h:i:s'))
        ;

        //set id asset register(di sesuaikan permintaan untuk uniq ini di isi berdasarkan apa)
        $id_asset_register = "123456";

        //data yang mau di insert di table transporter ticket
        $insert = array(
            "id" => $id,
            "roomdetail_id" => htmlspecialchars(trim($this->input->post('room_dest'))),
            "roomcategory_id" => $room_categories_id,
            "roominventory_id" => $room_inventory,
            "assetproduct_id" => "8",
            "useradmin_id" => "1",
            "id_ticket_register" => $id_ticket_register,
            "id_asset_register" => $id_asset_register,
            "room_inventory_name" => "bunga",
            "patient_name" => htmlspecialchars(trim($this->input->post('patient_name'))),
            "patient_contact" => htmlspecialchars(trim($this->input->post('patient_contact'))),
            "ticket_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_petugas'))),
            "date_ent" => date('Y-m-d h:i:s')
        );
        //data yang mau di save di table transporter
        $insert2 = array(
            "transporterticket_id" => $id,
            "id_ticket_register" => $id_ticket_register,
            "roominventory_id" => $room_inventory,
            "room_origin_id" => htmlspecialchars(trim($this->input->post('room_ori'))),
            "room_destination_id " => htmlspecialchars(trim($this->input->post('room_dest'))),
            "transfer_date" => date('Y-m-d h:i:s'),
            "arrival_date" => date('Y-m-d h:i:s'),
            "transporterstatus_id" => "1",
            "useroperator_id" => "1",
            "useradmin_id" => "1",
            "user_ent" => htmlspecialchars(trim($this->input->post('nama_petugas'))),
            "date_ent" => date('Y-m-d h:i:s'),
            "transporteractivity_id" => "1"
        );
        //save di table transporter ticket
        $save1 = $this->db->insert("transporter_tickets", $insert);

        //save di table transporter
        $save2 = $this->db->insert("transporters", $insert2);
        //update status ticket petugas sebelumnya
        $upd_status = "UPDATE transporter_tickets SET transporterstatus_id=2 WHERE id='". $idtiket ."' ";
        $this->db->query($upd_status);
        //update status activity petugas sebelumnya
        $upd_status2 = "UPDATE transporters SET transporterstatus_id=2,transporteractivity_id=4 WHERE transporterticket_id='". $idtiket ."' ";
        $this->db->query($upd_status2);
        return true;
    }

    function save_ganti_petugas($data)
    {
        // debug($data);
        $data['nama_petugas'] = $this->get_nama_petugas_by_id($data['id_petugas']);

        $update_data_transporter_tickets = array(
        // 'keterangan' => $data['alasan_penggantian'],
        'useradmin_id' => $data['id_petugas'],
        'user_ent' => $data['nama_petugas'],
        "date_ent" => date('Y-m-d h:i:s'),
        );

        $this->db->where('id_ticket_register', $data['id_tiket']);
        $this->db->update('transporter_tickets', $update_data_transporter_tickets);

        $update_data_transporters = array(
        // 'keterangan' => $data['alasan_penggantian'],
        'useradmin_id' => $data['id_petugas'],
        'user_ent' => $data['nama_petugas'],
        "date_ent" => date('Y-m-d h:i:s'),
        );

        $this->db->where('id_ticket_register', $data['id_tiket']);
        $this->db->update('transporters', $update_data_transporters); 
    }

    function cancel_ticket($data)
    {
        $update_data_transporter_tickets = array(
        // 'keterangan' => $data['alasan_pembatalan'],
        'transporterstatus_id' => '2', // status set to close
        );

        $this->db->where('id_ticket_register', $data['id_ticket']);
        $this->db->update('transporter_tickets', $update_data_transporter_tickets);

        $update_data_transporters = array(
        // 'keterangan' => $data['alasan_pembatalan'],
        'transporterstatus_id' => '2', // status set to close
        );

        $this->db->where('id_ticket_register', $data['id_ticket']);
        $this->db->update('transporters', $update_data_transporters);
    }


}
