<script type="text/javascript">
	$('select#inputKategori').change(function(){
		var kategori = $('select#inputKategori').val();
		$.ajax({
			url: "<?=base_url('adminaset/aset/getsubkategori');?>",
		    data: {kategori:kategori},
		    method: "GET",
		    cache: false,
		    success: function(msg){
		        $("#inputSubkategori").html(msg);
		    }
		});
	});

	$('select#inputSubkategori').change(function(){
		var subkategori = $('select#inputSubkategori').val();
		var kategori = $('select#inputKategori').val();
		$.ajax({
			url: "<?=base_url('adminaset/aset/getdetailkategori');?>",
		    data: {subkategori:subkategori, kategori:kategori},
		    method: "GET",
		    cache: false,
		    success: function(msg){
		        $("#inputDetail").html(msg);
		    }
		});
	});
</script>