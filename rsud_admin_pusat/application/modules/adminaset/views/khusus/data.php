<?php $this->load->view('khusus/menu');?>

<?=$this->session->flashdata('name');?>
<a href="<?=base_url('adminaset/khusus/data_tambah');?>" class="btn btn-success btn-lg mb-3 rounded">Tambah Data</a>

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Serial</th>
      <th scope="col">Nama Spare Part</th>
      <th scope="col">Quantity</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	<?php $no=1; foreach($data as $d): ?>
    <tr>
      <th scope="row"><?=$no++;?></th>
      <td><?=$d->id_sparepart_register;?></td>
      <td><?=$d->name;?></td>
      <td><?=$d->quantity;?></td>
      <td><?=$d->description;?></td>
      <td>
      	<a href="<?=base_url('adminaset/khusus/data_edit/'.$d->id);?>" class="btn btn-primary btn-sm rounded">Edit</a>
      	<a href="<?=base_url('adminaset/khusus/data_hapus/'.$d->id);?>" class="btn btn-danger btn-sm rounded">Hapus</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
