<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_respon extends CI_Model {

	public function detail($id)
	{
		return 	$this->db->select('
									tiket.id_ticket_register as tiket, 
									tiket.ticket_date as tanggal,
									akun.name as pelapor,
									tipe.name as jenis,
									detail.name as ruangan,
									trobel.id as idtrouble
								 ')
						 ->from('troubleshooting_tickets tiket')
						 ->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						 ->join('user_accounts akun','akun.id=tiket.useradmin_id')
						 ->join('troubleshooting_types tipe','tipe.id=tiket.troubleshootingtype_id')
						 ->join('room_details detail','detail.id=tiket.roomdetail_id')
						 ->where('tiket.id', $id)
						 ->get()
						 ->row();
	}

	public function barang($id)
	{
		return $this->db->select('
									produk.id_asset_register as idaset,
									produk.name as namaaset,
									produk.description as deskripsiproduk,
									aset.description as deskripsiaset,
									aset.name as detailkategori,
									subaset.name as subkategoriaset,
									tiket.description as keluhan,
									kataset.name as kategoriaset,
									detail.name as noruangan,
									area.name as area,
									katkamar.name as ruangan
								')
						->from('asset_products produk')
						->join('assets aset','aset.id=produk.asset_id')
						->join('asset_sub_categories subaset','subaset.id=aset.assetsubcategory_id')
						->join('asset_categories kataset','kataset.id=subaset.assetcategory_id')
						->join('troubleshootings trobel','trobel.assetproduct_id=produk.id')
						->join('troubleshooting_tickets tiket','tiket.id=trobel.troubleshootingticket_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_categories katkamar','katkamar.id=detail.roomcategory_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->where('tiket.id', $id)
						->get()
						->result();
	}

	public function respon()
	{
		$post = $this->input->post();

		$id = htmlspecialchars(trim($post['idtiket']));
		$idtrouble = htmlspecialchars(trim($post['idtrouble']));

		$tiket = [
			'useradmin_id' => htmlspecialchars(trim($post['petugas'])),
			'troubleshootingstatus_id' => '1'
		];
		$where = ['id' => $id];
		$cek = $this->db->update('troubleshooting_tickets', $tiket, $where);
		if ($cek) {
			$trouble = [
				'useradmin_id' => htmlspecialchars(trim($post['petugas'])),
				'troubleshootingactivity_id' => '2',
				'user_ent' => $this->session->nama
			];
			$this->db->set('date_ent','now()');
			$where = ['id' => $idtrouble];
			$cek = $this->db->update('troubleshootings', $trouble, $where);
			if ($cek) {
				return true;
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	}

}

/* End of file M_respon.php */
/* Location: ./application/modules/csunitit/models/M_respon.php */