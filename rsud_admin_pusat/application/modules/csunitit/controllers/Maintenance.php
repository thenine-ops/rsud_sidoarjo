<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('csunitit');
		$this->load->model('M_aset','maset');
		$this->load->model('M_ruangan','mruang');
		$this->load->model('M_petugas','mpetugas');
		$this->load->model('M_maintenance','mm');
	}

	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('ruangan', 'Ruangan', 'trim|required|numeric');
		$this->form_validation->set_rules('aset', 'Aset', 'trim|required|numeric');
		$this->form_validation->set_rules('petugas', 'Petugas', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'CS Unit IT',
				'hal' => 'maintenance/index',
				'ruangan' => $this->mruang->kamar(),
				'js' => 'maintenance/js',
				'petugas' => $this->mpetugas->data()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->mm->simpan();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Tiket berhasil dibuat!</div>');
				redirect('csunitit/maintenance','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Tiket gagal dibuat!</div>');
				redirect('csunitit/maintenance','refresh');
			}
		}
		
	}

	public function data()
	{
		$this->load->model('M_trouble','mtro');
		$this->load->model('M_ruangan','mruang');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|numeric');
		$this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|numeric');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|numeric');
		$this->form_validation->set_rules('ruangan', 'Ruangan', 'trim|required|numeric');

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Maintenance',
				'hal' => 'maintenance/data',
				'status' => $this->mtro->status(),
				'ruangan' => $this->mruang->categories(),
				'data' => $this->mm->semua()
			];
			$this->load->view('layout', $data);
		}else{
			$data = [
				'title' => 'Maintenance',
				'hal' => 'maintenance/hasil',
				'status' => $this->mtro->status(),
				'ruangan' => $this->mruang->categories(),
				'data' => $this->mm->filter(),
				'hasil' => $this->mruang->category_by_id($this->input->post('ruangan'))
			];
			$this->load->view('layout', $data);
		}
	}

}

/* End of file Maintenance.php */
/* Location: ./application/modules/csunitit/controllers/Maintenance.php */