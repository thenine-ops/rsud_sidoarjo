<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utama extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth();
	}

	public function index()
	{
		$this->load->view('index');
	}

}

/* End of file Utama.php */
/* Location: ./application/modules/intro/controllers/Utama.php */