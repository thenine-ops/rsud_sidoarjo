<?=$this->session->flashdata('name');?>
<?php
$attr = [
	'class' => 'form-inline mb-4'
];
?>
<?=validation_errors('<small class="text-danger">','</small>');?>
<?=form_open('adminaset/aset/sub_kategori_edit/'.$data->id, $attr);?>
<input type="hidden" name="id" id="inputId" class="form-control" value="<?=$data->id;?>">
  <select name="kategori" id="inputKategori" class="form-control form-control-lg mb-2 mr-sm-2" required="required">
    <option value="">Pilih Kategori</option>
    <?php foreach($kategori as $k): ?>
    <option value="<?=$k->id;?>" <?=($k->id==$data->assetcategory_id)?'selected':'';?>><?=$k->name;?></option>
    <?php endforeach; ?>
  </select>
	<label class="sr-only" for="nama">Nama</label>
	<input type="text" class="form-control form-control-lg mb-2 mr-sm-2 col-md-7 <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" placeholder="Nama Sub Kategori (Contoh: Software, Jaringan, Gedung, dll)" value="<?=$data->name;?>">

	<button type="submit" class="btn btn-success btn-lg mb-2 rounded">Simpan</button>
</form>