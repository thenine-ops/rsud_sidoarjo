<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Khusus extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminaset');
		$this->load->model('M_khusus','mk');
		$this->load->model('M_aset','maset');
	}

	public function index()
	{
		$data = [
			'title' => 'Master Spare Part',
			'hal' => 'khusus/master',
			'data' => $this->mk->master()
		];
		$this->load->view('layout', $data);
	}

	public function khusus_tambah()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part - Tambah Data',
				'hal' => 'khusus/master_tambah',
				'sub_kategori' => $this->maset->sub_kategori()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->mk->master_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
			}
			redirect('adminaset/khusus','refresh');
		}
	}

	public function khusus_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/khusus','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part - Ubah Data',
				'hal' => 'khusus/master_edit',
				'data' => $this->mk->master_by_id($id),
				'sub_kategori' => $this->maset->sub_kategori()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->mk->master_edit();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
			}
			redirect('adminaset/khusus','refresh');
		}
	}

	public function khusus_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/khusus','refresh');
		$cek = $this->maset->hapus('sparepart_generals', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		}
		redirect('adminaset/khusus','refresh');
	}


	public function data()
	{
		$data = [
			'title' => 'Spare Part Khusus',
			'hal' => 'khusus/data',
			'data' => $this->mk->data()
		];
		$this->load->view('layout', $data);
	}

	public function data_tambah()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Spare Part Khusus - Tambah Data',
				'hal' => 'khusus/data_tambah',
				'sp' => $this->mk->master()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->mk->data_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
			}
			redirect('adminaset/khusus/data','refresh');
		}
	}

	public function data_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/khusus/data','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Spare Part Khusus - Ubah Data',
				'hal' => 'khusus/data_edit',
				'data' => $this->mk->data_by_id($id),
				'sp' => $this->mk->master()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->mk->data_edit();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
			}
			redirect('adminaset/khusus/data','refresh');
		}
	}

	public function data_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/khusus/data','refresh');
		$cek = $this->maset->hapus('sparepart_product_generals', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		}
		redirect('adminaset/khusus/data','refresh');
	}

}

/* End of file Khusus.php */
/* Location: ./application/modules/adminaset/controllers/Khusus.php */