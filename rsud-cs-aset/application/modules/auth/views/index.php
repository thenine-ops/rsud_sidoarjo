<?=$this->session->flashdata('name');?>
<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>');?>


          <?=form_open('auth');?>
            <div class="input-group mb-3 mt-4">
              <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <p class="mb-3">
              <a class="link-auth" href="<?=base_url('lupa-password');?>">Forget Password</a>
            </p>
              <div class="col-12">
                <button type="submit" class="btn btn-success  btn-lg form-control" style="border-radius:20px;padding-bottom:40px">L O G I N</button>
              <!-- /.col -->
            </div>
          </form>

