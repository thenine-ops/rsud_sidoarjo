<?=$this->session->flashdata('name');?>
<div class="mx-3">
    <?=form_open();?>
        <input type="hidden" name="id" value="<?=$data->id;?>">
        <div class="form-group">
            <label for="aset">Nama Aset</label>
            <select class="form-control <?=(form_error('aset'))?'is-invalid':'';?>" name="aset" id="aset">
                <option>Pilih Aset</option>
                <?php foreach($aset as $k): ?>
                <option value="<?=$k->id;?>" <?=($data->asset_id==$k->id)?'selected':'';?>><?=$k->name;?></option>
                <?php endforeach; ?>
            </select>
            <?=form_error('aset','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Spare Part</label>
            <input type="text" class="form-control <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" aria-describedby="nama" placeholder="Nama Spare Part" value="<?=$data->name;?>">
            <?=form_error('nama','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="description">Deskripsi</label>
          <textarea class="form-control <?=(form_error('description'))?'is-invalid':'';?>" name="description" id="description" rows="3" placeholder="Deskripsi Spare Part"><?=$data->description;?></textarea>
          <?=form_error('description','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="quantity">Quantity</label>
          <input type="text" class="form-control <?=(form_error('quantity'))?'is-invalid':'';?>" name="quantity" id="quantity" aria-describedby="quantity" placeholder="Quantity" value="<?=$data->quantity;?>">
          <?=form_error('quantity','<small class="text-danger">','</small>');?>
        </div>
    <button type="submit" class="btn btn-success btn-lg">Submit</button>
    <a href="<?=base_url('adminaset/sparepart/data');?>" class="btn btn-dark btn-lg">Kembali</a>
    </form>
</div>