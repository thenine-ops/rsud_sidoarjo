<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sparepart extends CI_Model {

	public function data()
	{
		return $this->db->order_by('name','asc')->get('sparepart_products')->result();
	}

	public function data_khusus()
	{
		return $this->db->order_by('name','asc')->get('sparepart_product_generals')->result();
	}

	public function data_by_id($id)
	{
		return $this->db->where('id',$id)->get('sparepart_products')->row();
	}

	public function data_khusus_by_id($id)
	{
		return $this->db->where('id',$id)->get('sparepart_product_generals')->row();
	}

	public function tambah_stok()
	{
		$data = [
			'sparepart_type' => 'umum',
			'sparepartproduct_id' => htmlspecialchars(trim($this->input->post('sparepart'))),
			'quantity' => htmlspecialchars(trim($this->input->post('stok'))),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		$this->db->insert('sparepart_histories', $data);

		$id = htmlspecialchars(trim($this->input->post('sparepart')));
		$data = $this->data_by_id($id);
		$stok = $data->quantity;

		$data = [
			'quantity' => $stok + $this->input->post('stok')
		];

		$where = ['id' => $id];

		return $this->db->update('sparepart_products', $data, $where);
	}

	public function tambah_stok_khusus()
	{
		$data = [
			'sparepart_type' => 'khusus',
			'sparepartproduct_id' => htmlspecialchars(trim($this->input->post('sparepart'))),
			'quantity' => htmlspecialchars(trim($this->input->post('stok'))),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		$this->db->insert('sparepart_histories', $data);

		$id = htmlspecialchars(trim($this->input->post('sparepart')));
		$data = $this->data_khusus_by_id($id);
		$stok = $data->quantity;

		$data = [
			'quantity' => $stok + $this->input->post('stok')
		];

		$where = ['id' => $id];

		return $this->db->update('sparepart_product_generals', $data, $where);
	}

	public function stok($status='1')
	{
		$this->db->select('sp.id, sp.type, sp.id_sparepart_register, sp.name spname, sp.quantity, s.name sname, sc.name scname, a.name aname')
				->from('sparepart_products sp')
				->join('spareparts s','s.id=sp.sparepart_id')
				->join('sparepart_categories sc','sc.id=sp.sparepartcategory_id')
				->join('assets a','a.id=s.asset_id')
				;
		if($status!='1'){
			$this->db->where('sp.quantity <=','0');
		}else{
			$this->db->where('sp.quantity >','0');
		}
		return $this->db->get()->result();
	}

	public function stok_khusus($status='1')
	{
		$this->db->select('spg.id, spg.id_sparepart_register, spg.name spgname, spg.type, spg.quantity, sg.name sgname, asc.name ascname')
				->from('sparepart_product_generals spg')
				->join('sparepart_generals sg','sg.id=spg.sparepartgeneral_id')
				->join('asset_sub_categories asc','asc.id=sg.assetsubcategory_id')
				;
		if($status!='1'){
			$this->db->where('spg.quantity <=','0');
		}else{
			$this->db->where('spg.quantity >','0');
		}
		return $this->db->get()->result();
	}

}

/* End of file M_sparepart.php */
/* Location: ./application/modules/csunitaset/models/M_sparepart.php */