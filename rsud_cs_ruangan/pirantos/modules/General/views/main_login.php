<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	<!-- Javascript External -->
	<script src="<?= base_url() ?>prabotan/external/js/jquery.js"></script>
	<script src="<?= base_url() ?>prabotan/external/js/bootstrap.min.js"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url() ?>prabotan/internal/js/button_action.js"></script>
	<script src="<?= base_url() ?>prabotan/internal/js/modal.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/external/css/bootstrap.min.css">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/authenticate.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/loading.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/modal.css">
</head>

<body class="login-body">
	<div class="loading">
		<img src="<?= base_url() ?>prabotan/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="background-glass">
	</div>
	<?= $page ?>
</body>
<script src="<?= base_url() ?>prabotan/internal/js/general.js"></script>
</html>


