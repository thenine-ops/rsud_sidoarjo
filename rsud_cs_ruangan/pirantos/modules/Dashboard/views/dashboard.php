<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>

	<!-- Javascript External -->
	<script src="<?= base_url() ?>prabotan/external/js/jquery.js"></script>
	<script src="<?= base_url() ?>prabotan/external/js/bootstrap.min.js"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url() ?>prabotan/internal/js/button_action.js"></script>
	<script src="<?= base_url() ?>prabotan/internal/js/modal.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/external/css/bootstrap.min.css">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/dashboard.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/modal.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>prabotan/internal/css/loading.css">
</head>

<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>prabotan/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="background-glass">
	</div>
	<div class="header-container">
		<div class="logo-container">
			<img src="<?= base_url().'prabotan/image/asset/login-logo.png' ?>" alt="">			
		</div>
		<div class="logout-container logout-btn" data-link="<?= base_url() ?>authenticate/logout">
			<img src="<?= base_url().'prabotan/image/asset/wt-logout.png' ?>" alt="">
			<p>Logout</p>
		</div>
	</div>
	<div class="content-container">
		<?php 
		$user_log = $this->session->userdata('rsud_admin_pusat');
		?>
		<div class="content-component">
			<div class="text-1">Selamat Datang</div>
			<div class="textc-1"><?= strtoupper($user_log['name']) ?></div>
			<div class="text-2"><?= strtoupper(@$user_log['role']) ?></div>
			<div class="text-2">RSUD SIDOARJO</div>
			<div class="text-1">Semoga Harimu Menyenangkan</div>
			<br>
			<br>
			<div class="text-3 running-time"></div>
			<div class="text-3 running-date"></div>
		</div>
	</div>
	<div class="footer-container">
		<a class="button-menu" href="<?= base_url().'account' ?>">
			<div class="img-menu">
				<img src="<?= base_url().'prabotan/image/asset/wt-curriculum.png' ?>" alt="">
			</div>
			<div class="header-menu">
				MY ACCOUNT
			</div>
			<div class="desc-menu">
				Informasi tentang personal data dan akses sistem
			</div>
		</a>
		<a class="button-menu" href="<?= base_url().'apps' ?>">
			<div class="img-menu">
				<img src="<?= base_url().'prabotan/image/asset/wt-speed.png' ?>" alt="">
			</div>
			<div class="header-menu">
				GO TO APPLICATION
			</div>
			<div class="desc-menu">
				Pergi ke aplikasi untuk membantu menjalankan aktifitas kerja
			</div>
		</a>
		<a class="button-menu coming-soon" href="#">
			<div class="img-menu">
				<img src="<?= base_url().'prabotan/image/asset/wt-backup.png' ?>" alt="">
			</div>
			<div class="header-menu">
				BACKUP DATA
			</div>
			<div class="desc-menu">
				Simpan data aktifitas sistem untuk keamanan data
			</div>
		</a>
	</div>
</body>
<script src="<?= base_url() ?>prabotan/internal/js/general.js"></script>
</html>