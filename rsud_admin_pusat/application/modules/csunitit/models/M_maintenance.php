<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_maintenance extends CI_Model {

	public function id()
	{
		$data = $this->db->select('id')
						 ->from('troubleshooting_tickets')
						 ->order_by('id','desc')
						 ->get();

		$jum = $data->num_rows();
		$res = $data->row();
		if($jum>=1){
			$d = $res->id + 1;
		}else{
			$d = 1;
		}
		
		return $d;
	}

	public function simpan()
	{
		$id = $this->id();
		$post = $this->input->post();
		$idt = "CSIT-".time();
		$data = [
			'id' => $id,
			'roomdetail_id' => htmlspecialchars(trim($post['ruangan'])),
			'roomcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'unit_id' => $this->session->unit_id,
			'id_ticket_register' => $idt,
			'useradmin_id' => $this->session->id,
			'user_name' => $this->session->nama,
			'description' => 'Regular maintenance',
			'troubleshootingstatus_id' => 1,
			'user_ent' => $this->session->id,
			'troubleshootingtype_id' => 2
		];
		$this->db->set('ticket_date','now()');
		$this->db->set('ticket_closed_date','now()');
		$this->db->set('date_ent','now()');
		$cek = $this->db->insert('troubleshooting_tickets', $data);
		if ($cek) {
			$data = [
				'troubleshootingticket_id' => $id,
				'assetproduct_id' => htmlspecialchars(trim($post['aset'])),
				'troubleshootingactivity_id' => 2,
				'useradmin_id' => htmlspecialchars(trim($post['petugas'])),
				'id_ticket_register' => $idt,
				'id_asset_register' => htmlspecialchars(trim($post['id_asset_register'])),
				'asset_name' => htmlspecialchars(trim($post['asset_name'])),
				'description' => 'Regular maintenance',
				'user_ent' => $this->session->id
			];
			$this->db->set('date_ent','now()');
			$this->db->set('process_date','now()');
			$cek = $this->db->insert('troubleshootings', $data);
			if ($cek) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function data()
	{
		$bulan = date('m');
		$tahun = date('Y');
		return $this->db->select('
									tiket.id_ticket_register AS tiket,
									tiket.ticket_date AS waktu,
									detail.name AS keterangan,
									akun.name AS pelapor,
									katkamar.name AS ruangan,
									produk.name AS namaaset,
									area.name AS area,
									status.name AS statustiket,
									aktifitas.name AS statuspelayanan
								')
						->from('troubleshooting_tickets tiket')
						->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						->join('user_accounts akun','akun.id=tiket.useradmin_id')
						->join('room_categories katkamar','katkamar.id=tiket.roomcategory_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->join('asset_products produk','produk.id_asset_register=trobel.id_asset_register')
						->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
						->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
						->where('EXTRACT(YEAR FROM tiket.ticket_date)=', $tahun)
						->where('EXTRACT(MONTH FROM tiket.ticket_date)=', $bulan)
						->where('tiket.troubleshootingtype_id', 2)
						->get()
						->result();
	}

	public function semua()
	{
		return $this->db->select('
									tiket.id_ticket_register AS tiket,
									tiket.ticket_date AS waktu,
									detail.name AS keterangan,
									akun.name AS pelapor,
									katkamar.name AS ruangan,
									produk.name AS namaaset,
									area.name AS area,
									status.name AS statustiket,
									aktifitas.name AS statuspelayanan
								')
						->from('troubleshooting_tickets tiket')
						->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						->join('user_accounts akun','akun.id=tiket.useradmin_id')
						->join('room_categories katkamar','katkamar.id=tiket.roomcategory_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->join('asset_products produk','produk.id_asset_register=trobel.id_asset_register')
						->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
						->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
						->where('tiket.troubleshootingtype_id', 2)
						->get()
						->result();
	}

	public function filter()
	{
		$post = $this->input->post();
		$bulan = $post['bulan'];
		$tahun = $post['tahun'];
		$ruangan = $post['ruangan'];
		$status = $post['status'];
		return $this->db->select('
									tiket.id_ticket_register AS tiket,
									tiket.ticket_date AS waktu,
									detail.name AS keterangan,
									akun.name AS pelapor,
									katkamar.name AS ruangan,
									produk.name AS namaaset,
									area.name AS area,
									status.name AS statustiket,
									aktifitas.name AS statuspelayanan
								')
						->from('troubleshooting_tickets tiket')
						->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
						->join('user_accounts akun','akun.id=tiket.useradmin_id')
						->join('room_categories katkamar','katkamar.id=tiket.roomcategory_id')
						->join('room_details detail','detail.id=tiket.roomdetail_id')
						->join('room_areas area','area.id=detail.roomarea_id')
						->join('asset_products produk','produk.id_asset_register=trobel.id_asset_register')
						->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
						->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
						->where('EXTRACT(YEAR FROM tiket.ticket_date)=', $tahun)
						->where('EXTRACT(MONTH FROM tiket.ticket_date)=', $bulan)
						->where('tiket.troubleshootingstatus_id', $status)
						->where('tiket.roomcategory_id', $ruangan)
						->where('tiket.troubleshootingtype_id', 2)
						->get()
						->result();
	}

}

/* End of file M_maintenance.php */
/* Location: ./application/modules/csunitit/models/M_maintenance.php */