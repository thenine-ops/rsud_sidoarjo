<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aset extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminaset');
		$this->load->model('adminaset/M_aset','maset');
		$this->load->model('M_unit','munit');
	}

	public function index()
	{
		redirect('adminaset/aset/kategori','refresh');
	}

	public function kategori()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[15]');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Kategori',
				'hal' => 'aset/kategori',
				'data' => $this->maset->kategori()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->kategori_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
				redirect('adminaset/aset/kategori','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
				redirect('adminaset/aset/kategori','refresh');
			}
		}
	}

	public function kategori_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[15]');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Ubah Kategori',
				'hal' => 'aset/kategori_ubah',
				'data' => $this->maset->kategori_by_id($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->kategori_ubah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
				redirect('adminaset/aset/kategori','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
				redirect('adminaset/aset/kategori','refresh');
			}
		}
	}

	public function kategori_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset','refresh');
		$cek = $this->maset->hapus('asset_categories', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/kategori','refresh');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/kategori','refresh');
		}
	}










	public function sub_kategori()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[15]');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Sub Kategori',
				'hal' => 'aset/sub_kategori',
				'kategori' => $this->maset->kategori(),
				'data' => $this->maset->sub_kategori()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->sub_kategori_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
				redirect('adminaset/aset/sub_kategori','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
				redirect('adminaset/aset/sub_kategori','refresh');
			}
		}
	}

	public function sub_kategori_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[15]');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Ubah Sub Kategori',
				'hal' => 'aset/sub_kategori_ubah',
				'kategori' => $this->maset->kategori(),
				'data' => $this->maset->sub_kategori_by_id($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->sub_kategori_ubah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
				redirect('adminaset/aset/sub_kategori','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
				redirect('adminaset/aset/sub_kategori','refresh');
			}
		}
	}

	public function sub_kategori_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset','refresh');
		$cek = $this->maset->hapus('asset_sub_categories', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/sub_kategori','refresh');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/sub_kategori','refresh');
		}
	}















	public function detail_kategori()
	{
		$data = [
			'title' => 'Master Aset - Detail Kategori',
			'hal' => 'aset/detail_kategori',
			'kategori' => $this->maset->kategori(),
			'sub_kategori' => $this->maset->sub_kategori(),
			'data' => $this->maset->detail_kategori()
		];
		$this->load->view('layout', $data);
	}

	public function detail_kategori_tambah()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('subkategori', 'Sub Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Tambah Detail Kategori',
				'hal' => 'aset/detail_kategori_tambah',
				'kategori' => $this->maset->kategori(),
				'js' => 'aset/js'
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->detail_kategori_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
				redirect('adminaset/aset/detail_kategori','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
				redirect('adminaset/aset/detail_kategori','refresh');
			}
		}
	}

	public function detail_kategori_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset/detail_kategori','refresh');
		$cek = $this->maset->hapus('assets', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/detail_kategori','refresh');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/detail_kategori','refresh');
		}
	}

	public function detail_kategori_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset/detail_kategori','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('subkategori', 'Sub Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Ubah Detail Kategori',
				'hal' => 'aset/detail_kategori_ubah',
				'data' => $this->maset->detail_kategori_by_id($id),
				'kategori' => $this->maset->kategori(),
				'subkategori' => $this->maset->sub_kategori_by_idaset($id),
				'js' => 'aset/js'
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->detail_kategori_ubah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
				redirect('adminaset/aset/detail_kategori','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
				redirect('adminaset/aset/detail_kategori','refresh');
			}
		}
	}












	public function aset()
	{
		$data = [
			'title' => 'Master Aset',
			'hal' => 'aset/aset',
			'data' => $this->maset->aset()
		];
		$this->load->view('layout', $data);
	}

	public function aset_tambah()
	{
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		// $this->form_validation->set_rules('subkategori', 'Sub Kategori', 'trim|required|numeric');
		// $this->form_validation->set_rules('detail', 'Detail', 'trim|required|numeric');
		// $this->form_validation->set_rules('kondisi', 'Kondisi', 'trim|required|numeric');
		// $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|numeric');
		// $this->form_validation->set_rules('unit', 'Unit', 'trim|required|numeric');
		// $this->form_validation->set_rules('file', 'Gambar', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama Aset', 'trim|required|max_length[100]');
		// $this->form_validation->set_rules('tipe', 'Tipe Aset', 'trim|required|max_length[100]');
		// $this->form_validation->set_rules('serial', 'Serial Number', 'trim|required|max_length[100]');
		// $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[255]');

		$config['upload_path'] = '../../../../aset/web/assets/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']  = '1024';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			var_dump($error); die();
		}
		else{
			$data = array('upload_data' => $this->upload->data());
			echo "success";
			var_dump($data); die();
		}

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Tambah Data',
				'hal' => 'aset/aset_tambah',
				'js' => 'aset/js',
				'kategori' => $this->maset->kategori(),
				'kondisi' => $this->maset->kondisi(),
				'unit' => $this->munit->data()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->aset_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
				redirect('adminaset/aset/aset','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
				redirect('adminaset/aset/aset','refresh');
			}
		}
	}

	public function aset_ubah($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset/aset','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('subkategori', 'Sub Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required|numeric');
		$this->form_validation->set_rules('kondisi', 'Kondisi', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama Aset', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('tipe', 'Tipe Aset', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('serial', 'Serial Number', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Aset - Ubah Data',
				'hal' => 'aset/aset_ubah',
				'data' => $this->maset->aset_by_id($id),
				'kategori' => $this->maset->kategori(),
				'subkategori' => $this->maset->sub_kategori_by_idproduk($id),
				'detailkategori' => $this->maset->detail_kategori_by_idproduk($id),
				'js' => 'aset/js',
				'kondisi' => $this->maset->kondisi(),
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->maset->aset_ubah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
				redirect('adminaset/aset/aset','refresh');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
				redirect('adminaset/aset/aset','refresh');
			}
		}
	}

	public function aset_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/aset/aset','refresh');
		$cek = $this->maset->hapus('asset_products', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/aset','refresh');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
			redirect('adminaset/aset/aset','refresh');
		}
	}
























	public function getsubkategori()
	{
		$kategori = $this->input->get('kategori');
		$data = [
			'data' => $this->maset->getsubkategori($kategori)
		];
		$this->load->view('aset/subkategori', $data);
	}

	public function getdetailkategori()
	{
		$subkategori = $this->input->get('subkategori');
		$kategori = $this->input->get('kategori');
		$data = [
			'data' => $this->maset->getdetailkategori($kategori,$subkategori)
		];
		$this->load->view('aset/detailkategori', $data);
	}

}

/* End of file Aset.php */
/* Location: ./application/modules/adminaset/controllers/Aset.php */