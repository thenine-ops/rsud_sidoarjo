<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apps extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$admin_auth = $this->session->userdata('rsud_admin_pusat');
		if(!$admin_auth){
			redirect(base_url('authenticate'));
		}
	}
	public function index()
	{
		$data['page'] = 'dashboard/dashboard';
		$data['sidebar'] = 'apps';
		$this->load->view('general/main', $data);
	}
	public function lapor_cepat()
	{
		$data['page'] = 'dashboard/lapor_cepat';
		$data['sidebar'] = 'apps';
		$this->load->view('general/main', $data);
	}
	public function get_aktivitas()
	{
		$data['page'] = 'data_user';
		$data['sidebar'] = 'apps';
		$this->load->view('general/main', $data);
	}
	public function get_today_activity($status)
	{
		if($status != 'all'){
			$this->db->where('trs.name', $status);
		}
		$this->db->join('troubleshooting_status trs', 'trs.id = trt.troubleshootingstatus_id', 'left');
		$this->db->where('trt.ticket_date', date('Y-m-d'));
		$get_data = $this->db->get('troubleshooting_tickets trt')->result();

		echo count($get_data);
	}
	public function dashboard_data($status)
	{
		if($status != 'all'){
			$this->db->where('trs.name', $status);
		}
		$this->db->join('troubleshooting_tickets trt', 'trt.id = tr.troubleshootingticket_id', 'left');
		$this->db->join('troubleshooting_status trs', 'trs.id = trt.troubleshootingstatus_id', 'left');
		$this->db->join('troubleshooting_activities tra', 'tra.id = tr.troubleshootingactivity_id', 'left');
		$this->db->join('room_categories rc', 'rc.id = trt.roomcategory_id', 'left');
		$this->db->join('room_details r', 'r.id = trt.roomdetail_id', 'left');
		$this->db->join('asset_products asc', 'asc.id = tr.assetproduct_id', 'left');

		$this->db->select('tr.id_ticket_register as ticket');
		$this->db->select('trt.ticket_date as date');
		$this->db->select('trt.user_name as user_name');
		$this->db->select('asc.category_name as assetcategory_name');
		$this->db->select('asc.name as asset_name');
		$this->db->select('rc.name as roomcategory_name');
		$this->db->select('r.name as room_name');
		$this->db->select('trs.name as status');
		$this->db->where("(EXTRACT(YEAR FROM trt.ticket_date) = '".date('Y')."' OR trs.name != 'close')");
		$get_data = $this->db->get('troubleshootings tr')->result();
		$no=1;

		foreach ($get_data as $item) {
			$bg_status = 'c-trans';
			if(trim($item->status) == 'Open'){
				$bg_status = 'c-danger';
			} else if(trim($item->status) == 'Pending'){
				$bg_status = 'c-warning';
			} else if(trim($item->status) == 'Close'){
				$bg_status = 'c-success';
			} 


			echo "<tr class='ff-item'>";
			echo "<td>".$no++."</td>";
			echo "<td>".$item->ticket."</td>";
			echo "<td>".tanggalIndo($item->date)."</td>";
			echo "<td>".$item->user_name."</td>";
			echo "<td></td>";
			echo "<td>".$item->assetcategory_name.' - '.$item->asset_name."</td>";
			echo "<td>".$item->roomcategory_name.' - '.$item->room_name."</td>";
			echo "<td></td>";
			echo "<td><div class='pad-xs $bg_status'>".ucwords($item->status)."</div></td>";
			echo "<td></td>";
			echo "</tr>";
		}
	}

	public function act_lapor_cepat()
	{
		$input = $this->input->post();
		$input['tanggal'] = date('Y-m-d');
		$input['waktu'] = date('H:i:s');

		$this->session->set_userdata('data_lapor_cepat', $input);

		$data = $this->session->userdata('data_lapor_cepat');
		if($data){
			$feedback['res'] = "success";
		}else{
			$feedback['res'] = "fail";
			$feedback['msg'] = "Koneksi eror!";
		}

		echo json_encode($feedback);
	}
	
}
