<?php 
if($sidebar == 'account'){
	$title = 'My Account';
}else if($sidebar == 'apps'){
	$title = 'Application';
}
$data['user_log'] = $this->session->userdata('rsud_admin_pusat');
$data['header_title'] = $title;
$data['menu'] = $sidebar;
$this->load->view('top_src_content', $data);
?>
<div class="content-container">
	<div class="col col-content">
		<?php $this->load->view($sidebar.'/'.$page, $data); ?>
	</div>
	<div class="col col-menu">
		<div class="menu-logo">
			<img src="<?= base_url().'prabotan/image/asset/app-logo.png' ?>" alt="">
		</div>
		<?php $this->load->view('sidebar', $data); ?>
	</div>
</div>
<?php 
$this->load->view('bot_src_content', $data);
?>