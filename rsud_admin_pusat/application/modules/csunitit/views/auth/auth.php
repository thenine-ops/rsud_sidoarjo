<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$title;?> | RSUD Sidoarjo Management Service App</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('aset');?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?=base_url('aset');?>/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?=base_url('aset');?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>aset/internal/css/form.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>aset/internal/css/button_action.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>aset/internal/css/modal.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>aset/internal/css/loading.css">
  <!-- Theme style -->

  <?php (isset($css)) ? $this->load->view($css) : ""; ?>
  <style>
  .bg {
  width: 100%;
  background-size: cover;
  background-image: url('<?=base_url('aset');?>/image/asset/login-background.jpg') !important;
  background-position: center;
  background-repeat: no-repeat;
  }
  .box-log{
    height: 400px; overflow: hidden; 
    overflow-y: visible;
    border-radius:30px;
    background:none;
  }
  </style>
</head>
<body class="login-page bg" >
<div class="row">
  <div class="col-md-6 px-0">
    <div class="login-box">
      <div class="card box-log">
    <div class="card-body login-card-body" style="background:rgba(225,225,225,0.5)">
    <img src="<?=base_url('aset');?>/image/asset/app-logo.png" height="100px" />
      <?php $this->load->view($hal); ?>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?=base_url('aset');?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('aset');?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('aset');?>/dist/js/adminlte.min.js"></script>
<script src="<?=base_url()?>aset/internal/js/button_action.js"></script>
<?php (isset($js)) ? $this->load->view($js) : ""; ?>

</body>
</html>
