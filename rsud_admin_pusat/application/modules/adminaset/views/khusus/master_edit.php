<?=$this->session->flashdata('name');?>
<div class="mx-3">
    <?=form_open();?>
      <input type="hidden" name="id" id="inputId" class="form-control" value="<?=$data->id;?>">
        <div class="form-group">
          <label for="kategori">Kategori Aset</label>
          <select class="form-control <?=(form_error('kategori'))?'is-invalid':'';?>" name="kategori" id="kategori">
            <option>Pilih Kategori</option>
            <?php foreach($sub_kategori as $k): ?>
            <option value="<?=$k->id;?>" <?=($data->assetsubcategory_id==$k->id)?'selected':'';?>><?=$k->namasubkategori;?></option>
            <?php endforeach; ?>
          </select>
          <?=form_error('kategori','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Spare Part Khusus</label>
            <input type="text" class="form-control <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" aria-describedby="nama" placeholder="Nama Spare Part" value="<?=$data->name;?>">
            <?=form_error('nama','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="description">Deskripsi</label>
          <textarea class="form-control <?=(form_error('description'))?'is-invalid':'';?>" name="description" id="description" rows="3" placeholder="Deskripsi Spare Part Umum"><?=$data->description;?></textarea>
          <?=form_error('description','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="quantity">Quantity</label>
          <input type="text" class="form-control <?=(form_error('quantity'))?'is-invalid':'';?>" name="quantity" id="quantity" aria-describedby="quantity" placeholder="Quantity" value="<?=$data->quantity;?>">
          <?=form_error('quantity','<small class="text-danger">','</small>');?>
        </div>
    <button type="submit" class="btn btn-success btn-lg">Submit</button>
    <a href="<?=base_url('adminaset/khusus');?>" class="btn btn-dark btn-lg">Kembali</a>
    </form>
</div>