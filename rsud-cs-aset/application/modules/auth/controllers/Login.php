<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth','ma');
	}

	public function index()
	{
		parent::login();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Petugas Aset IT',
				'hal' => 'auth/index'
			];
			$this->load->view('auth/auth', $data);
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$cek = $this->ma->login();
		if ($cek) {
			$data = [
				'id' => $cek->id,
				'nama' => $cek->nama,
				'role_id' => $cek->role_id,
				'unit_id' => $cek->unit_id,
				'unit' => $cek->unitname,
				'role' => $cek->rolename,
			];
			$this->session->set_userdata($data);
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Selamat datang kembali!</div>');
			redirect('intro','refresh');
		} else {
			$this->gagal_login();
		}
	}

	public function alertService($js){
		$data = [
			'title' => 'Login Petugas Aset IT',
			'hal' => 'auth/index',
			'css' => 'auth/lupa_css',
			'js' => 'auth/'.$js
		];
		$this->load->view('auth/auth', $data);
	}
	
	public function gagal()
	{
		$this->alertService('gagal');
	}
	
	public function gagal_login()
	{
		$this->alertService('gagal_login');
	}

	public function berhasil()
	{
		$this->alertService('berhasil');
	}

	public function recover()
	{
		$this->alertService('recover_js');
	}

	public function keluar()
	{
		unset($_SESSION['id']);
		unset($_SESSION['nama']);
		unset($_SESSION['role_id']);
		unset($_SESSION['unit_id']);
		session_destroy();
		redirect('auth','refresh');
	}

}

/* End of file Login.php */
/* Location: ./application/modules/auth/controllers/Login.php */