<h3>DASHBOARD</h3>
<div class="flex">
	<div class="f-col-3 f-float-round pad-lg">
		<div class="image-round">
			<label class="profil-init"><?= get_acronym($user_log['name']) ?></label>
			<img src="<?= base_url() ?>prabotan/image/profile_photo/-.jpg" alt="">
		</div>
		<div class="bg-profil"></div>
	</div>
	<div class="f-col-7 f-float-round pad-sm">
		<div class="label">
			<div class="header">Nama</div>
			<div class="desc-clr"><?= strtoupper($user_log['name']) ?></div>
		</div>
		<div class="label">
			<div class="header">Posisi</div>
			<div class="desc-clr"><?= strtoupper($user_log['role']) ?></div>
		</div>
		<div class="label">
			<div class="header">Status</div>
			<?php
			if($user_log['status'] == 1){
				?>
				<div class="status bg-success">Aktif</div>
				<?php
			} else{
				?>
				<div class="status bg-danger">Tidak Aktif</div>
				<?php
			}
			?>
		</div>	
	</div>
</div>
<div class="flex">
	<div class="f-col">
		<div class="label">
			<div class="header">Email</div>
			<div class="desc"><?= $user_log['email'] ?></div>
		</div>
		<div class="label">
			<div class="header">No. Telp</div>
			<div class="desc"><?= '+62 '.$user_log['phone'] ?></div>
		</div>
	</div>
</div>
