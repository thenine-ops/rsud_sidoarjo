<script src="<?=base_url("aset/plugins/select2/js/select2.full.min.js")?>"></script>
<script src="<?=base_url("aset/plugins/sweetalert2/sweetalert2.all.min.js")?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('.select2').select2();
	});
</script>

<?php if($this->session->flashdata('berhasil')!==null){ ?>
<script type="text/javascript">
	Swal.fire({
	  icon: 'success',
	  title: 'Berhasil ditambahkan',
	  text: 'Silahkan lihat fitur STOK SPARE PART untuk melihat stok sparepart secara lebih detail'
	})
</script>
<?php } ?>