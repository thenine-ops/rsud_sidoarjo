<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$admin_auth = $this->session->userdata('rsud_admin_pusat');
		if(!$admin_auth){
			redirect(base_url('authenticate'));
		}
	}
	public function index()
	{
		$data['page'] = 'account_profile';
		$data['sidebar'] = 'account';
		$this->load->view('general/main', $data);
	}
	public function edit_profile()
	{
		$data['page'] = 'edit_profile';
		$data['sidebar'] = 'account';
		$this->load->view('general/main', $data);
	}
	public function edit_password()
	{
		$data['page'] = 'edit_password';
		$data['sidebar'] = 'account';
		$this->load->view('general/main', $data);
	}
	public function act_edit_profile(){
		$dataInput = $this->input->post();

		$this->db->where('id_account_register !=', $dataInput['id_account_register']);
		$this->db->where('email', $dataInput['email']);
		$cek_email = $this->db->get('accounts')->row();
		if(count($cek_email) == 0){
			$id = $dataInput['id_account_register'];
			unset($dataInput['id_account_register']);
			
			$this->db->where('id_account_register', $id);
			$this->db->update('accounts', $dataInput);

			$this->db->where('id_account_register', $id);
			$this->db->select('acc.id_account_register');
			$this->db->select('acc.name as name');
			$this->db->select('ro.description as role');
			$this->db->select('un.name as unit');
			$this->db->select('acc.status as status');
			$this->db->select('acc.email as email');
			$this->db->select('acc.phone as phone');
			$this->db->join('roles ro', 'ro.id = usr.role_id', 'left');
			$this->db->join('units un', 'un.id = usr.unit_id', 'left');
			$this->db->join('accounts acc', 'acc.id = usr.account_id', 'left');
			$get_sess = $this->db->get('users usr')->row_array();

			$this->session->set_userdata('rsud_admin_pusat', $get_sess);
			
			$feedback['res'] = "success";
		}else{
			$feedback['res'] = "fail";
			$feedback['msg'] = "Email sudah dipakai!";
		}
		echo json_encode($feedback);
	}
	public function act_edit_password	(){
		$dataInput = $this->input->post();

		$this->db->where('id_account_register', $dataInput['id_account_register']);
		$cek_acc = $this->db->get('accounts')->row();

		$this->db->where('account_id', $cek_acc->id);
		$this->db->where('password', $dataInput['old_password']);
		$cek_pass = $this->db->get('users')->row();

		if(($dataInput['new_password'] == $dataInput['new_password2']) && (count($cek_pass) == 1)){
			
			$data['password'] = $dataInput['new_password'];
			$this->db->where('account_id', $cek_acc->id);
			$this->db->update('users', $data);
			
			$feedback['res'] = "success";
		}else if(count($cek_pass) == 0){
			$feedback['res'] = "fail";
			$feedback['msg'] = "Kata sandi lama yang anda masukkan salah!";
		}
		else{
			$feedback['res'] = "fail";
			$feedback['msg'] = "Kata sandi baru dan konfirmasi kata sandi tidak sama!";
		}
		
		echo json_encode($feedback);
	}
}
