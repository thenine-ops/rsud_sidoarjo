<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sparepart extends CI_Model {

	public function lastId($tabel)
	{
		$data = $this->db->select("nextval(pg_get_serial_sequence('$tabel', 'id')) as id")->get($tabel)->row();
		if ($data->id<=9) {
			$d = '0'.$data->id;
		} else {
			$d = $data->id;
		}
		
		return $d;
	}

	public function hapus($tabel, $id)
	{
		return $this->db->delete($tabel, ['id'=>$id]);
	}

	public function stok($stok='1')
	{
		if ($stok!='1') {
			$this->db->where('quantity <=','0');
		}
		return $this->db->get('spareparts');
	}

	public function data($stok='1')
	{
		return $this->stok($stok)->result();
	}

	public function jumlah($stok='1')
	{
		return $this->stok($stok)->num_rows();
	}

	public function data_sparepart()
	{
		return $this->db->select('a.name as aset, s.name as sparepart, s.quantity, s.description, s.id')
				 ->from('spareparts s')
				 ->join('assets a','s.asset_id=a.id')
				 ->get()
				 ->result()
		;
	}

	public function detail_sparepart($id)
	{
		return $this->db->where('id', $id)
				 ->get('spareparts')
				 ->row()
		;
	}

	public function sparepart_tambah()
	{
		$post = $this->input->post();
		$data = [
			'asset_id' => htmlspecialchars(trim($post['aset'])),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		return $this->db->insert('spareparts', $data);
	}

	public function sparepart_edit()
	{
		$post = $this->input->post();
		$data = [
			'asset_id' => htmlspecialchars(trim($post['aset'])),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		$where = [
			'id' => htmlspecialchars(trim($post['id']))
		];
		return $this->db->update('spareparts', $data, $where);
	}

	public function kategori()
	{
		return $this->db->order_by('name','asc')->get('sparepart_categories')->result();
	}

	public function kategori_by_id($id)
	{
		return $this->db->where('id',$id)->get('sparepart_categories')->row();
	}

	public function kategori_tambah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['nama'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		return $this->db->insert('sparepart_categories', $object);
	}

	public function kategori_ubah()
	{
		$post = $this->input->post();
		$object = [
			'name' => htmlspecialchars(trim($post['nama']))
		];
		$where = [
			'id' => htmlspecialchars(trim($post['id']))
		];
		return $this->db->update('sparepart_categories', $object, $where);
	}

	public function data_umum()
	{
		return $this->db->select('sp.id, sp.id_sparepart_register serial, sp.name nama, sp.quantity, sp.type, sp.description')
						->from('sparepart_products sp')
						->join('spareparts s','s.id=sp.sparepart_id')
						->join('sparepart_categories sc','sc.id=sp.sparepartcategory_id')
						->order_by('sp.name','asc')
						->get()
						->result()
		;
	}

	public function data_umum_by_id($id)
	{
		return $this->db->where('id',$id)->get('sparepart_products')->row();
	}

	public function umum_tambah()
	{
		$post = $this->input->post();
		$data = [
			'sparepart_id' => htmlspecialchars(trim($post['sparepart'])),
			'sparepartcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'id_sparepart_register' => 'SP-'.date('Ymd').'0'.$this->lastId('sparepart_products'),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'type' => htmlspecialchars(trim($post['type'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()');
		return $this->db->insert('sparepart_products', $data);
	}

	public function umum_edit()
	{
		$post = $this->input->post();
		$data = [
			'sparepart_id' => htmlspecialchars(trim($post['sparepart'])),
			'sparepartcategory_id' => htmlspecialchars(trim($post['kategori'])),
			'name' => htmlspecialchars(trim($post['nama'])),
			'description' => htmlspecialchars(trim($post['description'])),
			'quantity' => htmlspecialchars(trim($post['quantity'])),
			'type' => htmlspecialchars(trim($post['type'])),
			'user_ent' => $this->session->id
		];
		$where = [
			'id' => htmlspecialchars(trim($post['id'])),
		];
		$this->db->set('date_ent','now()');
		return $this->db->update('sparepart_products', $data, $where);
	}

}

/* End of file M_sparepart.php */
/* Location: ./application/modules/adminaset/models/M_sparepart.php */