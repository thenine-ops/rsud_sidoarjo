<?=$this->session->flashdata('name');?>
<div class="row mb-3">
  <div class="col-md"><a href="<?=base_url('adminaset/pengguna');?>" class='h4'>Data Pengguna</a></div>
</div>
<div class="flex mb-4">
  <div class="f-col-5 f-float-round pad-lg">
    <div class="row">
      <div class="col-md">
        <select name="karyawan" id="inputKaryawan" class="form-control" required="required" disabled="on">
          <option value="">Pilih Nama Karyawan</option>
        </select>
      </div>
      <div class="col-md-2">
        <button type="button" class="btn btn-danger btn-block rounded" disabled="on">Lock</button>
      </div>
    </div>
    </div>
</div>

<div class="flex mb-4">
  <div class="f-col-5 f-float-round pad-lg">
    <span class="h4">Edit Data Pengguna</span>
    <?=validation_errors();?>
    <?=form_open('adminaset/pengguna/edit/'.$data->id);?>
      <input type="hidden" name="id" id="inputId" class="form-control" value="<?=$data->id;?>">

      <div class="form-group">
        <input type="text" class="form-control<?=(form_error('nama'))?' is-invalid':'';?>" name="nama" placeholder="Nama Karyawan" value="<?=$data->name;?>">
        <?=form_error('nama');?>
      </div>
      
      <div class="form-group">
        <input type="text" class="form-control<?=(form_error('username'))?' is-invalid':'';?>" name="username" placeholder="Username" value="<?=$data->username;?>">
        <?=form_error('username');?>
      </div>
      
      <div class="form-group">
        <input type="text" class="form-control<?=(form_error('password'))?' is-invalid':'';?>" name="password" placeholder="Password" value="<?=$data->password;?>">
        <?=form_error('password');?>
      </div>
      
      <div class="form-group">
        <input type="text" class="form-control<?=(form_error('email'))?' is-invalid':'';?>" name="email" placeholder="Email" value="<?=$data->email;?>">
        <?=form_error('email');?>
      </div>
      
      <div class="form-group">
        <input type="text" class="form-control<?=(form_error('phone'))?' is-invalid':'';?>" name="phone" placeholder="No.Telephone" value="<?=$data->phone;?>">
        <?=form_error('phone');?>
      </div>
      
      <div class="form-group">
        <select name="posisi" id="inputPosisi" class="form-control" required="required">
          <option value="2" <?=($data->unitid==2)?'selected':'';?>>Petugas Aset IT</option>
          <option value="3" <?=($data->unitid==3)?'selected':'';?>>Petugas Aset IPS</option>
        </select>
        <?=form_error('posisi');?>
      </div>
      
      <div class="form-group">
        <select name="status" id="inputStatus" class="form-control" required="required">
          <option value="1" <?=($data->status==1)?'selected':'';?>>Aktif</option>
          <option value="0" <?=($data->status==0)?'selected':'';?>>Non-Aktif</option>
        </select>
        <?=form_error('status');?>
      </div>

      <button type="submit" class="btn btn-success rounded pull-right btn-lg">Simpan</button>
    </form>
  </div>
</div>