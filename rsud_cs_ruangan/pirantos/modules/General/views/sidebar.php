<?php
$set_menu['account'] = [
	array(
		'name' => 'dashboard', 'link' => '', 'icon' => 'avatar.png',
		'desc' => 'Merupakan data preview profil pengguna',
		'status' => ''
	),
	array(
		'name' => 'ubah profil', 'link' => 'edit_profile', 'icon' => 'followers.png',
		'desc' => 'Merubah data profil pengguna',
		'status' => ''
	),
	array(
		'name' => 'ubah kata sandi', 'link' => 'edit_password', 'icon' => 'lock.png',
		'desc' => 'Merubah kata sandi untuk keamanan akun pengguna',
		'status' => ''
	),
];
$set_menu['apps'] = [
	array(
		'name' => 'dashboard', 'link' => '', 'icon' => 'avatar.png',
		'desc' => 'Merupakan preview dari aktivitas yang dilakukan unit ruangan',
		'status' => ''
	),
	array(
		'name' => 'perbaikan', 'link' => 'perbaikan', 'icon' => 'maintenance.png',
		'desc' => 'Permintaan perbaikan dari unit ruangan sebagai pelapor',
		'status' => 'coming-soon'
	),
	array(
		'name' => 'data aset', 'link' => 'data_aset', 'icon' => 'maintenance2.png',
		'desc' => 'Aset berupa sarana dan peralatan yang berada di ruangan',
		'status' => 'coming-soon'
	),
	array(
		'name' => 'laporan', 'link' => 'laporan_aktivitas', 'icon' => 'report.png',
		'desc' => 'Laporan aktifitas permintaan perbaikan yang dilakukan unit ruangan',
		'status' => 'coming-soon'
	)
];
$menu_uri = $this->uri->segment(2);
?>
<div class="menu-button">
	<?php 
	foreach ($set_menu[$menu] as $value) {
		?>
		<a href="<?= base_url().$menu.'/'.$value['link'] ?>" class="menu-item <?= $value['status'] ?> <?php if($menu_uri==$value['link'] || ($value['link'] == '' && $menu_uri=='lapor_cepat')){ echo 'active';} ?>">
			<div class="menu-icon">
				<img src="<?= uri_img('asset', $value['icon']) ?>" alt="">
			</div>
			<div class="menu-text">
				<div class="menu-title"><?= strtoupper($value['name']) ?></div>
				<div class="menu-desc"><?= $value['desc'] ?></div>
			</div>
		</a>
		<?php	
	}
	?>
</div>