<h3>TIKET PERMINTAAN</h3>
<?php
$data = $this->session->userdata('data_lapor_cepat');

$get_data_unit
?>
<div class="flex">
	<div class="f-col f-float-round">
		<div class="flex">
			<div class="f-col pad-sm">
				<div class="label mar-v-xs">
					<div class="title">ID Tiket</div>
					<div class="desc-clr"><?= $data['kode_laporan'] ?></div>
				</div>
				<div class="label mar-v-xs">
					<div class="title">Waktu</div>
					<div class="desc-clr"><?= $data['waktu'] ?> WIB</div>
				</div>
			</div>
			<div class="f-col pad-sm">
				<div class="label mar-v-xs">
					<div class="title">Pelapor</div>
					<div class="desc-clr"><?= $data['nama_pelapor'] ?></div>
				</div>
				<div class="label mar-v-xs">
					<div class="title">Permintaan Ke Unit</div>
					<select class="form-control" name="">
						<option value="">-- Choose --</option>}
					</select>
				</div>
			</div>
			<div class="f-col pad-sm">
				<div class="label mar-v-xs">
					<div class="title">Tanggal</div>
					<div class="desc-clr"><?= tanggalIndo($data['tanggal']) ?></div>
				</div>
				<br>
				<button type="button" class="btn btn-danger btn-block">LOCK</button>
			</div>
		</div>
	</div>
</div>
<div class="flex">
	<div class="f-col f-float-round pad-sm">
		<div class="flex-in">
			<div class="f-col">
				<div class="label">
					<div class="title">Area</div>
					<select class="form-control" name="">
						<option value="">-- Pilih Area --</option>}
					</select>
				</div>
			</div>
			<div class="f-col mar-0">
				<div class="label">
					<div class="title">Detail Area</div>
					<select class="form-control" name="">
						<option value="">-- Pilih Detail Area --</option>}
					</select>
				</div>
			</div>
		</div>
			<div class="label">
				<div class="title">Aset</div>
				<select class="form-control" name="">
					<option value="">-- Pilih Aset --</option>}
				</select>
			</div>
		<div class="flex-in">
			<div class="f-col-8">
				<div class="label">
					<div class="title">Keluhan</div>
					<textarea name="" class="form-control"></textarea>
				</div>
			</div>
			<div class="f-col-2 mar-0">
				<br>
				<br>
				<button type="submit" class="btn btn-primary rounded btn-block">TAMBAH</button>
			</div>
		</div>
	</div>
</div>
<div class="flex">
	<div class="f-col f-float-round pad-sm">
		<table class="">
			<tbody>
				<tr>
					<td width="1%">1.</td>
					<td>Data</td>
					<td>Data</td>
					<td>Data</td>
					<td>
						<button class='button-action' value="1"></button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>