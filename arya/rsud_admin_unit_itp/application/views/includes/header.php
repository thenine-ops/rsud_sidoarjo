<!DOCTYPE html>
<html>
<head>
	<title>Apps - Dashboard</title>

	<!-- Javascript External -->
	<script src="<?= base_url();?>aset/external/js/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="<?= base_url();?>aset/external/js/bootstrap.min.js"></script>
	<script src="<?= base_url();?>aset/external/js/highcharts.js"></script>
	<script src="<?= base_url();?>aset/external/js/highcharts-exporting.js"></script>
	<script src="<?= base_url();?>aset/external/js/highcharts-export-data.js"></script>
	<script src="<?= base_url();?>aset/external/js/highcharts-accessibility.js"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url();?>aset/internal/js/button_action.js"></script>
	<script src="<?= base_url();?>aset/internal/js/modal.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>aset/external/css/bootstrap.min.css">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>aset/internal/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>aset/internal/css/content.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>aset/internal/css/button_action.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>aset/internal/css/modal.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>aset/internal/css/loading.css">
</head>