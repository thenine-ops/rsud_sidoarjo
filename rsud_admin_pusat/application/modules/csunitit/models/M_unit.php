<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_unit extends CI_Model {

	public function data()
	{
		return $this->db->get('units')->result();
	}

	public function jumlah() // jumlah seluruh unit
	{
		return $this->db->get('units')->num_rows();
	}

	public function detail($id)
	{
		return $this->db->where('id', $id)->get('units')->row();
	}

	public function dashboard_info($unit, $status)
	{
		/**

		$unit 
			1 => Pusat
			2 => Unit IT
			3 => Unit IPS
			4 => Unit ITP
			5 => Unit Ruangan
			Yang dipakai hanya 2, 3 dan 4

		$status
			1 => Open
			2 => Close
			3 => Pending
			Yang dipakai hanya 1 dan 2

		*/
		$query = "
				select t.id
				from troubleshooting_tickets t
				join troubleshooting_status s on s.id=t.troubleshootingstatus_id
				where t.unit_id='$unit' and s.id='$status'
		";
		return $this->db->query($query)->num_rows();
	}

	public function cari($type='1', $unit='2')
	{
		return $this->db->select('tiket.id as idtiket, tipe.name as jenis, tiket.id_ticket_register as tiket, tiket.date_ent as waktu, aktifitas.name as status_pelayanan, status.name as status_ticket')
				 ->from('troubleshooting_tickets tiket')
				 ->join('troubleshooting_types tipe','tipe.id=tiket.troubleshootingtype_id')
				 ->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
				 ->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
				 ->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
				 ->join('units u','u.id=tiket.unit_id')
				 ->where('tiket.troubleshootingtype_id', $type)
				 ->where('tiket.unit_id', $unit)
				 ->get()
				 ->result();
	}

	public function jumlah_dashboard($activity='1', $unit='2')
	{
		return $this->db->select('tiket.id as idtiket, tipe.name as jenis, tiket.id_ticket_register as tiket, tiket.date_ent as waktu, aktifitas.name as status_pelayanan, status.name as status_ticket')
				 ->from('troubleshooting_tickets tiket')
				 ->join('troubleshooting_types tipe','tipe.id=tiket.troubleshootingtype_id')
				 ->join('troubleshootings trobel','trobel.troubleshootingticket_id=tiket.id')
				 ->join('troubleshooting_activities aktifitas','aktifitas.id=trobel.troubleshootingactivity_id')
				 ->join('troubleshooting_status status','status.id=tiket.troubleshootingstatus_id')
				 ->join('units u','u.id=tiket.unit_id')
				 ->where('trobel.troubleshootingactivity_id', $activity)
				 ->where('tiket.unit_id', $unit)
				 ->get()
				 ->num_rows();
	}

}

/* End of file M_unit.php */
/* Location: ./application/modules/csunitit/models/M_unit.php */