<?=$this->session->flashdata('name');?>
<div class="mx-3">
    <?=form_open();?>
        <div class="form-group">
          <label for="kategori">Kategori Spare Part</label>
          <select class="form-control <?=(form_error('kategori'))?'is-invalid':'';?>" name="kategori" id="kategori">
            <option>Pilih Kategori</option>
            <?php foreach($kategori as $k): ?>
            <option value="<?=$k->id;?>"><?=$k->name;?></option>
            <?php endforeach; ?>
          </select>
          <?=form_error('kategori','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="sparepart">Spare Part</label>
          <select class="form-control <?=(form_error('sparepart'))?'is-invalid':'';?>" name="sparepart" id="sparepart">
            <option>Pilih Spare Part</option>
            <?php foreach($sparepart as $k): ?>
            <option value="<?=$k->id;?>"><?=$k->sparepart;?></option>
            <?php endforeach; ?>
          </select>
          <?=form_error('sparepart','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Spare Part Umum</label>
            <input type="text" class="form-control <?=(form_error('nama'))?'is-invalid':'';?>" name="nama" id="nama" aria-describedby="nama" placeholder="Nama Spare Part">
            <?=form_error('nama','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="type">Type</label>
          <input type="text" class="form-control <?=(form_error('type'))?'is-invalid':'';?>" name="type" id="type" aria-describedby="type" placeholder="Type Spare Part">
          <?=form_error('type','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="description">Deskripsi</label>
          <textarea class="form-control <?=(form_error('description'))?'is-invalid':'';?>" name="description" id="description" rows="3" placeholder="Deskripsi Spare Part Umum"></textarea>
          <?=form_error('description','<small class="text-danger">','</small>');?>
        </div>
        <div class="form-group">
          <label for="quantity">Quantity</label>
          <input type="text" class="form-control <?=(form_error('quantity'))?'is-invalid':'';?>" name="quantity" id="quantity" aria-describedby="quantity" placeholder="Quantity">
          <?=form_error('quantity','<small class="text-danger">','</small>');?>
        </div>
    <button type="submit" class="btn btn-success btn-lg">Submit</button>
    <a href="<?=base_url('adminaset/sparepart/umum');?>" class="btn btn-dark btn-lg">Kembali</a>
    </form>
</div>