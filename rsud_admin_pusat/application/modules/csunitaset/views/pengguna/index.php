<?=$this->session->flashdata('name');?>
<div class="row mb-3">
  <div class="col-md"><a class='h4 text-primary'>Data Pengguna </a> <a href="<?=base_url('adminaset/pengguna/tambah');?>" class='h4 text-dark'>| Tambah Pengguna</a></div>
</div>
<div class="flex mb-4">
  <div class="f-col-5 f-float-round pad-lg">
    <div class="row">
      <div class="col-md">
        <h6>Filter Pengguna</h6>
        <?=form_open('adminaset/pengguna');?>
        <select name="unit" id="inputKaryawan" class="form-control <?=(form_error('unit'))?'is-invalid':'';?>">
          <option value="0">Semua Petugas</option>
          <option value="2">Petugas Aset IT</option>
          <option value="3">Petugas Aset IPS</option>
        </select>
        <?=form_error('unit');?>
      </div>
      <div class="col-md-2">
        <h6>&nbsp;</h6>
        <button type="submit" class="btn btn-danger btn-block rounded">Lock</button>
        <?=form_close();?>
      </div>
    </div>
    </div>
</div>

<div class="flex mb-4">
  <div class="f-col-5 f-float-round pad-lg">
    <h5>Data Pengguna</h5>
      <table class="table table-bordered">
          <tr class="bg-success text-light">
            <th scope="col">No</th>
            <th scope="col">Nama Pengguna</th>
            <th scope="col">Posisi</th>
            <th scope="col">Aksi</th>
          </tr>
          <?php $no=1; foreach($data as $d): ?>
          <tr>
            <th scope="row"><?=$no++;?></th>
            <td><?=$d->name;?></td>
            <td><?=$d->rolename;?> - <?=$d->unitname;?></td>
            <td>
              <a href="<?=base_url('adminaset/pengguna/detail/'.$d->id);?>" class="btn btn-success btn-sm rounded">detail</a>
              <a href="<?=base_url('adminaset/pengguna/edit/'.$d->id);?>" class="btn btn-warning btn-sm rounded">edit</a>
              <a href="<?=base_url('adminaset/pengguna/hapus/'.$d->id);?>" class="btn btn-danger btn-sm rounded">delete</a>
            </td>
          </tr>
          <?php endforeach; ?>
      </table>
  </div>
</div>