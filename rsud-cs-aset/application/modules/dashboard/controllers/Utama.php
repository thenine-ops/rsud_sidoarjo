<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utama extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth();
	}

	public function index()
	{
		echo "Dashboard";
	}

}

/* End of file Utama.php */
/* Location: ./application/modules/dashboard/controllers/Utama.php */