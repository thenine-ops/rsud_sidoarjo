<body class="dashboard-body">
    <div class="loading">
        <img src="<?=base_url();?>aset/image/asset/loading.gif" alt="">
    </div>
    <div class="modal-item"></div>

    <div class="header-container">
        <div class="rounded c-base">
            CS UNIT ITP
        </div>
        <div class="rounded c-trans">
            <div class="account-img c-base rounded">
                <label class="account-init">DF</label>
                <img src="<?=base_url();?>aset/image/profile_photo/profile.png" alt="">
            </div>
            <label class="account-name">Shiren Munaf / CS ITP</label>
        </div>
    </div>
    <div class="content-container">
        <div class="col col-content padding-content">
            <h4 class="bold">GANTI PETUGAS</h4>
            <div class="date-info f-green padding-tanggal">
                <a href="<?= site_url('csitp/ganti_petugas');?>" class="btn btn-sm rounded pull-right c-danger">x</a>
            </div>
            <div class="flex">
                <div class="f-col">
                    <div class="gray">Tanggal</div>
                    <button class="btn btn-outline-success w95"><?php $tgl = $result->transfer_date; $hasil =  explode(" ",$tgl); echo $hasil[0] ;?></button>
                </div>
                <div class="f-col">
                    <div class="gray">ID Tiket</div>
                    <button class="btn btn-outline-success w95"><?= "ITP_".$result->transporterticket_id;?></button>
                </div>
                <div class="f-col-right">
                    <div class="gray">Status</div>
                    <button class="btn btn-outline-danger w95 m-right0"> <?= $result->name_status;?></button> 
                </div>
            </div><br>
            <div class="flex f-float-round padding-tanggal">
                <div class="f-col">
                    <div class="head-form-control">Ruangan Penjemputan</div>
                    <div class="label m-input">
                        <input type="text" hidden="hidden" name="csrf_test_rsud" value="<?= $token; ?>">
                        <input type="text" hidden="hidden" name="room_ori" value="<?= $result->id_roomdetails_ori;?>">
                        <input type="text" class="form-control" value="<?= $result->name_room_origin;?>" placeholder="Nama Ruangan Penjemputan" disabled>
                    </div>
                    <div class="head-form-control">Ruangan Tujuan</div>
                    <div class="label m-input">
                        <input type="text" name="room_dest" hidden="hidden" value="<?= $result->id_roomdetails_dest;?>">
                        <input type="text" class="form-control" value="<?= $result->name_room_destination;?>" placeholder="Nama Ruangan Tujuan" disabled>
                    </div>
                    <div class="head-form-control">Petugas</div>
                    <div class="label m-input">
                        <input type="text" class="form-control" value="<?= $result->user_ent;?>" placeholder="Nama Petugas" disabled>
                    </div>
                </div>
            </div><br>
            <?= validation_errors(); ?>
            <?php $atrributs = array('id' => 'form_ganti_petugas'); ?>
            <?= form_open('csitp/save_ganti_petugas', $atrributs); ?>
            <input type="text" hidden="hidden" name="id_tiket" value="<?=$result->transporterticket_id;?>">
            <div class="flex" style="padding-right: 1px !important;">
                <div class="f-col">
                    <div class="head-form-control">Alasan Penggantian</div>
                    <div class="label m-input">
                        <textarea name="alasan_penggantian" id="alasan_penggantian" cols="10" rows="3" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="flex">
                <div class="f-col">
                    <div class="head-form-control">Petugas Pengganti</div>
                </div>
            </div>
            <div class="flex f-float-round padding-tanggal" style="padding-right: 0% !important;">
                <div class="flex">
                    <div class="f-col">
                        <select name="id_petugas" id="" class="form-control">
                            <option value="" selected disabled>Pilih Petugas</option>
                            <?php foreach ($petugas as $row) {; ?>
                                <option id="petugas_<?php echo $row->id; ?>" value="<?php echo $row->id; ?>"> <?php echo $row->user_ent; ?></option>
                            <?php }; ?>
                        </select>
                    </div>
                </div>
            </div><br>
            <button type="submit" class="btn btn-lg c-success pull-right">SIMPAN</button><br><br>
            <?= form_close();?>
        </div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?=base_url();?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?=base_url();?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="#" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?=base_url();?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/laporan_aktivitas_petugas'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?=base_url();?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
    </div>