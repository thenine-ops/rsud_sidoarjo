<a href="#" onclick="javascript:window.history.back()" class="btn btn-danger rounded pull-right">x</a>
<div class="flex pading-content" style="padding-bottom: 13px !important;">
    <div class="f-col-3 f-float-round">
        ini adalah image
    </div>
    <div class="f-col pad-xs">
        <div class="label">
            <h1><?=$data->name;?></h1>
            <h5 class="coral text-danger" style="margin-top: 10px;"><?=$data->id_sparepart_register;?></h5>
        </div>
        <div class="flex" style="margin-top: 20px;">
            <div class="f-col-12" style="margin-right: 3px;">
                <button class="btn btn-outline-primary rounded w100">SPAREPART UMUM</button>
            </div>
            <div class="f-col">
                <button class="btn btn-outline-success rounded w100">STOK: <?=$data->quantity;?></button>
            </div>
        </div>
    </div>
</div>
<div class="flex">
  <div class="f-col-7 f-float-round pad-sm">
    <span class="text-muted">Keterangan</span><br><?=$data->description;?>
  </div>
</div>