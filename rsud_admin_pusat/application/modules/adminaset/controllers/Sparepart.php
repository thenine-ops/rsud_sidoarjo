<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminaset');
		$this->load->model('M_sparepart','ms');
		$this->load->model('M_aset','maset');
	}

	public function index()
	{
		redirect('adminaset/sparepart/kategori','refresh');
	}

	public function kategori()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part - Kategori',
				'hal' => 'sparepart/kategori',
				'data' => $this->ms->kategori()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->kategori_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
			}
			redirect('adminaset/sparepart/kategori','refresh');
		}
	}

	public function kategori_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/sparepart','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part - Ubah Kategori',
				'hal' => 'sparepart/kategori_ubah',
				'data' => $this->ms->kategori_by_id($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->kategori_ubah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
			}
			redirect('adminaset/sparepart/kategori','refresh');
		}
	}

	public function kategori_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/sparepart','refresh');
		$cek = $this->ms->hapus('sparepart_categories', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		}
		redirect('adminaset/sparepart/kategori','refresh');
	}

	public function data()
	{		
		$data = [
			'title' => 'Master Spare Part',
			'hal' => 'sparepart/data',
			'aset' => $this->maset->data_aset(),
			'data' => $this->ms->data_sparepart()
		];
		$this->load->view('layout', $data);		
	}

	public function data_tambah()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		$this->form_validation->set_rules('aset', 'Aset', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part',
				'hal' => 'sparepart/data_tambah',
				'aset' => $this->maset->data_aset()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->sparepart_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
			}
			redirect('adminaset/sparepart/data','refresh');
		}
	}

	public function data_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/sparepart/data','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		$this->form_validation->set_rules('aset', 'Aset', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part',
				'hal' => 'sparepart/data_edit',
				'aset' => $this->maset->data_aset(),
				'data' => $this->ms->detail_sparepart($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->sparepart_edit();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
			}
			redirect('adminaset/sparepart/data','refresh');
		}
	}

	public function data_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/sparepart/data','refresh');
		$cek = $this->maset->hapus('spareparts', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		}
		redirect('adminaset/sparepart/data','refresh');
	}

	public function umum()
	{		
		$data = [
			'title' => 'Spare Part Umum',
			'hal' => 'sparepart/umum',
			'aset' => $this->maset->data_aset(),
			'data' => $this->ms->data_umum()
		];
		$this->load->view('layout', $data);		
	}

	public function umum_detail($id='')
	{	
		if(!$id or $id<=0) redirect('adminaset/sparepart/umum','refresh');
		$data = [
			'title' => 'Spare Part Umum',
			'hal' => 'sparepart/umum',
			'aset' => $this->maset->data_aset(),
			'data' => $this->ms->data_umum_by_id()
		];
		$this->load->view('layout', $data);		
	}

	public function umum_tambah()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('sparepart', 'sparepart', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Spare Part Umum - Tambah Data',
				'hal' => 'sparepart/umum_tambah',
				'sparepart' => $this->ms->data_sparepart(),
				'kategori' => $this->ms->kategori()
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->umum_tambah();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil ditambah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal ditambah!</div>');
			}
			redirect('adminaset/sparepart/umum','refresh');
		}
	}

	public function umum_edit($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/sparepart/umum','refresh');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|numeric');
		$this->form_validation->set_rules('sparepart', 'sparepart', 'trim|required|numeric');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Master Spare Part',
				'hal' => 'sparepart/umum_edit',
				'sparepart' => $this->ms->data_sparepart(),
				'kategori' => $this->ms->kategori(),
				'data' => $this->ms->data_umum_by_id($id)
			];
			$this->load->view('layout', $data);
		} else {
			$cek = $this->ms->umum_edit();
			if ($cek) {
				$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
			} else {
				$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Data gagal diubah!</div>');
			}
			redirect('adminaset/sparepart/umum','refresh');
		}
	}

	public function umum_hapus($id='')
	{
		if(!$id or $id<=0) redirect('adminaset/sparepart/umum','refresh');
		$cek = $this->maset->hapus('sparepart_products', $id);
		if ($cek) {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		}
		redirect('adminaset/sparepart/umum','refresh');
	}

}

/* End of file Sparepart.php */
/* Location: ./application/modules/adminaset/controllers/Sparepart.php */