<?php defined('BASEPATH') or exit('No direct script access allowed');

class Cs_unit_itp extends MY_controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_csitp');
        $this->load->helper('my_helper');
    }

    function index()
    {
        date_default_timezone_set("Asia/Jakarta");
        $data['main_content'] = 'tiket_pemindahan_pasien';
        $data['rooms'] = $this->M_csitp->get_rooms()->result();
        $data['token'] = $this->security->get_csrf_hash();
        $this->load->view('includes/template', $data);
    }

    function save_patient_movement()
    {
        date_default_timezone_set("Asia/Jakarta");
        $data['main_content'] = 'tiket_pemindahan_pasien';
        $data['token'] = $this->security->get_csrf_hash();
        $this->form_validation->set_rules('nama', 'Nama Pasien', 'required');
        $this->form_validation->set_rules('kontak', 'Kontak', 'required');
        $this->form_validation->set_rules('id_room_penjemputan', 'Nama Ruangan Penjemputan', 'required');
        $this->form_validation->set_rules('id_roomdetail', 'Nama Ruangan Tujuan', 'required');
        $this->form_validation->set_rules('nama_pelapor', 'Nama Pelapor', 'required');
        $this->form_validation->set_rules('nama_ruangan', 'Nama Ruangan', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('includes/template', $data);
        } else {
            $save = $this->M_csitp->save_patients_tickets();
            if ($save) {
                redirect('Cs_unit_itp');
            }
        }
    }

    function cancel_ticket() {
        $data['id_ticket'] = $this->input->post('id_ticket');
        $data['alasan_pembatalan'] = $this->input->post('alasan_pembatalan');
        $this->M_csitp->cancel_ticket($data);
        redirect('csitp/ganti_petugas');
    }

    function get_rooms_area()
    {
        $id = $this->input->post('id');
        $data = $this->M_csitp->rooms_area($id)->result_array();
        $response = array(
            "area" => $data[0]['name'],
            "name" => $this->security->get_csrf_token_name(),
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($response);
    }

    //AKTIFITAS PEMINDAHAN PASIEN
    function aktifitas_pemindahan_pasien()
    {
        $data['main_content'] = 'aktifitas_pemindahan_pasien';
        $data['permintaan'] = $this->M_csitp->get_count_permintaan_activity();
        $data['terjadwal'] = $this->M_csitp->get_count_terjadwal_activity();
        $data['selesai'] = $this->M_csitp->get_count_selesai_activity();
        $data['rooms'] = $this->M_csitp->get_rooms()->result();
        $filter = $this->input->post('tgl');
        if($filter != ""){
            $data['result'] = $this->M_csitp->get_aktifitas_harian_filter($filter)->result();
            $this->load->view('includes/template', $data);
        } else {
            $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
            $this->load->view('includes/template', $data);
        }
        
    }

    function data_pindah_pasien()
    {
        $data['main_content'] = 'data_pindah_pasien';
        $data['status'] = $this->M_csitp->get_status();
        
        if(($this->input->post('tgl')==null) && $this->input->post('id_status')==null) {
            $data['token'] = $this->security->get_csrf_hash();
            $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
            $this->load->view('includes/template', $data);
        } else {
            $filter = $this->input->post('tgl')."_".$this->input->post('id_status');
            $data['token'] = $this->security->get_csrf_hash();
            $data['result'] = $this->M_csitp->get_data_pindah_pasien($filter)->result();
            $this->load->view('includes/template', $data);
        }
        
    }

    function detail_pemindahan_pasien($id){
        $data['main_content'] = 'detail_pemindahan_pasien';
        $data['result'] = $this->M_csitp->get_aktifitas_harian_detail($id)->row();
        $this->load->view('includes/template', $data);
    }

    function batalkan_tiket($id){
        $data['main_content'] = 'tiket_batal';
        $data['token'] = $this->security->get_csrf_hash();
        $data['result'] = $this->M_csitp->get_aktifitas_harian_detail($id)->row();
        $data['transporterticket_id'] = $data['result']->transporterticket_id;
        $this->load->view('includes/template', $data);
    }    

    function ganti_petugas() {
        $data['main_content'] = 'ganti_petugas';
        $data['petugas'] = $this->M_csitp->get_petugas();
        // debug($_POST);
        if (is_null($this->input->post('id_petugas'))) {
            $filter = $this->input->post('tgl');
        } elseif (is_null($this->input->post('tgl'))) {
            $filter = $this->input->post('id_petugas');
        } else {
            $filter = $this->input->post('id_petugas')."_".$this->input->post('tgl');
        }
        if($filter == "") {
            $data['token'] = $this->security->get_csrf_hash();
            $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
            $this->load->view('includes/template', $data);
        } else {
            $data['token'] = $this->security->get_csrf_hash();
            $data['result'] = $this->M_csitp->get_aktifitas_harian_filter_by_id_petugas($filter);
            $this->load->view('includes/template', $data);
        }
        
    }

    function ganti_petugas_form($id) {
        $data['main_content'] = 'ganti_petugas2';
        $data['token'] = $this->security->get_csrf_hash();
        $data['petugas'] = $this->M_csitp->get_petugas();
        $data['result'] = $this->M_csitp->get_aktifitas_harian_detail($id)->row();
        $this->load->view('includes/template', $data);
        
    }

    function save_ganti_petugas() {
        // debug($_POST);
        date_default_timezone_set("Asia/Jakarta");
        $data['main_content'] = 'ganti_petugas';
        $data['token'] = $this->security->get_csrf_hash();
        $this->form_validation->set_rules('nama_petugas', 'Nama Petugas', 'required');
        $save_data = array(
                        'id_tiket' => $this->input->post('id_tiket'),
                        'id_petugas' => $this->input->post('id_petugas'),
                        'alasan_penggantian' => $this->input->post('alasan_penggantian')
                     );
        $this->M_csitp->save_ganti_petugas($save_data);
        // die();
        redirect('csitp/ganti_petugas','refresh');
    }

    function laporan_aktivitas_pindah_pasien() {
        $data['main_content'] = 'laporan_aktivitas_pindah_pasien';
        $data['token'] = $this->security->get_csrf_hash();
        $data['petugas'] = $this->M_csitp->get_petugas();
        // $data['result'] = $this->M_csitp->get_aktifitas_harian_detail($id)->row();
        $this->load->view('includes/template', $data);
    }

    function laporan_aktivitas_petugas() {
        $data['main_content'] = 'laporan_aktivitas_petugas';
        $data['token'] = $this->security->get_csrf_hash();
        $data['petugas'] = $this->M_csitp->get_petugas();
        $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
        // debug($data);
        $this->load->view('includes/template', $data);
    }

    function search_laporan_aktivitas_petugas() {
        if (empty($this->input->post())) {
            redirect('csitp/laporan_aktivitas_petugas','refresh');
        }
        
        $data['main_content'] = 'laporan_aktivitas_petugas';
        $data['token'] = $this->security->get_csrf_hash();        
        $data['petugas'] = $this->M_csitp->get_petugas();

        $filter = "";

        if (!empty($this->input->post('id_petugas'))==true) {
            $filter = $this->input->post('id_petugas');
        }

        if (!empty($this->input->post('start_date'))==true && !empty($this->input->post('end_date'))==true) {
            $filter = $this->input->post('id_petugas')."_".$this->input->post('start_date')."_".$this->input->post('end_date');
        }

        if(is_null($filter)) {
            $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
            $this->load->view('includes/template', $data);
        } else {
            $data['result'] = $this->M_csitp->get_aktifitas_harian_filter_by_date_range($filter);
            $this->load->view('includes/template', $data);
        }        
    }
}
