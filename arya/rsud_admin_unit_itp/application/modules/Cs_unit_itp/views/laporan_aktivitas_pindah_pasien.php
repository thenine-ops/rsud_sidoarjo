<!DOCTYPE html>
<html>

<head>
	<title>Apps - Dashboard</title>

	<!-- Javascript External -->
	<script src="../../template/huda/external/js/jquery.js"></script>
	<script src="../../template/huda/external/js/bootstrap.min.js"></script>
	<script src="../../template/huda/external/js/highcharts.js"></script>
	<script src="../../template/huda/external/js/highcharts-exporting.js"></script>
	<script src="../../template/huda/external/js/highcharts-export-data.js"></script>
	<script src="../../template/huda/external/js/highcharts-accessibility.js"></script>

	<!-- Javascript Internal -->
	<script src="../../template/huda/internal/js/button_action.js"></script>
	<script src="../../template/huda/internal/js/modal.js"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="../../template/huda/external/css/bootstrap.min.css">

	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="../../template/huda/internal/css/form.css">
	<link rel="stylesheet" type="text/css" href="../../template/huda/internal/css/content.css">
	<link rel="stylesheet" type="text/css" href="../../template/huda/internal/css/button_action.css">
	<link rel="stylesheet" type="text/css" href="../../template/huda/internal/css/modal.css">
	<link rel="stylesheet" type="text/css" href="../../template/huda/internal/css/loading.css">
</head>

<body class="dashboard-body">
	<div class="loading">
		<img src="../../template/huda/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-base">
			Master data
		</div>
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init">DF</label>
				<img src="../../template/huda/image/profile_photo/profile.png" alt="">
			</div>
			<label class="account-name">Pevita Munaf / Admin ITP</label>
		</div>
		<a href="" class="btn btn-danger rounded pull-right">close</a>
	</div>
	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">LAPORAN AKTIVITAS</h4>
            <div class="date-info f-green padding-tanggal">
                Jumat, 13 Maret 2020
            </div>
            <div class="flex">
                <ul class="sub-menu-container">
                    <li class="item"><a href="<?= site_url('csitp/laporan_aktivitas_petugas'); ?>">AKTIFITAS PETUGAS</a></li>
                    <li class="sep">|</li>
                    <li class="item active"><a href="">AKTIFITAS PINDAH PASIEN</a></li>
                </ul>
            </div>
			<div class="f-col f-float-round pad-sm">
                <div class="flex">
					<div class="f-col-4">
						<input type="month" id="start" name="start" class="form-control" required>
					</div>
					<div class="f-col-4">
						<select class="form-control">
							<option></option>
							<option></option>
						</select>
					</div>
                </div>
                <div class="flex">
					<div class="f-col-8">
						<select class="form-control">
							<option value="" selected disabled>Pilih Pasien</option>
							<option></option>
						</select>
					</div>
				</div>
                <div class="flex">
					<div class="f-col-4">
						<a href="" class="btn c-success rounded pull-right" style="width: 100px;">CARI</a>
					</div>
                </div>
			</div>
            <a href="#"><h6 class="m-3 pull-right">Download</h6></a>
			<div class="flex">
                <table class="table table-green table-bordered bold">
                    <thead> 
                        <th style="text-align: left;" colspan="11">LAPORAN PEMINDAHAN PASIEN BULAN MARET</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: left;" colspan="11">NAMA PASIEN -- DUTA</td>
                        </tr>
                        <tr>
                            <th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
                            <th colspan="1" rowspan="2" scope="colgroup">ID Tiket Permintaan</th>
                            <th colspan="3" scope="colgroup">Ruangan Penjemputan</th>
                            <th colspan="3" scope="colgroup">Ruangan Tujuan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Petugas</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Pasien</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status</th>
                        </tr>
                        <tr>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Detail Ruangan</th>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Detail Ruangan</th>
                        </tr>
                        <tr>
                            <td>15/2020</td>
                            <td>13032020</td>
                            <td>Bunga</td>
                            <td>Daun</td>
                            <td>daun123</td>
                            <td>Macan</td>
                            <td>Daun</td>
                            <td>daun123</td>
                            <td>Cinta</td>
                            <td>jul</td>
                            <td>sembuh</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" colspan="11">NAMA PASIEN -- DUTA</td>
                        </tr>
                        <tr>
                            <th colspan="1" rowspan="2" scope="colgroup">Tanggal</th>
                            <th colspan="1" rowspan="2" scope="colgroup">ID Tiket Permintaan</th>
                            <th colspan="3" scope="colgroup">Ruangan Penjemputan</th>
                            <th colspan="3" scope="colgroup">Ruangan Tujuan</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Petugas</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Nama Pasien</th>
                            <th colspan="1" rowspan="2" scope="colgroup">Status</th>
                        </tr>
                        <tr>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Detail Ruangan</th>
                            <th scope="col">Nama Ruangan</th>
                            <th scope="col">Area Ruangan</th>
                            <th scope="col">Detail Ruangan</th>
                        </tr>
                        <tr>
                            <td>15/2020</td>
                            <td>13032020</td>
                            <td>Bunga</td>
                            <td>Daun</td>
                            <td>daun123</td>
                            <td>Macan</td>
                            <td>Daun</td>
                            <td>daun123</td>
                            <td>Cinta</td>
                            <td>jul</td>
                            <td>sembuh</td>
                        </tr>
                    </tbody>
                </table>
            </div>
		</div>
        <div class="col col-menu pad-sm">
            <div class="menu-logo">
                <img src="<?= base_url(); ?>aset/image/asset/app-logo.png" alt="">
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/aktifitas_pemindahan_pasien'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/web.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">DASHBOARD</div>
                        <div class="menu-desc">Merupakan Preview dari aktifitas yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/ganti_petugas'); ?>" class="menu-item">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/maintenance.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">PINDAH PASIEN</div>
                        <div class="menu-desc">Permintaan pemindahan pasien antara ruangan</div>
                    </div>
                </a>
            </div>
            <div class="menu-button">
                <a href="<?= site_url('csitp/laporan_aktivitas_petugas'); ?>" class="menu-item active">
                    <div class="menu-icon">
                        <img src="<?= base_url(); ?>aset/image/asset/Business Report.png" alt="">
                    </div>
                    <div class="menu-text">
                        <div class="menu-title">LAPORAN AKTIFITAS</div>
                        <div class="menu-desc">Laporan aktivitas aktifitas pelayanan yang dilakukan unit ITP</div>
                    </div>
                </a>
            </div>
        </div>
	</div>
	<div class="footer-container">
		<label>Nine Cloud 2019</label>
	</div>
</body>
<script src="../../template/huda/internal/js/general.js"></script>

</html>