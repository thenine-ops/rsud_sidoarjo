<div class="login-container">
	<div class="img-container">
		<img src="<?= base_url().'prabotan/image/asset/login-logo.png' ?>" alt="">			
	</div>
	<div class="form-container">
		<form class="form-ajax" data-uri="<?= base_url() ?>authenticate/act_login" data-redirect="<?= base_url() ?>dashboard">
			<div class="form-group">
				<div class="fg-img">
					<img src="<?= base_url().'prabotan/image/asset/avatar.png' ?>" alt="">
				</div>
				<input type="text" class="form-control" name="username" placeholder="Email">			
			</div>
			<div class="form-group">
				<div class="fg-img">
					<img src="<?= base_url().'prabotan/image/asset/lock.png' ?>" alt="">
				</div>
				<input type="password" class="form-control" name="password" placeholder="Kata Sandi">			
			</div>
			<div class="fpass-container"><a class="coming-soon" href="#<? //base_url().'authenticate/forget_password' ?>">Lupa Kata Sandi</a></div>
			<button class="btn-login btn-lg btn-block" type="submit">MASUK</button>
		</form>
	</div>
</div>