<a href="<?=base_url('csunitaset/laporan');?>" <?php if($this->uri->segment(3)==''){ ?>class="text-success" style="font-weight: bold;"<?php } ?>>Aktifitas Sparepart</a> | <a href="<?=base_url('csunitaset/laporan/kosong');?>" <?php if($this->uri->segment(3)!=''){ ?>class="text-success" style="font-weight: bold;"<?php } ?>>Aktifitas Ruangan</a>

<div class="flex">
    <div class="f-col">
        <div class="label m-input">
            <input type="text" class="form-control"
                placeholder="Cari Sparepart (ID Sparepart -- Nama Sparepart)">
        </div>
        <a href="" class="pull-right">Download</a>
    </div>
</div>
<div class="flex">
    <table class="table table-green table-bordered bold">
        <thead> 
            <th style="text-align: left;" colspan="8">LAPORAN AKTIFITAS SPAREPART</th>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: left;" colspan="8">Nama Ruangan</td>
            </tr>
            <tr>
                <td style="width: 120px;">Tanggal</td>
                <td>ID Permintaan</td>
                <td>ID Aset</td>
                <td>Nama Aset</td>
                <td>ID Sparepart</td>
                <td>Jumlah</td>
                <td>Status</td>
                <td>Action</td>
            </tr>
            <tr>
                <td>03/08/2019</td>
                <td>1</td>
                <td>1</td>
                <td>kopmuter</td>
                <td>1</td>
                <td>1</td>
                <td>Belum di ambil</td>
                <td><a href="#">Detail</a></td>
            </tr>
            <tr>
                <td style="text-align: left;" colspan="8">Nama Ruangan</td>
            </tr>
            <tr>
                <td style="width: 120px;">Tanggal</td>
                <td>ID Permintaan</td>
                <td>ID Aset</td>
                <td>Nama Aset</td>
                <td>ID Sparepart</td>
                <td>Jumlah</td>
                <td>Status</td>
                <td>Action</td>
            </tr>
            <tr>
                <td>03/08/2019</td>
                <td>1</td>
                <td>1</td>
                <td>kopmuter</td>
                <td>1</td>
                <td>1</td>
                <td>Belum di ambil</td>
                <td><a href="#">Detail</a></td>
            </tr>
            <tr>
                <td style="text-align: left;" colspan="8">Nama Ruangan</td>
            </tr>
            <tr>
                <td style="width: 120px;">Tanggal</td>
                <td>ID Permintaan</td>
                <td>ID Aset</td>
                <td>Nama Aset</td>
                <td>ID Sparepart</td>
                <td>Jumlah</td>
                <td>Status</td>
                <td>Action</td>
            </tr>
            <tr>
                <td>03/08/2019</td>
                <td>1</td>
                <td>1</td>
                <td>kopmuter</td>
                <td>1</td>
                <td>1</td>
                <td>Belum di ambil</td>
                <td><a href="#">Detail</a></td>
            </tr>
        </tbody>
    </table>
</div>