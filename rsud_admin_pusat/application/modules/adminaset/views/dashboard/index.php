<?=$this->session->flashdata('name');?>
<div class="flex">
  <div class="f-col-5 f-float-round center pad-lg text-center">
    <div class="h6">Permintaan Spare Part Hari Ini</div>
    <div class="h1 text-success">
        17
    </div>
    <div class="h6">SPARE PART</div>
  </div>
  <div class="f-col-5 f-float-round center pad-lg text-center">
    <div class="h6">Total Spare Part Stok Habis</div>
      <div class="flex">
          <div class="f-col">
              <div class="h1 text-danger">
                  <?=$stokhabis?>
              </div>
              <div class="h6">SPARE PART</div>
          </div>
      </div>
  </div>
</div>

<div class="flex">
  <div class="f-col"><h6>Aktifitas Penggunaan Spare Part Bulan <?=bulan(date('m'));?> <?=date('Y');?></h6></div>
  <div class="f-col"><span class="pull-right"><a href="<?=base_url('adminaset/dashboard');?>">Grafik</a> | <a href="<?=base_url('adminaset/dashboard/tabel');?>">Tabel</a></span></div>
</div>

<div class="flex">
  <div class="f-col f-float-round pad-sm">
    <div class="position-relative mb-4">
      <div class="chartjs-size-monitor">
        <div class="chartjs-size-monitor-expand">
          <div class=""></div>
        </div>
        <div class="chartjs-size-monitor-shrink">
          <div class=""></div>
        </div>
      </div>
      <canvas id="visitors-chart" height="250" width="451" class="chartjs-render-monitor"
        style="display: block; height: 200px; width: 361px;"></canvas>
    </div>
  </div>
</div>