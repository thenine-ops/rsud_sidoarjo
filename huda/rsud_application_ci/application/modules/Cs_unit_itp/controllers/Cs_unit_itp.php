<?php defined('BASEPATH') or exit('No direct script access allowed');

class Cs_unit_itp extends MY_controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_csitp');
    }

    function index()
    {
        date_default_timezone_set("Asia/Jakarta");
        $data['main_content'] = 'tiket_pemindahan_pasien';
        $data['rooms'] = $this->M_csitp->get_rooms()->result();
        $data['token'] = $this->security->get_csrf_hash();
        $this->load->view('includes/template', $data);
    }

    function save_patient_movement()
    {
        date_default_timezone_set("Asia/Jakarta");
        $data['main_content'] = 'tiket_pemindahan_pasien';
        $data['token'] = $this->security->get_csrf_hash();
        $this->form_validation->set_rules('nama', 'Nama Pasien', 'required');
        $this->form_validation->set_rules('kontak', 'Kontak', 'required');
        $this->form_validation->set_rules('id_room_penjemputan', 'Nama Ruangan Penjemputan', 'required');
        $this->form_validation->set_rules('id_roomdetail', 'Nama Ruangan Tujuan', 'required');
        $this->form_validation->set_rules('nama_pelapor', 'Nama Pelapor', 'required');
        $this->form_validation->set_rules('nama_ruangan', 'Nama Ruangan', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('includes/template', $data);
        } else {
            $save = $this->M_csitp->save_patients_tickets();
            if ($save) {
                redirect('Cs_unit_itp');
            }
        }
    }

    function get_rooms_area()
    {
        $id = $this->input->post('id');
        $data = $this->M_csitp->rooms_area($id)->result_array();
        $response = array(
            "area" => $data[0]['name'],
            "name" => $this->security->get_csrf_token_name(),
            "token" => $this->security->get_csrf_hash()
        );
        echo json_encode($response);
    }

    //AKTIFITAS PEMINDAHAN PASIEN
    function aktifitas_pemindahan_pasien()
    {
        $data['main_content'] = 'aktifitas_pemindahan_pasien';
        $data['permintaan'] = $this->M_csitp->get_count_permintaan_activity();
        $data['terjadwal'] = $this->M_csitp->get_count_terjadwal_activity();
        $data['selesai'] = $this->M_csitp->get_count_selesai_activity();
        $filter = $this->input->post('tgl');
        if($filter != ""){
            $data['result'] = $this->M_csitp->get_aktifitas_harian_filter($filter)->result();
            $this->load->view('includes/template', $data);
        } else {
            $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
            $this->load->view('includes/template', $data);
        }
        
    }

    function detail_pemindahan_pasien($id){
        $data['main_content'] = 'detail_pemindahan_pasien';
        $data['result'] = $this->M_csitp->get_aktifitas_harian_detail($id)->row();
        $this->load->view('includes/template', $data);
    }

    function ganti_petugas() {
        $data['main_content'] = 'ganti_petugas';
        $filter = $this->input->post('tgl');
        if($filter == "") {
            $data['token'] = $this->security->get_csrf_hash();
            $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
            $this->load->view('includes/template', $data);
        } else {
            $data['token'] = $this->security->get_csrf_hash();
            $data['result'] = $this->M_csitp->get_aktifitas_harian_filter($filter)->result();
            $this->load->view('includes/template', $data);
        }
    }
    function ganti_petugas_form($id) {
        $data['main_content'] = 'ganti_petugas2';
        $data['token'] = $this->security->get_csrf_hash();
        $data['result'] = $this->M_csitp->get_aktifitas_harian_detail($id)->row();
        $this->load->view('includes/template', $data);
        
    }
    function save_ganti_petugas() {
        date_default_timezone_set("Asia/Jakarta");
        $data['main_content'] = 'ganti_petugas';
        $data['result'] = $this->M_csitp->get_aktifitas_harian()->result();
        $data['token'] = $this->security->get_csrf_hash();
        $this->form_validation->set_rules('nama_petugas', 'Nama Petugas', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('includes/template', $data);
        } else {
            $save = $this->M_csitp->save_ganti_petugas_model();
            if ($save) {
                redirect('csitp/ganti_petugas');
            }
        }
    }
}
