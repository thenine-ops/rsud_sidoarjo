<?=$this->session->flashdata('name');?>
<div class="flex">
  <div class="f-col-7 f-float-round pad-sm">
    <h6>Pelayanan Hari Ini</h6>

    <div class="row mb-3">
      <div class="col-12">
        <div id="dashboard-info"></div>
      </div>
    </div>

  </div>

  <div class="f-col-5 f-float-round pad-sm">
    <h6>Tambah Stok Spare Part</h6>

    <?=form_open();?>

          <div class="label mt-3">
            <select name="sparepart" id="inputSparepart" class="form-control select2 <?=(form_error('sparepart'))?'is-invalid':'';?>" required="required">
              <option value="">Pilih Spare Part</option>
              <?php foreach($sparepart as $s): ?>
              <option value="<?=$s->id?>"><?=$s->name?></option>
              <?php endforeach; ?>
            </select>
            <?=form_error('sparepart','<small class="text-danger">','</span>');?>
          </div>

          <div class="label">
            <input type="text" class="form-control mt-3 <?=(form_error('stok'))?'is-invalid':'';?>" name="stok" value="<?=set_value('stok');?>" placeholder="Jumlah Stok">
            <?=form_error('stok','<small class="text-danger">','</span>');?>
          </div>
          
          <button type="submit" class="btn btn-primary rounded mt-3 pull-right">Tambah Stok</button>

          <?=form_close();?>

  </div>
</div>

<div class="flex">
  <div class="f-col"><h6>Aktifitas Permintaan Spare Part</h6></div>
</div>

<div class="flex f-float-round pad-lg pad-bottom">
                    <div class="f-col-3 f-float-round">
                        ini adalah image
                    </div>
                    <div class="f-col f-float-round pad-xs">
                        <div class="label pad-xs">
                            <button class="btn btn-outline-primary rounded w100" style="margin-right: 15px !important;">SPAREPART UMUM</button>
                            <button class="btn btn-outline-success rounded w100">56Pcs</button>
                        </div>
                        <div class="label pad-xs">
                            <h4>Catridge Canon PG-810 Black</h4>
                            <h5 class="coral">CTRD_123456093</h5>
                        </div>
                        <div class="flex">
                            <div class="f-col-12" style="margin-right: 3px;">
                                <div class="gray pad-left-5">Permintaan</div>
                                <button class="btn btn-outline-danger rounded w100" style="margin-right: 15px !important;">SPAREPART UMUM</button>
                            </div>
                            <div class="f-col">
                                <div class="gray pad-left-4">ID Permintaan</div>
                                <button class="btn btn-outline-danger rounded w100">PR_123123213</button>
                            </div>
                            <div class="f-col-6">
                                <a href="" class="btn btn-success btn-lg right shadow rounded" style="margin-top: 14px;">VALIDASI</a>
                            </div>
                            
                        </div>
                    </div>
                </div>