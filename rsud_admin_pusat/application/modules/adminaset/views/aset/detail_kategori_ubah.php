<?=form_open('adminaset/aset/detail_kategori_edit/'.$data->id);?>
  <input type="hidden" name="id" id="inputId" class="form-control" value="<?=$data->id;?>">
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Kategori</label>
    <div class="col-sm-10">
      <select name="kategori" id="inputKategori" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('kategori'))?'is-invalid':'';?>">
        <option value="">Pilih Kategori</option>
        <?php foreach($kategori as $k): ?>
        <option value="<?=$k->id;?>" <?=($k->id==$data->assetcategory_id)?'selected':'';?>><?=$k->name;?></option>
        <?php endforeach; ?>
      </select>
      <?=form_error('kategori','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Sub Kategori</label>
    <div class="col-sm-10">
      <select name="subkategori" id="inputSubkategori" class="form-control form-control-lg mb-2 mr-sm-2 <?=(form_error('subkategori'))?'is-invalid':'';?>">
        <option value="<?=$subkategori->id;?>" selected><?=$subkategori->name;?></option>
      </select>
      <?=form_error('subkategori','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="detail" class="col-sm-2 col-form-label">Detail Kategori</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('detail'))?'is-invalid':'';?>" id="detail" placeholder="Detail Kategori (Contoh: Printer, Komputer, Jaringan WiFi, dll)" name="detail" value="<?=$data->name;?>">
      <?=form_error('detail','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('deskripsi'))?'is-invalid':'';?>" id="deskripsi" placeholder="Deskripsi Detail Kategori" name="deskripsi" value="<?=$data->description;?>">
      <?=form_error('deskripsi','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <label for="quantity" class="col-sm-2 col-form-label">Quantity</label>
    <div class="col-sm-10">
      <input type="text" class="form-control form-control-lg <?=(form_error('quantity'))?'is-invalid':'';?>" id="quantity" placeholder="Quantity" name="quantity" value="<?=$data->quantity;?>">
      <?=form_error('quantity','<small class="text-danger">','</small>');?>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-10 offset-2">
      <button type="submit" class="btn btn-success rounded btn-lg">Simpan</button> 
      <a href="<?=base_url('adminaset/aset/detail_kategori');?>" class="btn btn-dark rounded btn-lg">Kembali</a>
    </div>
  </div>
</form>