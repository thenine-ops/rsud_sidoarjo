<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Authenticate extends CI_Model {

	function getDataAuth($username, $password){
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$get_data = $this->db->get('user_admin')->row();
		if($get_data){
			$this->db->where('acc.id', $get_data->useraccount_id);
			$this->db->select('acc.id_account_register');
			$this->db->select('acc.name as name');
			$this->db->select('ro.description as role');
			$this->db->select('un.name as unit');
			$this->db->select('acc.status as status');
			$this->db->select('acc.email as email');
			$this->db->select('acc.phone as phone');
			$this->db->join('roles ro', 'ro.id = usr.role_id', 'left');
			$this->db->join('units un', 'un.id = usr.unit_id', 'left');
			$this->db->join('user_accounts acc', 'acc.id = usr.useraccount_id', 'left');
			$data = $this->db->get('user_admin usr')->row_array();

			$feedback['data'] = $data;
			$feedback['res'] = "success";
		}else{
			$feedback['res'] = FALSE;
		}

		return $feedback;
	}
}

/* End of file m_Authenticate.php */
/* Location: ./application/models/m_Authenticate.php */