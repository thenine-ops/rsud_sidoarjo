<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$admin_auth = $this->session->userdata('rsud_admin_pusat');
		$type = $this->uri->segment(2);
		if(!$admin_auth){
			// redirect(base_url('authenticate'));
		}else{
			if($type != 'logout'){
			redirect(base_url('dashboard'));
			}
		}
		$this->load->model('M_Authenticate', 'MAuth');
	}
	public function index()
	{
		$data['page'] = $this->load->view('login', NULL, TRUE);
		$this->load->view('general/main_login', $data);
	}
	public function forget_password()
	{
		$data['page'] = $this->load->view('forget_password', NULL, TRUE);
		$this->load->view('general/main_login', $data);
	}
	public function change_password()
	{
		$data['page'] = $this->load->view('change_password', NULL, TRUE);
		$this->load->view('general/main_login', $data);
	}
	public function asdasdasd()
	{
		$this->db->where('id', 'id', FALSE);
		$this->db->get('user_admin');
		echo $this->db->last_query();
	}
	public function act_login()
	{
		$post = $this->input->post();
		$auth = $this->MAuth->getDataAuth($post['username'], $post['password']);
		if($auth['res']==TRUE){
			$this->session->set_userdata('rsud_admin_pusat', $auth['data']);
			$feedback['res'] = "success";
		}else{
			$feedback['res'] = "fail";
			$feedback['msg'] = "Login gagal, email atau kata sandi salah!";
		}
		echo json_encode($feedback);
	}
	public function insert_user()
	{
		$data['unit_id'] = 1;
		$data['id_account_register'] = 'SPRADM';
		$data['name'] = 'Wahyu Kristiawan';
		$data['email'] = 'whykris97@gmail.com';
		$data['phone'] = '081111222333';
		$data['status'] = '1';
		$data['user_ent'] = '';
		$data['date_ent'] = date('Y-m-d');
		$this->db->insert('accounts', $data);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('authenticate'));
	}
}
